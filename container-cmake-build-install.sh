#!/bin/sh

if [ -d sysroot ] ; then
	echo "sysroot already exists, not creating"
else
	mkdir sysroot
fi

cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=sysroot -DENABLE_PYTHON2=NO -DENABLE_PYTHON3=NO && make -j4 && make install 
