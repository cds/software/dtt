
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdscmd_d						*/
/*                                                         		*/
/* Module Description: implements functions for executing gds commands	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef DEBUG
#define DEBUG
#endif


#ifndef DYNAMIC_LOAD
#ifdef OS_VXWORKS
#define DYNAMIC_LOAD		0
#else
#define DYNAMIC_LOAD		1
#endif
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Includes: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#if DYNAMIC_LOAD
#include <dlfcn.h>
#endif
#include <stdio.h>
#include "PConfig.h"
#include "gdscmd_d.h"

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Constants: LIBRARY_NAME	name of dynamic library			*/
/*            DISPATCH_LEN	length of function dispatch table	*/
/*            FUNC_1		name of first function			*/
/*            FUNC_2		name of second function			*/
/*            FUNC_3		name of third function			*/
/*            FUNC_4		name of fourth function			*/
/*            FUNC_5		name of fivth function			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#ifdef P__WIN32
#define RTLD_LOCAL		0
#define LIBRARY_NAME		"libdtt.dll"
#else

#ifdef P__DARWIN
#define LIBRARY_NAME		"libdtt.dylib"
#else
#define LIBRARY_NAME		"libdtt.so.1"
#endif

#endif

#define str_libdir_(s) #s
#define str_libdir(s) str_libdir_(s)

#define DISPATCH_LEN		5
#define FUNC_1			"gdsCmdInit"
#define FUNC_2			"gdsCmdFini"
#define FUNC_3			"gdsCmd"
#define FUNC_4			"gdsCmdNotifyHandler"
#define FUNC_5			"gdsCmdData"


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Types: func_t		void function type			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   typedef void (*func_t) (void);


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Globals: init		init variable				*/
/*          handle		library handle				*/
/*          dispatch		dispatch table				*/
/*          funcname		list of function names in linked library*/
/*      							    	*/
/*----------------------------------------------------------------------*/
#if DYNAMIC_LOAD
   static int		init = 0;
   static void*		handle;	    /* library handle */
   static func_t	dispatch[DISPATCH_LEN];
   static const char* const	funcname[] = 
   {FUNC_1, FUNC_2, FUNC_3, FUNC_4, FUNC_5};
#endif


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: _gdsCmdInit					*/
/*                                                         		*/
/* Procedure Description: initializes the gds command interface		*/
/*                                                         		*/
/* Procedure Arguments: flag						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int _gdsCmdInit (int flag, const char* conf)
   {
   #if !DYNAMIC_LOAD
      return -99;
   #else
   
      int		i;
      if (init == 0) {
         /* load dynamic library */
         handle = dlopen (LIBRARY_NAME, RTLD_NOW | RTLD_LOCAL);
         /*handle = dlopen (LIBRARY_NAME, RTLD_NOW | RTLD_GLOBAL);*/
#ifdef P__DARWIN
         if (handle == NULL) {
	   char libname[ 2048 ];

	   sprintf( libname, "%s/%s", str_libdir(LIBDIR), LIBRARY_NAME );

	   handle = dlopen (libname, RTLD_NOW | RTLD_LOCAL);
	   if (handle == NULL) {
            printf ("Loading %s failed\n", libname);
            printf ("Error: %s\n", dlerror());
            return -97;
	   }
         }
#endif /* P__DARWIN */
         if (handle == NULL) {
            printf ("Loading %s failed\n", LIBRARY_NAME);
            printf ("Error: %s\n", dlerror());
            return -97;
         }
      
         /* resolve dispatch table */
         for (i = 0; i < DISPATCH_LEN; i++) {
            dispatch[i] = (func_t) dlsym (handle, funcname[i]);
            if (dispatch[i] == NULL) {
               printf ("dlsym failed %i\n", i);
               return -98;
            }
         }
         init = 1;
      }
   
      /* call true function */
      return ((int (*) (int, const char*)) *dispatch[0]) (flag, conf);
   #endif
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: _gdsCmdFini					*/
/*                                                         		*/
/* Procedure Description: cleans up the gds command interface		*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int _gdsCmdFini ()
   {
   #if !DYNAMIC_LOAD
      return -99;
   #else
   
      int		retval;
#if 0
      /* This goes with the comment below regarding closing the library.*/
      int		i ;
#endif
   
      if (init == 0) {
         return 0;
      }
      /* call true function */
      retval = ((int (*) ()) *dispatch[1]) ();
   
#if 0
      /* JCB We need to not close the library, because it's where this
       * function is.  Closing it causes a segmentation fault.
       */
      /* unload dynamic library and return */
      i = dlclose (handle);
#endif
      init = 0;
      return retval;
   #endif
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: _gdsCmd					*/
/*                                                         		*/
/* Procedure Description: calls a gds command 				*/
/*                                                         		*/
/* Procedure Arguments: message, parameter, reply pointer		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int _gdsCmd (const char* msg, const char* prm, int pLen,
               char** res, int* rLen)
   {
   #if !DYNAMIC_LOAD
      return -99;
   #else
   
      if (init == 0) {
         return -99;
      }
      /* call true function */
      return ((int (*) (const char*, const char*, int, char**, int*))
             *dispatch[2]) (msg, prm, pLen, res, rLen);
   #endif
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: _gdsCmdData					*/
/*                                                         		*/
/* Procedure Description: transfers data 				*/
/*                                                         		*/
/* Procedure Arguments: data object name, transfer direction, data type,*/
/*                      length, offset, data array, array length	*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int _gdsCmdData (const char* name, int toKernel, int datatype,
                   int len, int ofs, float** data, int* datalength)
   {
   #if !DYNAMIC_LOAD
      return -99;
   #else
   
      if (init == 0) {
         return -99;
      }
      /* call true function */
      return ((int (*) (const char*, int, int, int, int, float**, int*))
             *dispatch[4]) (name, toKernel, datatype, len, ofs, data,
                           datalength);
   #endif
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: _gdsCmdNotifyHandler			*/
/*                                                         		*/
/* Procedure Description: installs a command notification handler	*/
/*                                                         		*/
/* Procedure Arguments: callback routine				*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 when failed			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   int _gdsCmdNotifyHandler (gdsCmdNotification callback)
   {
   #if !DYNAMIC_LOAD
      return -99;
   #else
   
      if (init == 0) {
         return -99;
      }
   
      /* call true function */
      return ((int (*) (gdsCmdNotification)) *dispatch[3]) (callback);
   #endif
   }
