/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: cmdline							*/
/*                                                         		*/
/* Module Description: implements the command line interface		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Includes: 								*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <time.h>
#include <string>
#include <fstream>
#include <cstdlib>
#include "cmdline.hh"
#include "gdsutil.h"
#include "gdsmsg.h"
#include "diagnames.h"


namespace diag {
   using namespace std;


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Forward declarations: 						*/
/*	readline		GNU readline (cmd line support)		*/
/*	add_history		GNU add_hsitory (cmd line support)	*/
/*	current_history		GNU current_hsitory (cmd line support)	*/
/*      								*/
/*----------------------------------------------------------------------*/
extern "C" char* readline (const char* prompt);
extern "C" void add_history (const char* line);



/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: commandline						*/
/*                                                         		*/
/* Method Description: command line interpreter	constructor		*/
/*                                                         		*/
/* Method Arguments: cmd line arguments, input stream, output stream	*/
/*                                                         		*/
/* Method Returns: void							*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
   commandline::commandline (int argc, char* argv[], bool Silent) 
   :  basic_commandline (Silent), prompt ("diag> ")
   {
      setup (argc, argv);
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: operator ()						*/
/*                                                         		*/
/* Method Description: command line interpreter				*/
/*                                                         		*/
/* Method Arguments: void						*/
/*                                                         		*/
/* Method Returns: true while not yet finished				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   bool commandline::operator () () 
   {
      if (finished) {
         return !finished;
      }
   
      char*	  newline;	/* new cmd line */
   
      /* read line from std input */
      newline = readline (prompt.c_str());
      if (newline == 0) {
         finished = true;
         return !finished;
      }
   
      /* remove leading blanks */
      if (newline != 0) {
         char*		p = newline;
         while (*p == ' ') {
            p++;
         }
         strcpy (newline, p);
      }
   
      /* add to history */
      if ((newline != 0) && (strlen (newline) > 0) && 
         (lastline != newline)) {
         add_history (newline);
      }
   
      /* parse */
      lastline = string (newline);
      string 		line (newline);
      free (newline);
      parse (line);
      return !finished;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Method Name: cbfunc							*/
/*                                                         		*/
/* Method Description: callback function				*/
/*                                                         		*/
/* Method Arguments: filename						*/
/*                                                         		*/
/* Method Returns: true if successful					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/     
   void commandline::printline (const std::string& s)
   {
      printf ("%s\n", s.c_str());
   }


}
