/* Version $Id: foton.hh 6317 2010-09-17 17:58:31Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_FOTON_H
#define _LIGO_FOTON_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: foton							*/
/*                                                         		*/
/* Module Description: Filter Online Tool				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 1.0	 29Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: foton.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/** @name foton
    This is the Filter Online Tool.

    \begin{verbatim}
    Usage: foton [-w] [-d display] [-p path] [filename]
           -w            read-write mode (readonly default)
           -p 'path'     path of online configuration files
           -d 'display'  X windows display
           -h            this help
    \end{verbatim}

    @memo Filter Online Tool
    @author Written July 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

//@}

#endif // _LIGO_FOTON_H
