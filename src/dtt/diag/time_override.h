//
// Created by erik.vonreis on 3/14/24.
//

#ifndef CDS_CRTOOLS_TIME_OVERRIDE_H
#define CDS_CRTOOLS_TIME_OVERRIDE_H

#include <string>
#include <tconv.h>
#include <databroker.hh>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Return GPS time in nanoseconds.  If time is synchronized with the data source
 * using time_override_sync(),
 * the the return value will be an approximation of the latest timestamp at the data source.
 *
 * If there has been no override, or time_override_clear was called, then the system time is returned.
 *
 * @return
 */
tainsec_t gps_now_ns();


#ifdef __cplusplus
}

/**
 * When this function succeeds, gps_now_ns() will return a time that's synchronized
 * with the data source data stream at the time of the call.
 *
 * The call uses a datastream for the given channel to determine time at the data source.
 *
 * The difference between system time and data source time at the time of the call is added to system time
 * to approximate data source time in calls to gps_now_ns
 * @param hostname
 * @param port
 * @param info
 * @return
 */
bool time_override_sync(std::string hostname, int port, gdsChnInfo_t *info);

/**
 * Clear time override.  gps_now_ns() will return system time.
 */
void time_override_clear();

#endif

#endif //CDS_CRTOOLS_TIME_OVERRIDE_H
