//
// Created by erik.vonreis on 3/14/24.
//
#include <mutex>
#include "time_override.h"
#include "databroker.hh"
#include "DAQSocket.hh"

static std::mutex mut;

static bool override = false;

void time_override_clear() {
    override = false;
}


/**
 * Sync (remote) and local time and epoch of last synchronization.
 * The difference is used to dead-reckon the remote time.
 */
static tainsec_t sync_time = 0;
static tainsec_t local_time=0;


/**
 * If local time is overridden with data source time,
 * return the time close to the latest time stamp by using a memoized offset from local time.
 * Otherwise return local system time.
 */
extern "C" tainsec_t gps_now_ns() {
    if (override) {
        return TAInow() + sync_time - local_time;
    } else {
        return TAInow();
    }
}


/// Derive current "ADC" time from the nds server
/// Diaggui doesn't work if isn't synched somehow to the ADC clock used to collect data.
/// \return
bool time_override_sync(std::string hostname, int port, gdsChnInfo_t *info) {
    bool synced = false;
    const int64_t timeout = 5000000000l;

    // we assume about two EPOCHS of time have passed since the data was produced.
    // this our estimate of the time delta form the latest timestamp to now.
    const int64_t sync_time_offset = _EPOCH * 0;

    std::lock_guard<std::mutex> lock(mut);

    DAQSocket temp_nds;
    if (temp_nds.isOpen() || (temp_nds.open(hostname.c_str(), port) == 0)) {

        temp_nds.AddChannel(info->chName,
                            DAQSocket::rate_bps_pair(info->dataRate, info->bps));
        if (temp_nds.RequestOnlineData(1, 10) == 0) {
            tainsec_t now = TAInow();
            for (;;) {
                int err = temp_nds.WaitforData(true);
                if (err < 0) {
                    break;
                }
                if (err == 0) {
                    if (TAInow() - now > timeout) {
                        break;
                    } else {
                    }
                } else {
                    char *buffer;
                    temp_nds.GetData(&buffer, 1);
                    DAQDRecHdr *hdr = (DAQDRecHdr *) buffer;
                    if (hdr->GPS > 0) {
                        tainsec_t sync_now =
                                ((int64_t) hdr->GPS * _ONESEC) + (hdr->NSec) + sync_time_offset;
                        sync_time = sync_now;
                        local_time = TAInow();
                        //local_time = sync_now;
                        printf("ADC time delta = %lf sec\n", (double) (sync_now - local_time) / _ONESEC);
                        synced = true;
                        override = true;
                        break;
                    }
                }
            }
        }

        temp_nds.close();
    }
    return synced;
}

