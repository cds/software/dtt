/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: supervisory						*/
/*                                                         		*/
/* Module Description: diagnostics supervisory abstract class		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include <time.h>
#include <iostream>
#include "tconv.h"
#include "supervisory.hh"
#include "diagnames.h"
#include "diagdatum.hh"
#include "testpointmgr.hh"
#include "rtddinput.hh"
#include "time_override.h"

   const double __ONESEC = (double) _ONESEC;


namespace diag {
   using namespace std;
   using namespace thread;

static const int my_debug = 0 ;

   supervisory::~supervisory () 
   {
   }


   bool supervisory::init (diagStorage& Storage, const cmdnotify& Notify,
                     dataBroker& databroker, testpointMgr& TPMgr, 
                     bool* Abort, bool* Pause, bool rtMode) 
   {
      semlock	lockit (mux);
      storage = &Storage;
      notify = Notify;
      tpMgr = &TPMgr;
      dataMgr = &databroker;
      abort = Abort;
      pause = Pause;
      RTmode = rtMode;
   
      return setup ();
   }




   const double basic_supervisory::setupTime = 5.0;


   bool basic_supervisory::setup () 
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "basic_supervisory::setup()" << endl ;
      // check storage
      if (storage == 0) {
         errmsg << "No diagnostics parameters" << endl;
         return false;
      }
      // delete previously measured data
      storage->eraseResults();
   
      bool		err = false;
      /* check defaults and sync objects */
      if ((storage->Def == 0) || (storage->Sync == 0)) {
         errmsg << "Unable to load value from Def and/or Sync" << endl;
         err = true;
      }
   
      /* read def settings */
      if (!diagDef::self().getParam (*storage->Def,
                           stDefAllowCancel, allowCancel)) {
         errmsg << "Unable to load value from Def." << 
            stDefAllowCancel << endl;
         err = true;
      }
      cerr << "basic_supervisory::setup() - allowCancel = " << allowCancel << endl ;
      if (!diagDef::self().getParam (*storage->Def,
                           stDefNoStimulus, noStimulus)) {
         errmsg << "Unable to load value from Def." <<
            stDefNoStimulus << endl;
         err = true;
      }
      if (!diagDef::self().getParam (*storage->Def, 
                           stDefNoAnalysis, noAnalysis)) {
         errmsg << "Unable to load value from Def." <<
            stDefNoAnalysis << endl;
         err = true;
      }
      if (!diagDef::self().getParam (*storage->Def,
                           stDefKeepTraces, keepTraces)) {
         errmsg << "Unable to load value from Def." <<
            stDefKeepTraces << endl;
         err = true;
      }
      if (!diagDef::self().getParam (*storage->Def, 
                           stDefSiteDefault, siteDefault)) {
         errmsg << "Unable to load value from Def." <<
            stDefSiteDefault << endl;
         err = true;
      }
      if (!diagDef::self().getParam (*storage->Def, 
                           stDefSiteForce, siteForce)) {
         errmsg << "Unable to load value from Def." <<
            stDefSiteForce << endl;
         err = true;
      }
      if (!diagDef::self().getParam (*storage->Def, 
                           stDefIfoDefault, ifoDefault)) {
         errmsg << "Unable to load value from Def." <<
            stDefIfoDefault << endl;
         err = true;
      }
      if(!diagDef::self().getParam (*storage->Def, 
                          stDefIfoForce, ifoForce)) {
         errmsg << "Unable to load value from Def." <<
            stDefIfoForce <<endl;
         err = true;
      }
   
      /* read sync settings */
      int synctype = 0;
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncType, synctype)) {
         errmsg << "Unable to load value from Sync." << 
            stSyncType << endl;
         err = true;
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncStart, start)) {
         errmsg << "Unable to load value from Sync." << 
            stSyncStart << endl;
         err = true;
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncWait, wait)) {
         errmsg << "Unable to load value from Sync." << 
            stSyncWait << endl;
         err = true;
      }
      switch (synctype) {
         // time now
         default:
         case 0:
            {
               start = 0;
               wait = 0;
               break;
            }
         // relative time
         case 1:
            {
               start = 0;
               break;
            }
         // absolute time
         case 2:
            {
               wait = 0;
               break;
            }
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncRepeatRate, repeatrate)) {
         errmsg << "Unable to load value from Sync." <<
            stSyncRepeatRate << endl;
         err = true;
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncSlowDown, slowDown)) {
         slowDown = 0;
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncWaitForStart, waitForStart)) {
         waitForStart = "";
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncWaitAtEachStep, waitAtEachStep)) {
         waitAtEachStep = "";
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncSignalEndOfStep, signalEndOfStep)) {
         signalEndOfStep = "";
      }
      if (!diagSync::self().getParam (*storage->Sync,
                           stSyncSignalEnd, signalEnd)) {
         signalEnd = "";
      }
   
      // check if we can support real-time mode
      cerr << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
      cerr << "RTmode = " << RTmode << endl;
      cerr << "start = " << start << endl;
      time_override_clear();
      if (!err && RTmode) {
         // we may need to synchronize with the data source at the start
         // of a real-time test.
         this->override_time();
         tainsec_t	now = gps_now_ns();
         tainsec_t	mtime = 
            ((start == 0) ? now : start) + (tainsec_t) (wait * __ONESEC);
         if (((start == 0) && (wait < 0)) ||
            ((start > 0) && 
            (mtime < now + (tainsec_t)(setupTime * __ONESEC)))) {
            cerr << "RT mode = false !!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
            cerr << "mtime = " << mtime << endl;
            cerr << "wait =  " << (tainsec_t) (wait * __ONESEC) << endl;
            cerr << "now =   " << now << endl;
            cerr << "setupTime = " << (tainsec_t)(setupTime * __ONESEC) << endl;
            RTmode = false;
         }
      }
      // disable test point manager if not in real-time mode
      if (tpMgr != 0) {
         tpMgr->setState (RTmode);
      }
   
      /* return on error */
      if (my_debug) cerr << "basic_supervisory::setup() return" << endl ;
      return !err;
   
   }


   bool basic_supervisory::readEnvironment ()
   {
      semlock		lockit (mux);
      envlist.clear();
   
      if (my_debug) cerr << "basic_supervisory::readEnvironment ()" << endl ;
      /* check storage */
      if (storage == 0) {
         errmsg << "No diagnostics parameters" << endl;
         return false;
      }
   
      /* go through environment list */
      int		i = 0;
      bool		err = false;
      for (diagStorage::gdsDataObjectList::iterator 
          iter = storage->Env.begin();
          iter !=  storage->Env.end(); iter++, i++) {
      
         environment		env;
         /* test data object */
         if (*iter == 0) {
            continue;
         }
         env.num = i;
         if (!diagEnv::self().getParam (**iter,
                              stEnvActive, env.active)) {
            errmsg << "Unable to load value from Env[" << i <<
               "]." << stEnvActive << endl;
            err = true;
            env.num = -1;
         }
         if (!diagEnv::self().getParam (**iter,
                              stEnvChannel, env.channel)) {
            errmsg << "Unable to load value from Env[" << i <<
               "]." << stEnvChannel << endl;
            err = true; 
            env.num = -1;
         }
         if (!diagEnv::self().getParam (**iter,
                              stEnvWaveform, env.waveform)) {
            errmsg << "Unable to load value from Env[" << i <<
               "]." << stEnvWaveform << endl;
            err = true;
            env.num = -1;
         }
         if (!diagEnv::self().getParam (**iter,
                              stEnvWait, env.wait)) {
            errmsg << "Unable to load value from Env[" << i <<
               "]." << stEnvWait << endl;
            err = true;
            env.num = -1;
         }
         /* read points */
         gdsDatum		dat;
         if (diagEnv::self().getParam (**iter, stEnvPoints, dat)) {
            if ((dat.dimension.size() == 1) &&
               (dat.datatype == gds_float32) &&
               (dat.value != 0)) {
               env.points.assign ((float*) dat.value,
                                 (float*) dat.value + dat.elNumber());
            }
            else {
               errmsg << "Unable to load value from Env[" << i <<
                  "]." << stEnvPoints << endl;
               err = true;
               env.num = -1;
            }
         }
         else {
            env.points.clear();
         }
      
         /* add env to list */
         if (env.num != -1) {
            envlist.push_back(env);
         }
      }
   
      /* return on error */
      if (err) {
	 if (my_debug) cerr << "basic_supervisory::readEnvironment () return false" << endl ;
         return false;
      }
   
      if (my_debug) cerr << "basic_supervisory::readEnvironment () return true" << endl ;
      return true;
   }


   bool basic_supervisory::setMeasurementTime (tainsec_t mtime)
   {
      if (my_debug) cerr << "basic_supervisory::setMeasurementTime( mtime = " << (mtime / _ONESEC) << '.' << (mtime % _ONESEC) << ')' << endl ;
      if ((storage->TestTime == 0) || (storage->TestTimeUTC == 0)) {
         return false;
      }
      utc_t		utc;
      char		s[100];
      *(storage->TestTime) = gdsParameter (stTestTime, gds_int64, &mtime, "ns");
      TAIntoUTC (mtime, &utc);
      strftime (s, 100, "%Y-%m-%d %H:%M:%S", &utc);
      *(storage->TestTimeUTC) = gdsParameter (stTestTimeUTC, s, "ISO-8601");
      if (my_debug) cerr << "basic_supervisory::setMeasurementTime() return" << endl ;
      return true;
   }


}
