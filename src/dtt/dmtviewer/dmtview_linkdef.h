#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ namespace dttgui;
#pragma link C++ class dttgui::TLGMonitorSelection;
#pragma link C++ class dttgui::TLGMonitorSelectionDialog;
//#pragma link C++ class dttgui::TLGMonitorDatum;
//#pragma link C++ class dttgui::TLGMonitorDatumList;
//#pragma link C++ class dttgui::TLGMonitorMgr;
//#pragma link C++ class dttgui::xsilHandlerMonitor;
//#pragma link C++ class dttgui::xsilHandlerQueryMonitor;

#endif

