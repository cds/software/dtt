
#set global app version
add_compile_definitions(VERSION="${PROJECT_VERSION}" 
    PORTMAP=1 
    _CHANNEL_DB=0
    STDC_HEADERS=1
    LIGO_GDS
    _CONFIG_DYNAMIC
    _ADVANCED_LIGO
    COMPAT_INITIAL_LIGO=1
    #GDS_NO_EPICS
    #GDS_ONLINE
    TARGET=1
    _DEFAULT_SOURCE=1
    _POSIX_C_SOURCE=199506L
    PIC
    _XOPEN_SOURCE=600
    LIBDIR=/usr/lib
    GDS_NODMT
)

#setup any rpc generation

if(APPLE)
#SET( RPCFLAGS -N -C -DPORTMAP -DLIGO_GDS -DGDS_NO_EPICS ${max_chnname_size} -Y $ENV{BUILD_PREFIX}/bin)
#FIND_PROGRAM(RPCGEN_PROG rpcgen)
#add_compile_definitions(
#    u_int=unsigned\ int
#    u_long=unsigned\ long
#    u_short=unsigned\ short
#)
elseif(UNIX)
SET( RPCFLAGS -C -N -M -DPORTMAP -DLIGO_GDS -DGDS_NO_EPICS ${max_chnname_size} -Y $ENV{BUILD_PREFIX}/bin)
FIND_PROGRAM(RPCGEN_PROG rpcgen)
endif()

MESSAGE("Found rpcgen at ${RPCGEN_PROG}")



#    add_compile_definitions(
#	u_int=unsigned\ int 
#        u_long=unsigned\ long 
#        u_short=unsigned\ short 
#    )
if(NOT ONLY_FOTON)

    # get and check path to rpc.h
    find_path(RPC_INCLUDE_DIR rpc/rpc.h
            HINTS /usr/ / ${ABS_INCLUDE_DIR}
            PATH_SUFFIXES  tirpc
            CMAKE_FIND_ROOT_PATH_BOTH)

    MESSAGE(STATUS "RPC_INCLUDE_DIR=${RPC_INCLUDE_DIR}")

    if(NOT RPC_INCLUDE_DIR)
        MESSAGE(FATAL_ERROR "could not find 'rpc.h'")
    endif()

   if(NOT RPCGEN_PROG)
       MESSAGE(FATAL_ERROR "rpcgen command not found")
   endif()

    # determine if we're using tirpc or not
    GET_FILENAME_COMPONENT(RPC_INCLUDE_LASTDIR ${RPC_INCLUDE_DIR} NAME)
    if(${RPC_INCLUDE_LASTDIR} STREQUAL "tirpc")
        SET(USING_TIRPC TRUE)
    else()
        SET(USING_TIRPC FALSE)
    endif()

    MESSAGE(STATUS "USING_TIRPC=${USING_TIRPC}")

    if(${USING_TIRPC} )
        SET(TIRPC_LIBS "tirpc")
    else()
        SET(TIRPC_LIBS "")
    endif()


    SET (GDS_GEN_RPC_HDRS "")
    SET (RPC_BASE gdsrsched rawgapi rtestpoint)
    SET (RPC_OUTPUT_DIR ${CMAKE_BINARY_DIR}/rpc_output)

    set_property(GLOBAL PROPERTY RPC_OUT_FILES "" )
    set_property(GLOBAL PROPERTY RPC_TARGETS "" )

    FILE(MAKE_DIRECTORY ${RPC_OUTPUT_DIR})

    function(CREATE_RPC_TARGET basename)
        SET(OUTPATH ${RPC_OUTPUT_DIR}/${basename})
        SET(INNAME ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x)
        MESSAGE(STATUS "Generating rpc rules for ${INNAME}")
        SET(OUTFILES
            ${OUTPATH}.h
            ${OUTPATH}_svc.c
            ${OUTPATH}_xdr.c
            ${OUTPATH}_clnt.c)
        SET(intfiles
                ${OUTPATH}.h
                ${OUTPATH}_svc.c_
                ${OUTPATH}_xdr.c_
                ${OUTPATH}_clnt.c_)
        add_custom_command(OUTPUT ${OUTFILES}
            COMMAND ${CMAKE_COMMAND} -E remove -f ${intfiles}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -h -o ${basename}.h ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -c -o ${basename}_xdr.c_ ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -l -o ${basename}_clnt.c_ ${INNAME}
            COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -m -o ${basename}_svc.c_ ${INNAME}
            COMMAND ${CMAKE_SOURCE_DIR}/scripts/cleanrpc ${basename}
            MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x
            WORKING_DIRECTORY ${RPC_OUTPUT_DIR}
            )

        SET(libname ${basename}_rpc)
        add_library(${libname} OBJECT
            ${OUTFILES})
        if(APPLE)
            target_compile_options(${libname} PUBLIC -std=gnu99)
            target_compile_definitions(${libname} PUBLIC
        	u_int=unsigned\ int
                u_long=unsigned\ long
                u_short=unsigned\ short
            )
        endif()
        target_include_directories(${libname} PUBLIC
    	${DTT_INCLUDES})
        target_include_directories(${libname} PUBLIC SYSTEM BEFORE ${RPC_INCLUDE_DIR})

        get_property(tmp GLOBAL PROPERTY RPC_OUT_FILES)
        set_property(GLOBAL PROPERTY RPC_OUT_FILES ${tmp} ${OUTFILES} )

        get_property(tmp GLOBAL PROPERTY RPC_TARGETS)
        set_property(GLOBAL PROPERTY RPC_TARGETS ${tmp} ${libname})

    endfunction()

    function(CREATE_RPC_TARGET_NO_SVC basename)
        SET(OUTPATH ${RPC_OUTPUT_DIR}/${basename})
        SET(INNAME ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x)
        MESSAGE(STATUS "Generating rpc rules for ${INNAME}")
        SET(OUTFILES
                ${OUTPATH}.h
                #${OUTPATH}_svc.c
                ${OUTPATH}_xdr.c
                ${OUTPATH}_clnt.c)
        SET(intfiles
                ${OUTPATH}.h
                ${OUTPATH}_svc.c_
                ${OUTPATH}_xdr.c_
                ${OUTPATH}_clnt.c_)
        add_custom_command(OUTPUT ${OUTFILES}
                COMMAND ${CMAKE_COMMAND} -E remove -f ${intfiles}
                COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -h -o ${basename}.h ${INNAME}
                COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -c -o ${basename}_xdr.c_ ${INNAME}
                COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -l -o ${basename}_clnt.c_ ${INNAME}
                COMMAND ${RPCGEN_PROG} ${RPCFLAGS} -m -o ${basename}_svc.c_ ${INNAME}
                COMMAND ${CMAKE_SOURCE_DIR}/scripts/cleanrpc ${basename}
                MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${basename}.x
                WORKING_DIRECTORY ${RPC_OUTPUT_DIR}
                )

        SET(libname ${basename}_rpc)
        add_library(${libname} OBJECT
                ${OUTFILES})
        if(APPLE)
            target_compile_options(${libname} PUBLIC -std=gnu99)
            target_compile_definitions(${libname} PUBLIC
        	u_int=unsigned\ int
                u_long=unsigned\ long
                u_short=unsigned\ short
            )
        endif()
        target_include_directories(${libname} PUBLIC
                ${DTT_INCLUDES})
        target_include_directories(${libname} PUBLIC SYSTEM BEFORE ${RPC_INCLUDE_DIR})
        get_property(tmp GLOBAL PROPERTY RPC_OUT_FILES)
        set_property(GLOBAL PROPERTY RPC_OUT_FILES ${tmp} ${OUTFILES} )

        get_property(tmp GLOBAL PROPERTY RPC_TARGETS)
        set_property(GLOBAL PROPERTY RPC_TARGETS ${tmp} ${libname})

    endfunction(CREATE_RPC_TARGET_NO_SVC)
endif()

SET( DTT_INCLUDES
    ../util
    ../diag
    ../storage
    ../cmd
    ../awg
    ../daq
    ../rmem         
    ../drv
    ../sched
    ../conf    
    ../gpstime
    ../dfm
    ../dfmgui
    ../fantom
    ${EXTERNAL_INCLUDE_DIRECTORIES}
)

#build object libraries
MESSAGE(STATUS "ONLY_FOTON=${ONLY_FOTON}")
if (NOT ONLY_FOTON)
    add_subdirectory(awg)
    add_subdirectory(cmd)
    add_subdirectory(conf)
    add_subdirectory(daq)
    add_subdirectory(drv)
    add_subdirectory(gpstime)
    add_subdirectory(rmem)
    add_subdirectory(sched)
    add_subdirectory(storage)
    add_subdirectory(util)
    add_subdirectory(diag)
    add_subdirectory(lidax)
    add_subdirectory(StripChart)
    add_subdirectory(dmtviewer)
    add_subdirectory(dfm)
    add_subdirectory(fantom)
    add_subdirectory(dfmgui)
    add_subdirectory(foton)
endif()

add_subdirectory(filterwiz)

if (NOT ONLY_FOTON)
    SET(DTT_LIBS
                    conf_lib
                    daq_lib
                    drv_lib
                    sched_lib
                    storage_lib
                    util_lib
                    diag_lib
                    cmd_lib
                    gpstime_lib
            )

    #this probably has to come after all the other libraries to make sure we get all rpc libs
    get_property(RPC_LIBRARIES GLOBAL PROPERTY RPC_TARGETS)
    #building executables
    add_subdirectory(prog)
    add_subdirectory(awgstream)

    #test subdirs
    add_subdirectory(linktest)
    add_subdirectory(gui)
endif()



