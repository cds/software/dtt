add_library(lidax SHARED
		ldxprog.cc
		ldxreport.cc
		TLGLidax.cc
		XsilLidax.cc
		)
target_include_directories(lidax PUBLIC
        ${EXTERNAL_INCLUDE_DIRECTORIES}
        ${DTT_INCLUDES}
        )

target_link_libraries(lidax PUBLIC
        ${ROOT_LIBRARIES}
        fantom
        dttgui
        gdsbase
        dfmgui
        xsil
        )

set_target_properties(lidax PROPERTIES
	VERSION 0.0.1
    PUBLIC_HEADER lidax.hh
    PRIVATE_HEADER
        "ldxprog.hh;ldxreport.hh;ldxtype.hh;TLGLidax.hh;XsilLidax.hh"
        )


INSTALL_LIB(lidax ${INCLUDE_INSTALL_DIR}/gds/lidax)

add_executable(lidax_bin
			lidax.cc
		)

set_target_properties(lidax_bin PROPERTIES OUTPUT_NAME lidax)

set_target_properties(lidax_bin
			PROPERTIES OUTPUT_NAME lidax)

target_include_directories(lidax_bin PUBLIC
		${EXTERNAL_INCLUDE_DIRECTORIES}
        ${DTT_INCLUDES}
		)

target_link_libraries(lidax_bin
		lidax
		xsil
		dfmgui
		)

install(TARGETS lidax_bin
		DESTINATION bin)