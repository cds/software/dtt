add_library(dfm SHARED
        dataacc.cc
        dfm.cc
        dfmfunc.cc
        dfmnds.cc
        dfmsends.cc
        dfmtape.cc
        dfmapi.cc
        dfmfile.cc
        dfmlars.cc
        dfmsm.cc
        udn.cc)

target_include_directories(dfm PRIVATE
        ${CMAKE_SOURCE_DIR}/src/dtt/fantom)

target_link_libraries(dfm PRIVATE
        fantom
        framefast
        pthread)

set_target_properties(dfm
        PROPERTIES
        VERSION 0.0.0
        PUBLIC_HEADER dfm.hh
        PRIVATE_HEADER
            "dfmapi.hh;dataacc.hh;udn.hh;dfmtype.hh;dfmsm.hh;dfmsends.hh;dfmnds.hh;dfmlars.hh;dfmfunc.hh"
        )

INSTALL_LIB(dfm ${INCLUDE_INSTALL_DIR}/gds/dfm)
