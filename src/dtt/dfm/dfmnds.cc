#include "dfmnds.hh"
#include "fchannel.hh"
#include "ndsio.hh"
#include <stdio.h>
#include <cstring>
#include <cstdlib>


namespace dfm {
   using namespace std;
   using namespace fantom;


   // 5s timeout
   static const int _TIMEOUT = 5000000;	
   // Default NDS port
   static const int kNDSPORT = 8088;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmnds							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmnds::dfmnds () : fPort (0)
   {
      fChnPreselFull = true;
   }

//______________________________________________________________________________
   bool dfmnds::open (const std::string& addr, bool read)
   {
      fAddr = addr;
      string::size_type pos = fAddr.find (":");
      if (pos != string::npos) {
         fServer = fAddr.substr (0, pos);
         fPort = atoi (fAddr.c_str() + pos + 1);
      }
      else {
         fServer = fAddr;
         fPort = kNDSPORT;
      }
      for (string::iterator i = fServer.begin(); 
          i != fServer.end(); ++i) {
         *i = tolower (*i);
      }
      while (!fServer.empty() && isspace (fServer[0])) {
         fServer.erase (0, 1);
      }
      while (!fServer.empty() && 
            isspace (fServer[fServer.size()-1])) {
         fServer.erase (fServer.size() - 1);
      }
      return true;
   }

//______________________________________________________________________________
   void dfmnds::close()
   {
      fAddr = "";
   }

//______________________________________________________________________________
   bool dfmnds::requestUDNs (UDNList& udn)
   {
      char buf[1024];
      if (fPort == kNDSPORT) {
         sprintf (buf, "nds://%s", fServer.c_str());
      }
      else {
         sprintf (buf, "nds://%s:%d", fServer.c_str(), fPort);
      }
      string s;
      //udn.clear();
      s = buf; s+= "/frames";
      udn.insert (UDNList::value_type (UDN (s.c_str()), UDNInfo()));
      s = buf; s+= "/trend";
      udn.insert (UDNList::value_type (UDN (s.c_str()), UDNInfo()));
      s = buf; s+= "/minute-trend";
      udn.insert (UDNList::value_type (UDN (s.c_str()), UDNInfo()));
      return true;
   }

//______________________________________________________________________________
   bool dfmnds::requestUDNInfo (const UDN& udn, UDNInfo& info)
   {
      // build UDN info
      //cerr << "ask for UDN info" << endl;
      frametype utype = NONE;
      const char* p = udn;
      if ((p == 0) || (strncasecmp (p, "nds://", 6) != 0)) {
         return false;
      }
      p += 6;
      while (*p && (*p != '/')) p++;
      if (strcasecmp (p, "/frames") == 0) {
         utype = FF;
      }
      else if (strcasecmp (p, "/trend") == 0) {
         utype = STF;
      }
      else if (strcasecmp (p, "/minute-trend") == 0) {
         utype = MTF;
      }
      else {
         return false;
      }
      UDNInfo uinfo;
      uinfo.setType (utype);
      // get channel names
      channellist chns;
      if (!nds_support::getChannels (fServer.c_str(), fPort, chns, utype)) {
         return false;
      }
      uinfo.setChannels (chns);
      channellist exc_chns;
      if (!nds_support::getExcitationChannels(exc_chns, chns)) {
        return false;
      }
      uinfo.setExcitationChannels(exc_chns);
      // get times
      Time start;
      Time stop;
      //cerr << "ask for time" << endl;
      if (!nds_support::getTimes (fServer.c_str(), fPort, start, 
                           stop, utype)) {
         return false;
      }
      //cerr << "ask for time done" << endl;
      if (stop > start) {
         uinfo.insertDSeg (start, stop - start);
      }
      info = uinfo;
      return true;
   }


}
