SET(diaglinkages


    # project object libraries
    cmd_lib
    conf_lib
    daq_lib
    gpstime_lib
    sched_lib
    storage_lib
    util_lib
    diag_lib

    # project shared libaries
    awg
    fantom
    dfm
    excitation
    testpointmgr

    # rpc libraries
    rgdsmsg_rpc
    rgdsmsgtype_rpc
    rgdsmsgcb_rpc

    # nds lies
    ndscxx

    # gds libraries
    # gdsbase
    # daqs
    dmtsigp
    gdsbase
    gdsalgo
    gdscntr

    # lsc libraries
    framefast
    frameutil

    #system libraries
    ${READLINE_LIBRARIES}
    dl
    z
    m
    fftw3
    fftw3f
    expat
    pthread
    curl
    sasl2
)

add_library(dtt SHARED
        )

set_target_properties( dtt
        PROPERTIES
        VERSION 1.0.2
)

target_link_libraries(dtt
        ${diaglinkages}
        ${TIRPC_LIBS}
        )

INSTALL_LIB(dtt)


#### DIAG ####
add_executable(diag
    diag.cc
)

target_link_libraries(diag
    dtt
)

target_include_directories(diag PRIVATE
    ${DTT_INCLUDES}
)


#### DIAGD ####
add_executable(diagd
        diagd.cc
        )

target_link_libraries(diagd
        dtt
        )

target_include_directories(diagd PRIVATE
        ${DTT_INCLUDES}
        )


#### CHNDUMP ####
add_executable(chndump
        chndump.c
        )

target_link_libraries(chndump
        dtt
        )

target_include_directories(chndump PRIVATE
        ${DTT_INCLUDES}
        )


##### gdserrd ####
#add_executable(gdserrd
#        gdserrd.c
#        )
#
#target_link_libraries(gdserrd
#        ${diaglinkages}
#        )
#
#target_include_directories(gdserrd PRIVATE
#        ${DTT_INCLUDES}
#        )

#### tpcmd ####
add_executable(tpcmd
        tpcmd.c
        )

target_link_libraries(tpcmd
        awg
        )

target_include_directories(tpcmd PRIVATE
        ${DTT_INCLUDES}
        )

#### xmlconv ####
add_executable(xmlconv
        xmlconv.cc
        )

target_link_libraries(xmlconv
        dtt
        )

target_include_directories(xmlconv PRIVATE
        ${DTT_INCLUDES}
        )


#### xmldata ####
add_executable(xmldata
        xmldata.cc
        )

target_link_libraries(xmldata
        dtt
        )

target_include_directories(xmldata PRIVATE
        ${DTT_INCLUDES}
        )

#### xmldir ####
add_executable(xmldir
        xmldir.cc
        )

target_link_libraries(xmldir
        dtt
        )

target_include_directories(xmldir PRIVATE
        ${DTT_INCLUDES}
        )


install(TARGETS diag diagd chndump tpcmd xmlconv xmldata xmldir
        DESTINATION bin
        )

### InspiralRange
add_executable(InspiralRange
        InspiralRange.cc)

install(TARGETS InspiralRange
        DESTINATION bin)
