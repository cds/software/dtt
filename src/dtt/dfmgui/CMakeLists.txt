add_library(dfmgui SHARED
        dfmgui.cc
        TLGDfmChannels.cc
        TLGDfmMonitors.cc
        TLGDfmSel.cc
        TLGDfmServer.cc
        TLGDfmTimes.cc
        TLGDfmUdn.cc )
target_include_directories(dfmgui PUBLIC
        #${EXTERNAL_INCLUDE_DIRECTORIES}
        ${CMAKE_CURRENT_SOURCE_DIR}/../gui/dttgui
        ${CMAKE_CURRENT_SOURCE_DIR}/../dfm
        ${CMAKE_CURRENT_SOURCE_DIR}/../fantom
        )

target_link_libraries(dfmgui PUBLIC
        dttgui
        dfm
        fantom
        frameutil
        parsl
        framefast
        daqs
        sasl2
        lsmp
        sockutil
        gdsbase
        z
        pthread
        gdsalgo
        fftw3f
        fftw3
        readline
        tinfo
        ${ROOT_LIBRARIES}
        resolv
        lzma
        pcre
        crypto
        ssl
        freetype
        png
        )

set_target_properties( dfmgui
        PROPERTIES
        VERSION 0.0.0
        PUBLIC_HEADER dfmgui.hh
        PRIVATE_HEADER
            "montype.hh;TLGDfmChannels.hh;TLGDfmMonitors.hh;TLGDfmSel.hh;TLGDfmServer.hh;TLGDfmTimes.hh;TLGDfmUdn.hh"
        )

INSTALL_LIB(dfmgui ${INCLUDE_INSTALL_DIR}/gds/dfm)

add_executable(dfmguitest
        dfmguitest.cc
        )

target_link_libraries(dfmguitest
        dfmgui)

# skipping creation of libRdfmgui ROOT dictionary, since it's not produced in GDS.

#install(TARGETS dfmguitest
#        DESTINATION bin)
