//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmTimes							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGDfmTimes.hh"
#include "TLGEntry.hh"
#include <TVirtualX.h>
#include <time.h>

namespace dfm {
   using namespace std;
   using namespace dttgui;
   using namespace fantom;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmTimeLayout						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGDfmTimeLayout : public TGLayoutManager {
   public:
      TLGDfmTimeLayout (TGCompositeFrame* p, Int_t lines) 
      : fMain (p), fLines (lines) {
         fList = fMain->GetList(); }
      virtual void Layout ();
      virtual TGDimension GetDefaultSize() const {
         return TGDimension (545, 60 + 25 * fLines); }
   
   protected:
      TGCompositeFrame*	fMain;
      TList*		fList;
      Int_t 		fLines;
   };

//______________________________________________________________________________
   void TLGDfmTimeLayout::Layout ()
   {
      const Int_t x[] = {10, 100, 450};
      const Int_t w[] = {80, 330, 80};
      const Int_t y[] = {0, 35, 60, 95, 120, 155, 180};
      TGFrameElement* ptr;
      Int_t col = 0;
      Int_t row = 0;
      TIter next (fList);
      while ((ptr = (TGFrameElement*) next())) {
         Int_t ww = w[col];
         if (col == 0) {
            ww = ptr->fFrame->GetWidth();
         }
         ptr->fFrame->MoveResize (x[col], y[row]+20, ww, 22);
         if (++col >= 3) {
            row++; col = 0;
         }
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmTimeSelDlg						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmTimeSelDlg::TLGDfmTimeSelDlg (const TGWindow* pwin, 
                     const TGWindow* main, dataserver& ds, const UDNList& udnsel,
                     Time& start, Interval& duration, Bool_t& ret)
   : TLGTransientFrame (pwin, main, 10, 10), fDS (&ds), fUDNret (&udnsel), 
   fT0 (&start), fDt (&duration), fOk (&ret), fStartUTCDirty (false),
   fStopUTCDirty (false)
   {
      SetDSegments ();
      // Layout hints
      fL[0] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 0, 0, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 4, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 12, 2, 10, 0);
      fL[5] = new TGLayoutHints (kLHintsRight | kLHintsTop, 2, 12, 10, 0);
      fL[6] = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           55, 55, 2, 2);
      fL[7] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 0, 0, 0, 0);
      fL[8] = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           40, 40, 2, 1);
      fL[9] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY |
                           kLHintsExpandX, 2, 2, 0, 0);
      // time segment list
      fG[1] = new TGGroupFrame (this, "List");
      AddFrame (fG[1], fL[0]);
      fF[7] = new TGHorizontalFrame (fG[1], 10, 10);
      fG[1]->AddFrame (fF[7], fL[7]);
      fTimeSeg = new TGListView (fF[7], 300, 250);
      fTimeSeg->SetHeaders (6);
      fTimeSeg->SetHeader ("  Seg", kTextLeft, kTextLeft, 0);
      fTimeSeg->SetHeader ("  Start Date ", kTextRight, kTextRight, 1);
      fTimeSeg->SetHeader ("  Time ", kTextRight, kTextRight, 2);
      fTimeSeg->SetHeader ("  Stop Date ", kTextRight, kTextRight, 3);
      fTimeSeg->SetHeader ("  Time ", kTextRight, kTextRight, 4);
      fTimeSeg->SetHeader ("  Duration ", kTextRight, kTextRight, 5);
      fTSCon = new TGLVContainer (fTimeSeg->GetViewPort(), 
                           10, 10, kHorizontalFrame, fgWhitePixel);
      fTimeSeg->GetViewPort()->SetBackgroundColor (fgWhitePixel);
      fTimeSeg->SetContainer (fTSCon);
      fTimeSeg->SetViewMode (kLVDetails);
      // const TGPicture* fPics = fClient->GetPicture ("folder_s.xpm");
      // const TGPicture* fPict = fClient->GetPicture ("folder_t.xpm");
      const TGPicture* fPics = fClient->GetPicture ("arrow_right.xpm");
      const TGPicture* fPict = fClient->GetPicture ("arrow_right.xpm");
      int count = 0;
      for (UDNInfo::dsegiter i = fDSeg.beginDSeg(); 
          i != fDSeg.endDSeg(); ++i) {
         Time T0 = i->first;
         Interval dt = i->second;
         Time T1 = T0 + dt;
         char buf[1024];
         TGString** list = new TGString* [6];
         TimeStr (T0, buf, "%d %M %Y");
         list[0] = new TGString (buf);
         TimeStr (T0, buf, "%H:%N:%S");
         list[1] = new TGString (buf);
         TimeStr (T1, buf, "%d %M %Y");
         list[2] = new TGString (buf);
         TimeStr (T1, buf, "%H:%N:%S");
         list[3] = new TGString (buf);
         int sec = (int) ((double)dt + 0.5);
         sprintf (buf, "%i", sec);
         list[4] = new TGString (buf);
         list[5] = 0;
         sprintf (buf, "%03i", ++count);
         TGLVEntry* e = new TGLVEntry 
            (fTSCon, fPics, fPict, new TGString (buf), list, kLVDetails);
         e->SetUserData (&*i);
         fTSCon->AddItem (e);
      }
      fF[7]->AddFrame (fTimeSeg, fL[0]);
      fF[6] = new TGHorizontalFrame (fG[1], 10, 10);
      fG[1]->AddFrame (fF[6], fL[7]);
      fSetStart = new TGTextButton (fF[6], "Set start", kDfmTimeSet);
      fSetStart->Associate (this);
      fF[6]->AddFrame (fSetStart, fL[8]);
      fSetBoth = new TGTextButton (fF[6], "Set both", kDfmTimeSet + 2);
      fSetBoth->Associate (this);
      fF[6]->AddFrame (fSetBoth, fL[8]);
      fSetStop = new TGTextButton (fF[6], "Set stop", kDfmTimeSet + 1);
      fSetStop->Associate (this);
      fF[6]->AddFrame (fSetStop, fL[8]);
      // time selction
      fG[0] = new TGGroupFrame (this, "Selected");
      AddFrame (fG[0], fL[0]);
      fG[0]->SetLayoutManager (new TLGDfmTimeLayout (fG[0], 6));
         // line 0
      fLabel[18] = new TGLabel (fG[0], "Type: ");
      fG[0]->AddFrame (fLabel[18], fL[1]);
      fF[5] = new TGHorizontalFrame (fG[0], 10, 10);
      fG[0]->AddFrame (fF[5], fL[1]);
      for (int i = 0; i < 3; i++) {
         const char* const kType[3] =
            {"Start/Duration   ", "Start/Stop   ", "Stop/Duration"};
         fType[i] = new TGRadioButton (fF[5], kType[i], kDfmTimeSelType + i);
         fType[i]->Associate (this);
         fF[5]->AddFrame (fType[i], fL[2]);
      }
      fLabel[19] = new TGLabel (fG[0], "");
      fG[0]->AddFrame (fLabel[19], fL[1]);
         // line 1
      fLabel[0] = new TGLabel (fG[0], "Start GPS: ");
      fG[0]->AddFrame (fLabel[0], fL[1]);
      fF[0] = new TGHorizontalFrame (fG[0], 10, 10);
      fG[0]->AddFrame (fF[0], fL[1]);
      fTimeGPS = new TLGNumericControlBox (fF[0], 0., 12, kDfmTimeGPS,
                           kNESInteger, kNEANonNegative);
      fTimeGPS->Associate (this);
      fF[0]->AddFrame (fTimeGPS, fL[2]);
      fLabel[1] = new TGLabel (fF[0], "sec        ");
      fF[0]->AddFrame (fLabel[1], fL[2]);
      fTimeGPSN = new TLGNumericControlBox (fF[0], 0., 12, kDfmTimeGPSN,
                           kNESInteger, kNEANonNegative, kNELLimitMinMax,
                           0, 999999999);
      fTimeGPSN->Associate (this);
      fF[0]->AddFrame (fTimeGPSN, fL[2]);
      fLabel[2] = new TGLabel (fF[0], "nsec");
      fF[0]->AddFrame (fLabel[2], fL[2]);
      fLabel[3] = new TGLabel (fG[0], "");
      fG[0]->AddFrame (fLabel[3], fL[2]);
         // line 2
      fLabel[4] = new TGLabel (fG[0], "       UTC: ");
      fG[0]->AddFrame (fLabel[4], fL[1]);
      fF[1] = new TGHorizontalFrame (fG[0], 10, 10);
      fG[0]->AddFrame (fF[1], fL[1]);
      fTimeDate = new TLGNumericControlBox (fF[1], 0., 12, kDfmTimeDate,
                           kNESDayMYear);
      fTimeDate->Associate (this);
      fF[1]->AddFrame (fTimeDate, fL[2]);
      fLabel[5] = new TGLabel (fF[1], "dd/mm/yy   ");
      fF[1]->AddFrame (fLabel[5], fL[2]);
      fTimeTime = new TLGNumericControlBox (fF[1], 0., 10, kDfmTimeTime,
                           kNESHourMinSec, kNEANonNegative);
      fTimeTime->Associate (this);
      fF[1]->AddFrame (fTimeTime, fL[2]);
      fLabel[6] = new TGLabel (fF[1], "hh:mm:ss");
      fF[1]->AddFrame (fLabel[6], fL[2]);
      fTimeNow = new TGTextButton (fG[0], new TGHotString
                           (" &Now "), kDfmTimeNow);
      fTimeNow->Associate (this);
      fG[0]->AddFrame (fTimeNow, fL[1]);
         // line 3
      fLabel[7] = new TGLabel (fG[0], "Stop GPS: ");
      fG[0]->AddFrame (fLabel[7], fL[1]);
      fF[2] = new TGHorizontalFrame (fG[0], 10, 10);
      fG[0]->AddFrame (fF[2], fL[1]);
      fStopGPS = new TLGNumericControlBox (fF[2], 0., 12, kDfmStopGPS,
                           kNESInteger, kNEANonNegative);
      fStopGPS->Associate (this);
      fF[2]->AddFrame (fStopGPS, fL[2]);
      fLabel[8] = new TGLabel (fF[2], "sec        ");
      fF[2]->AddFrame (fLabel[8], fL[2]);
      fStopGPSN = new TLGNumericControlBox (fF[2], 0., 12, kDfmStopGPSN,
                           kNESInteger, kNEANonNegative, kNELLimitMinMax,
                           0, 999999999);
      fStopGPSN->Associate (this);
      fF[2]->AddFrame (fStopGPSN, fL[2]);
      fLabel[9] = new TGLabel (fF[2], "nsec");
      fF[2]->AddFrame (fLabel[9], fL[2]);
      fLabel[10] = new TGLabel (fG[0], "");
      fG[0]->AddFrame (fLabel[10], fL[2]);
         // line 4
      fLabel[11] = new TGLabel (fG[0], "       UTC: ");
      fG[0]->AddFrame (fLabel[11], fL[1]);
      fF[3] = new TGHorizontalFrame (fG[0], 10, 10);
      fG[0]->AddFrame (fF[3], fL[1]);
      fStopDate = new TLGNumericControlBox (fF[3], 0., 12, kDfmStopDate,
                           kNESDayMYear);
      fStopDate->Associate (this);
      fF[3]->AddFrame (fStopDate, fL[2]);
      fLabel[12] = new TGLabel (fF[3], "dd/mm/yy   ");
      fF[3]->AddFrame (fLabel[12], fL[2]);
      fStopTime = new TLGNumericControlBox (fF[3], 0., 10, kDfmStopTime,
                           kNESHourMinSec, kNEANonNegative);
      fStopTime->Associate (this);
      fF[3]->AddFrame (fStopTime, fL[2]);
      fLabel[13] = new TGLabel (fF[3], "hh:mm:ss");
      fF[3]->AddFrame (fLabel[13], fL[2]);
      fStopNow = new TGTextButton (fG[0], new TGHotString
                           (" &Now "), kDfmStopNow);
      fStopNow->Associate (this);
      fG[0]->AddFrame (fStopNow, fL[1]);
         // line 5
      fLabel[14] = new TGLabel (fG[0], "Duration: ");
      fG[0]->AddFrame (fLabel[14], fL[1]);
      fF[4] = new TGHorizontalFrame (fG[0], 10, 10);
      fG[0]->AddFrame (fF[4], fL[1]);
      fDuration = new TLGNumericControlBox (fF[4], 0., 12, kDfmDuration,
                           kNESInteger, kNEANonNegative);
      fDuration->Associate (this);
      fF[4]->AddFrame (fDuration, fL[2]);
      fLabel[15] = new TGLabel (fF[4], "sec        ");
      fF[4]->AddFrame (fLabel[15], fL[2]);
      fDurationN = new TLGNumericControlBox (fF[4], 0., 12, kDfmDurationN,
                           kNESInteger, kNEANonNegative, kNELLimitMinMax,
                           0, 999999999);
      fDurationN->Associate (this);
      fF[4]->AddFrame (fDurationN, fL[2]);
      fLabel[16] = new TGLabel (fF[4], "nsec");
      fF[4]->AddFrame (fLabel[16], fL[2]);
      fLabel[17] = new TGLabel (fG[0], "");
      fG[0]->AddFrame (fLabel[17], fL[1]);
      // button group
      fF[0] = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fF[0], fL[0]);
      fOkButton = new TGTextButton (fF[0], 
                           new TGHotString ("&Ok"), 1);
      fOkButton->Associate (this);
      fF[0]->AddFrame (fOkButton, fL[6]);
      fCancelButton = new TGTextButton (fF[0], 
                           new TGHotString ("&Cancel"), 0);
      fCancelButton->Associate (this);
      fF[0]->AddFrame (fCancelButton, fL[6]);
      // set values
      SetType (0);
      SetStartTime (start);
      SetStopTime (start + duration);
      SetDuration (duration);
   
      // set dialog box title
      SetWindowName ("Time Selection");
      SetIconName ("Time Selection");
      SetClassHints ("TimeSelectionDlg", "TimeSelectionDlg");
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmTimeSelDlg::~TLGDfmTimeSelDlg ()
   {
      delete fCancelButton;
      delete fOkButton;
      delete fDurationN;
      delete fDuration;
      delete fStopNow;
      delete fStopTime;
      delete fStopDate;
      delete fStopGPSN;
      delete fStopGPS;
      delete fTimeNow;
      delete fTimeTime;
      delete fTimeDate;
      delete fTimeGPSN;
      delete fTimeGPS;
      for (int i = 0; i < 3; i++) {
         delete fType[i];
      }
      delete fSetStop;
      delete fSetBoth;
      delete fSetStart;
      delete fTSCon;
      delete fTimeSeg;
      for (int i = 0; i < 20; i++) {
         delete fLabel[i];
      }
      for (int i = 0; i < 8; i++) {
         delete fF[i];
      }
      for (int i = 0; i < 2; i++) {
         delete fG[i];
      }
      for (int i = 0; i < 10; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmTimeSelDlg::SetDSegments ()
   {
      if (fUDNret->size() >= 1) {
         UDNInfo* udn = fDS->get (fUDNret->begin()->first);
         if (udn) fDSeg = *udn;
      }
   }

//______________________________________________________________________________
   void TLGDfmTimeSelDlg::SetStartTime (const Time& T0)
   {
      time_t t = getUTC (T0);
      tm utc;
      gmtime_r (&t, &utc);
      utc.tm_year += 1900;
      utc.tm_mon++;
      fTimeGPS->SetIntNumber (T0.getS());
      fTimeGPSN->SetIntNumber (T0.getN());
      fTimeDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
      fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
   }

//______________________________________________________________________________
   void TLGDfmTimeSelDlg::SetStopTime (const Time& T1)
   {
      time_t t = getUTC (T1);
      tm utc;
      gmtime_r (&t, &utc);
      utc.tm_year += 1900;
      utc.tm_mon++;
      fStopGPS->SetIntNumber (T1.getS());
      fStopGPSN->SetIntNumber (T1.getN());
      fStopDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
      fStopTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
   }

//______________________________________________________________________________
   void TLGDfmTimeSelDlg::SetDuration (const Interval& Dt)
   {
      fDuration->SetIntNumber (Dt.GetS());
      fDurationN->SetIntNumber (Dt.GetN());
   }

//______________________________________________________________________________
   void TLGDfmTimeSelDlg::SetType (int sel)
   {
      for (int i = 0; i < 3; i++) {
         fType[i]->SetState (i == sel ? kButtonDown : kButtonUp);
      }
   }

//______________________________________________________________________________
   void TLGDfmTimeSelDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGDfmTimeSelDlg::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // get values
                  int sel = 0;
                  if (fType[1]->GetState() == kButtonDown) sel = 1;
                  if (fType[2]->GetState() == kButtonDown) sel = 2;
                  if (fStartUTCDirty) {
                     Long_t msg = MK_MSG 
                        (kC_TEXTENTRY, (EWidgetMessageTypes)kTE_TEXTUPDATED);
                     ProcessMessage (msg, kDfmTimeDate, 0);
                  
                  }
                  if (fStopUTCDirty) {
                     Long_t msg = MK_MSG 
                        (kC_TEXTENTRY, (EWidgetMessageTypes)kTE_TEXTUPDATED);
                     ProcessMessage (msg, kDfmStopDate, 0);
                  }
                  Time T0 (fTimeGPS->GetIntNumber(), 
                          fTimeGPSN->GetIntNumber());
                  Time T1 (fStopGPS->GetIntNumber(), 
                          fStopGPSN->GetIntNumber());
                  Interval Dt (fDuration->GetIntNumber(), 
                              fDurationN->GetIntNumber());
                  if (fType[1]->GetState() == kButtonDown) {
                     Dt = T1 - T0;
                     if (Dt < Interval (0.)) Dt = Interval (1, 0);
                  }
                  else if (fType[2]->GetState() == kButtonDown) {
                     T0 = T1 - Dt;
                     if (T0 > T1) T0 = Time(0);
                  }
                  // set ret value
                  if (fOk) *fOk = kTRUE;
                  *fT0 = T0;
                  *fDt = Dt;
                  DeleteWindow();
                  break;
               }
            // Set start time
            case kDfmTimeSet:
               {
                  void* cur = 0;
                  const TGLVEntry* e = dynamic_cast<const TGLVEntry*>(
                                       fTSCon->GetNextSelected (&cur));
                  if (e != 0) {
                     UDNInfo::dataseglist::value_type* seg = 
                        (UDNInfo::dataseglist::value_type*)e->GetUserData();
                     SetStartTime (seg->first);
                     if (fType[2]->GetState() == kButtonDown) {
                        SetType (1);
                     }
                  }
                  break;
               }
            // Set stop time
            case kDfmTimeSet + 1:
               {
                  void* cur = 0;
                  const TGLVEntry* e = dynamic_cast<const TGLVEntry*>(
                                       fTSCon->GetNextSelected (&cur));
                  if (e != 0) {
                     UDNInfo::dataseglist::value_type* seg = 
                        (UDNInfo::dataseglist::value_type*)e->GetUserData();
                     SetStopTime (seg->first + seg->second);
                     if (fType[0]->GetState() == kButtonDown) {
                        SetType (1);
                     }
                  }
                  break;
               }
            // Set start & stop time
            case kDfmTimeSet + 2:
               {
                  void* cur = 0;
                  const TGLVEntry* e = dynamic_cast<const TGLVEntry*>(
                                       fTSCon->GetNextSelected (&cur));
                  if (e != 0) {
                     UDNInfo::dataseglist::value_type* seg = 
                        (UDNInfo::dataseglist::value_type*)e->GetUserData();
                     SetStartTime (seg->first);
                     SetStopTime (seg->first + seg->second);
                     SetDuration (seg->second);
                     SetType (1);
                  }
                  break;
               }
            // Time now
            case kDfmTimeNow:
               {
                  Time now = Now();
                  now.setN (0);
                  SetStartTime (now);
                  break;
               }
            // Time now (stop)
            case kDfmStopNow:
               {
                  Time now = Now();
                  now.setN (0);
                  SetStopTime (now);
                  break;
               }
         }
      }
      // Radio buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         switch (parm1) {
            // Time selection
            case kDfmTimeSelType:
            case kDfmTimeSelType + 1:
            case kDfmTimeSelType + 2:
               {
                  int sel = parm1 - kDfmTimeSelType;
                  for (int i = 0; i < 3; i++) {
                     if (i != sel) fType[i]->SetState (kButtonUp);
                  }
                  break;
               }
         }
      }
      // Text entry
      if ((GET_MSG (msg) == kC_TEXTENTRY) &&
         (GET_SUBMSG (msg) == kTE_TEXTCHANGED)) {
         switch (parm1) {
            // GPS seconds have changed
            case kDfmTimeGPS:
            case kDfmTimeGPSN:
               {
                  fStartUTCDirty = false;
                  break;
               }
            // UTC time has changed
            case kDfmTimeDate:
            case kDfmTimeTime:
               {
                  fStartUTCDirty = true;
                  break;
               }
            // GPS seconds have changed (stop)
            case kDfmStopGPS:
            case kDfmStopGPSN:
               {
                  fStopUTCDirty = false;
                  break;
               }
            // UTC time has changed
            case kDfmStopDate:
            case kDfmStopTime:
               {
                  fStopUTCDirty = true;
                  break;
               }
         }
      }
      // Text entry update
      if ((GET_MSG (msg) == kC_TEXTENTRY) &&
         (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
         switch (parm1) {
            // GPS seconds have changed
            case kDfmTimeGPS:
               {
                  // set UTC start time
                  Time start = Time (fTimeGPS->GetIntNumber(), 0);
                  time_t t = getUTC (start);
                  tm utc;
                  gmtime_r (&t, &utc);
                  utc.tm_year += 1900;
                  utc.tm_mon++;
                  fTimeDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
                  fTimeTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  break;
               }
            // GPS nano-seconds have changed
            case kDfmTimeGPSN:
               {
                  break;
               }
            // UTC time has changed
            case kDfmTimeDate:
            case kDfmTimeTime:
               {
                  // set GPS seconds
                  tm utc;
                  memset (&utc, 0, sizeof (tm));
                  fTimeDate->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
                  fTimeTime->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  utc.tm_year -= 1900;
                  utc.tm_mon--;
                  time_t t = mktime (&utc);
                  Time start = fromUTC (t);
                  t = 0;
                  start -= mktime (gmtime_r (&t, &utc));
                  fTimeGPS->SetIntNumber (start.getS());
                  break;
               }
            // GPS seconds have changed (stop)
            case kDfmStopGPS:
               {
                  // set UTC start time
                  Time start = Time (fStopGPS->GetIntNumber(), 0);
                  time_t t = getUTC (start);
                  tm utc;
                  gmtime_r (&t, &utc);
                  utc.tm_year += 1900;
                  utc.tm_mon++;
                  fStopDate->SetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
                  fStopTime->SetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  break;
               }
            // GPS nano-seconds have changed
            case kDfmStopGPSN:
               {
                  break;
               }
            // UTC time has changed
            case kDfmStopDate:
            case kDfmStopTime:
               {
                  // set GPS seconds
                  tm utc;
                  memset (&utc, 0, sizeof (tm));
                  fStopDate->GetDate (utc.tm_year, utc.tm_mon, utc.tm_mday);
                  fStopTime->GetTime (utc.tm_hour, utc.tm_min, utc.tm_sec);
                  utc.tm_year -= 1900;
                  utc.tm_mon--;
                  time_t t = mktime (&utc);
                  Time start = fromUTC (t);
                  t = 0;
                  start -= mktime (gmtime_r (&t, &utc));
                  fStopGPS->SetIntNumber (start.getS());
                  break;
               }
         }
      }
      return kTRUE;
   }


}
