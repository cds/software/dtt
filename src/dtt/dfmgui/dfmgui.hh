/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmguiroot						*/
/*                                                         		*/
/* Module Description: ROOT GUI for DFM					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmguiroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_DFMGUIROOT_H
#define _LIGO_DFMGUIROOT_H


#include "dataacc.hh"

   class TGWindow;


namespace dfm {


/** @name ROOT GUI for DFM 
    This header defines functions for setting up a dialogbox which
    lets the user select data server, UDN, channels, etc.
   
    @memo ROOT GUI for DFM
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Dialogbox for setting up data access servers and UDNs.
    @memo ROOT GUI dialogbox for DFM.
    @param p Parent window
    @param main Main window
    @param dacc Data access object
    @param channelsel Shows a channel selection widget
    @param timesel Shows a time selection widget
    @param stagingsel Shows a staging selection widget
    @return true if successful
 ************************************************************************/
   bool dfmDlg (const TGWindow* p, const TGWindow* main,
               dataaccess& dacc, bool channelsel = true,
               bool timesel = true, bool stagingsel = true);

//@}

}


#endif // _LIGO_DFMGUIROOT_H
