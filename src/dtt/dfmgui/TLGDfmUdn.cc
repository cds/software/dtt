/* version $Id: TLGDfmUdn.cc 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 4; -*- */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmdlgroot							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGDfmUdn.hh"
#include "TLGEntry.hh"
#include "dirio.hh"
#include "dmtio.hh"
#include "tapeio.hh"
#include <TSystem.h>
#include <TGMsgBox.h>
#include <TVirtualX.h>
#include <iostream>
#include <cstdlib>

namespace dfm {
   using namespace std;
   using namespace dttgui;
   using namespace fantom;


   static const char* const gFrameTypes[] = { 
   "Frame files", "*.gwf",
   "Frame files", "*.F",
   "Second trend", "*.T",
   "Minute trend", "*.mT",
   "Text files", "*.txt",
   "UDN list", "*.udn",
   "All files", "*",
   0, 0 };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmUDNDirDlg						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmUDNDirDlg::TLGDfmUDNDirDlg (const TGWindow *p, 
                     const TGWindow *main, UDN& udn, 
                     Bool_t sourcesel, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fSourceSel (sourcesel), 
   fUDN (&udn), fOk (&ret)
   {
      fName = 0;
      fFileButton = 0;
      fEnableButton = 0;
      fStartButton[0] = 0;
      fStartButton[1] = 0;
      fStart[0] = 0;
      fStart[1] = 0;
      fStopButton[0] = 0;
      fStopButton[1] = 0;
      fStop[0] = 0;
      fStop[1] = 0;
      fOkButton = 0;
      fCancelButton = 0;
      for (int i = 0; i < 3; i++) {
         fLabel[i] = 0;
      }
      for (int i = 0; i < 7; i++) {
         fF[i] = 0;
      }
      for (int i = 0; i < 7; i++) {
         fL[i] = 0;
      }
   
      UDN u (udn);
      if (!u || u.empty()) {
         if (!ChooseFile (u)) {
            ret = false;
            delete this;
            return;
         }
      }
      fL[0] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 4, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[3] = new TGLayoutHints (kLHintsTop | kLHintsLeft | 
                           kLHintsExpandX, 70, 70, 10, 2);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 2, 1, 1);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 10, 2, 0, 0);
      // Directory group
      fF[0] = new TGGroupFrame (this, "Directory/File");
      AddFrame (fF[0], fL[0]);
      fF[1] = new TGHorizontalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[1], fL[1]);
      fLabel[0] = new TGLabel (fF[1], "Name: ");
      fF[1]->AddFrame (fLabel[0], fL[4]);
      fName = new TLGTextEntry (fF[1], "", 100);
      fName->Associate (this);
      fName->Resize (330, 23);
      fF[1]->AddFrame (fName, fL[2]);
      fFileButton = new TGTextButton (fF[1], 
                           new TGHotString ("    &Choose...    "), 100);
      fFileButton->Associate (this);
      fF[1]->AddFrame (fFileButton, fL[6]);
      // Autonumbering group
      fF[2] = new TGGroupFrame (this, "Auto-numbering");
      AddFrame (fF[2], fL[0]);
         // Enable
      fF[3] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[3], fL[1]);
      fEnableButton = new TGCheckButton (fF[3], "Enable", 101);
      fEnableButton->Associate (this);
      fF[3]->AddFrame (fEnableButton, fL[4]);
         // Start
      fF[4] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[4], fL[5]);
      fLabel[1] = new TGLabel (fF[4], "Start:   ");
      fF[4]->AddFrame (fLabel[1], fL[4]);
      fStartButton[0] = new TGCheckButton (fF[4], "Directory ", 102);
      fStartButton[0]->Associate (this);
      fF[4]->AddFrame (fStartButton[0], fL[4]);
      fStart[0] = new TLGNumericControlBox (fF[4], 0., 4, 
                           103, kNESInteger, kNEANonNegative);;
      fStart[0]->Associate (this);
      fF[4]->AddFrame (fStart[0], fL[4]);
      fStartButton[1] = new TGCheckButton (fF[4], "File ", 104);
      fStartButton[1]->Associate (this);
      fF[4]->AddFrame (fStartButton[1], fL[6]);
      fStart[1] = new TLGNumericControlBox (fF[4], 0., 4, 
                           105, kNESInteger, kNEANonNegative);;
      fStart[1]->Associate (this);
      fF[4]->AddFrame (fStart[1], fL[4]);
         // Stop or Number of files
      fF[5] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[5], fL[5]);
      if (fSourceSel) {
         fLabel[2] = new TGLabel (fF[5], "Stop:   ");
         fF[5]->AddFrame (fLabel[2], fL[4]);
         fStopButton[0] = new TGCheckButton (fF[5], "Directory ", 106);
         fStopButton[0]->Associate (this);
         fF[5]->AddFrame (fStopButton[0], fL[4]);
         fStop[0] = new TLGNumericControlBox (fF[5], 0., 4, 
                              107, kNESInteger, kNEANonNegative);;
         fStop[0]->Associate (this);
         fF[5]->AddFrame (fStop[0], fL[4]);
         fStopButton[1] = new TGCheckButton (fF[5], "File ", 108);
         fStopButton[1]->Associate (this);
         fF[5]->AddFrame (fStopButton[1], fL[6]);
         fStop[1] = new TLGNumericControlBox (fF[5], 0., 4, 
                              109, kNESInteger, kNEANonNegative);;
         fStop[1]->Associate (this);
         fF[5]->AddFrame (fStop[1], fL[4]);
      }
      else {
         fLabel[2] = 0;
         fStopButton[0] = new TGCheckButton (fF[5],
                              "Number of files per directory ", 106);
         fStopButton[0]->Associate (this);
         fF[5]->AddFrame (fStopButton[0], fL[4]);
         fStop[0] = new TLGNumericControlBox (fF[5], 0., 6, 
                              107, kNESInteger, kNEANonNegative);;
         fStop[0]->Associate (this);
         fF[5]->AddFrame (fStop[0], fL[4]);
         fStopButton[1] = 0;
         fStop[1] = 0;
      } 
      // button group
      fF[6] = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fF[6], fL[0]);
      fOkButton = new TGTextButton (fF[6], 
                           new TGHotString ("&Ok"), 1);
      fOkButton->Associate (this);
      fF[6]->AddFrame (fOkButton, fL[3]);
      fCancelButton = new TGTextButton (fF[6], 
                           new TGHotString ("&Cancel"), 0);
      fCancelButton->Associate (this);
      fF[6]->AddFrame (fCancelButton, fL[3]);
      // set values
      SetValues (u);
   
      // set dialog box title
      SetWindowName ("Select UDN: Directory");
      SetIconName ("Select UDN");
      SetClassHints ("SelectUDNDirDlg", "SelectUDNDirDlg");
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmUDNDirDlg::~TLGDfmUDNDirDlg () 
   {
      delete fName;
      delete fFileButton;
      delete fEnableButton;
      delete fStartButton[0];
      delete fStartButton[1];
      delete fStart[0];
      delete fStart[1];
      delete fStopButton[0];
      delete fStopButton[1];
      delete fStop[0];
      delete fStop[1];
      delete fOkButton;
      delete fCancelButton;
      for (int i = 0; i < 3; i++) {
         delete fLabel[i];
      }
      for (int i = 0; i < 7; i++) {
         delete fF[i];
      }
      for (int i = 0; i < 7; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmUDNDirDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDirDlg::GetValues (UDN& udn)
   {
      string s = fName->GetText();
      devicetype dtype = dev_from_name (s.c_str());
      // file
      if (dtype == dev_file) {
         udn = UDN (s.c_str());
         return kTRUE;
      }
      // all others but dir
      else if (dtype != dev_dir) {
         return kFALSE; 
      }
      // dir
      bool autoinc = fEnableButton->GetState() == kButtonDown;
      if (!autoinc) {
         udn = UDN (s.c_str());
         return kTRUE;
      }
      int dir0 = fStart[0]->GetIntNumber ();
      bool d0 = fStartButton[0]->GetState() == kButtonDown;
      int file0 = fStart[1]->GetIntNumber ();
      bool f0 = fStartButton[1]->GetState() == kButtonDown;
      int file1 = 0;
      bool f1 = false;
      int dir1 = 0;
      bool d1 = false;
      int num = -1;
      if (fSourceSel) {
         dir1 = fStop[0]->GetIntNumber ();
         d1 = fStopButton[0]->GetState() == kButtonDown;
         file1 = fStop[1]->GetIntNumber ();
         f1 = fStopButton[1]->GetState() == kButtonDown;
      }
      else {
         num = fStop[0]->GetIntNumber ();
         if (fStopButton[0]->GetState() == kButtonUp) num = -1;
      }
      s += "@";
      if (d0 || f0) {
         char buf[256];
         sprintf (buf, "%i", dir0);
         if (f0) sprintf (buf + strlen (buf), ".%i", file0);
         s += buf;
      }
      if (d1 || f1) {
         char buf[256];
         sprintf (buf, ":%i", dir1);
         if (f1) sprintf (buf + strlen (buf), ".%i", file1);
         s += buf;
      }
      if (num >= 0) {
         char buf[256];
         sprintf (buf, "#%i", num);
         s += buf;
      }
      udn = UDN (s.c_str());
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDirDlg::SetValues (const UDN& udn)
   {
      devicetype dtype = dev_from_name (udn);
      // directory
      if (dtype == dev_dir) {
         dir_support supp (udn);
         fName->SetText (supp.dirStem().c_str());
         fEnableButton->SetState (supp.autoIncrement() ? 
                              kButtonDown : kButtonUp);
         fStart[0]->SetIntNumber (supp.curDir());
         fStartButton[0]->SetState (supp.curDir() > 0 ? 
                              kButtonDown : kButtonUp);
         fStart[1]->SetIntNumber (supp.curFile());
         fStartButton[0]->SetState (supp.curFile() > 0 ? 
                              kButtonDown : kButtonUp);
         if (fSourceSel) {
            if (supp.lastDir() >= 0) {
               fStop[0]->SetIntNumber (supp.lastDir());
               fStopButton[0]->SetState (kButtonDown);
            }
            else {
               fStop[0]->SetIntNumber (0);
               fStopButton[0]->SetState (kButtonUp);
            }
            if (supp.lastFile() >= 0) {
               fStop[1]->SetIntNumber (supp.lastFile());
               fStopButton[1]->SetState (kButtonDown);
            }
            else {
               fStop[1]->SetIntNumber (0);
               fStopButton[1]->SetState (kButtonUp);
            }
         }
         else {
            fStopButton[0]->SetState (kButtonDown);
            fStop[0]->SetIntNumber (supp.fileNum());
         }
      }
      // file & rest
      else {
         fName->SetText (udn);
         fEnableButton->SetState (kButtonUp);
         fStart[0]->SetIntNumber (0);
         fStartButton[0]->SetState (kButtonUp);
         fStart[1]->SetIntNumber (0);
         fStartButton[1]->SetState (kButtonUp);
         fStop[0]->SetIntNumber (0);
         fStopButton[0]->SetState (kButtonUp);
         if (fSourceSel) {
            fStop[1]->SetIntNumber (0);
            fStopButton[1]->SetState (kButtonUp);
         }
      }
      return (dtype == dev_dir) || (dtype == dev_file);
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDirDlg::ChooseFile (UDN& udn)
   {
      // file save as dialog
      TGFileInfo	info;
      info.fFilename = 0;
      info.fIniDir = 0;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gFrameTypes);
   #else
      info.fFileTypes = const_cast<char**>(gFrameTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 12 ; // point to all files
      {
	 // On return, info.fFilename will be filled in
	 // unless the user cancels the operation.
	 new TLGFileDialog(GetMain(), &info, kFDOpen) ;
      }
      if (!info.fFilename)
   #else
      if (!TLGFileDialog (GetMain(), info, kFDOpen, 0))
   #endif
      {
      #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
         delete [] info.fFilename;
      #endif
         return kFALSE;
      }
      else 
      {
         udn = UDN (info.fFilename);
         // check if a directory
         Long_t id, size, flags, modtime;
         id = size = flags = modtime = 0;
         bool exists = !gSystem->GetPathInfo (info.fFilename, &id, &size, 
                              &flags, &modtime);
         // if sorce: check if exists
         if (fSourceSel && !exists) {
            Int_t retval;
            new TGMsgBox(gClient->GetRoot(), GetMain(), "Error", 
                        "File does not exist.", 
                        kMBIconStop, kMBOk, &retval);
         #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
            delete [] info.fFilename;
         #endif
            return kFALSE;
         }
         // if destination: check if not regular file
         if (!fSourceSel && exists && !(flags & 2)) {
            Int_t retval;
            new TGMsgBox(gClient->GetRoot(), GetMain(), "Error", 
                        "File not allowed for output.", 
                        kMBIconStop, kMBOk, &retval);
         #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
            delete [] info.fFilename;
         #endif
            return kFALSE;
         }
         // directory
         if ((fSourceSel && (flags & 2)) || !fSourceSel) {
            // check auto-numbering
            char* p = info.fFilename + strlen (info.fFilename);
            while ((p > info.fFilename) && isdigit (*(p-1))) {
               p--;
            }
            TString s;
            if (isdigit (*p)) {
               int num = atoi (p);
               *p = 0;
               char buf [1024];
               sprintf (buf, "dir://%s@%i", info.fFilename, num);
               s = buf;
            }
            else {
               s = TString ("dir://") + info.fFilename;
            }
            udn = UDN ((const char*)s);
         #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
            delete [] info.fFilename;
         #endif
            return kTRUE;
         }
         // regulare file ?
         else {
            TString s = TString ("file://") + info.fFilename;
            udn = UDN ((const char*)s);
         #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
            delete [] info.fFilename;
         #endif
            return kTRUE;
         }
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDirDlg::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // get data
                  UDN udn;
                  if (GetValues (udn)) {
                     *fUDN = udn;
                     if (fOk) *fOk = kTRUE;
                     DeleteWindow();
                  }
                  else {
                     Int_t retval;
                     char buf[1024];
                     sprintf (buf, "Illegal UDN %s.", (const char*) udn);
                     new TGMsgBox(gClient->GetRoot(), GetMain(), "Error", 
                                 buf, kMBIconStop, kMBOk, &retval);
                  }
                  break;
               }
            // Choose file
            case 100:
               {
                  UDN udn;
                  if (ChooseFile (udn)) {
                     SetValues (udn);
                  }
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmUDNTapeDlg						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmUDNTapeDlg::TLGDfmUDNTapeDlg (const TGWindow *p, 
                     const TGWindow *main, UDN& udn, 
                     Bool_t sourcesel, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fSourceSel (sourcesel),
   fUDN (&udn), fOk (&ret)
   {
      fL[0] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 4, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[3] = new TGLayoutHints (kLHintsTop | kLHintsLeft | 
                           kLHintsExpandX, 45, 45, 10, 2);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 2, 1, 1);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 10, 2, 0, 0);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY |
                           kLHintsExpandX, 0, 0, 1, 1);
      fL[8] = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandX, 0, 0, 4, 0);
      // Device group
      fF[0] = new TGGroupFrame (this, "Device");
      AddFrame (fF[0], fL[0]);
      fF[1] = new TGHorizontalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[1], fL[1]);
      fLabel[0] = new TGLabel (fF[1], "Name: ");
      fF[1]->AddFrame (fLabel[0], fL[2]);
      fName = new TLGTextEntry (fF[1], "", 100);
      fName->Associate (this);
      fName->Resize (330, 23);
      fF[1]->AddFrame (fName, fL[4]);
      // buffer group
      fF[2] = new TGGroupFrame (this, "Buffers");
      AddFrame (fF[2], fL[0]);
         // File position or number of archives
      fF[3] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[3], fL[1]);
      fPrmButton[0] = new TGCheckButton (fF[3], sourcesel ?
                           "File position to start: " :
                           "Number of archives per tape: ", 101);
      fPrmButton[0]->Associate (this);
      fF[3]->AddFrame (fPrmButton[0], fL[4]);
      fPosArch = new TLGNumericControlBox (fF[3], 0., 6, 
                           102, kNESInteger, kNEANonNegative);;
      fPosArch->Associate (this);
      fF[3]->AddFrame (fPosArch, fL[4]);
         // Number of files
      fF[4] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[4], fL[1]);
      fPrmButton[1] = new TGCheckButton (fF[4], sourcesel ?
                           "Number of files to read: " :
                           "Number of files per archive: ", 103);
      fPrmButton[1]->Associate (this);
      fF[4]->AddFrame (fPrmButton[1], fL[4]);
      fFNum = new TLGNumericControlBox (fF[4], 1., 6, 
                           104, kNESInteger, kNEAPositive);;
      fFNum->Associate (this);
      fF[4]->AddFrame (fFNum, fL[4]);
         // File/dir selection
      fF[5] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[5], fL[8]);
      fPrmButton[2] = new TGCheckButton (fF[5], sourcesel ?
                           "File selection: " :
                           "Directory name: ", 105);
      fPrmButton[2]->Associate (this);
      fF[5]->AddFrame (fPrmButton[2], fL[4]);
      fFD = new TLGTextEntry (fF[5], "", 106);
      fFD->Associate (this);
      fF[5]->AddFrame (fFD, fL[7]);
         // Robot specification
      fF[6] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[6], fL[8]);
      fPrmButton[3] = new TGCheckButton (fF[6], "Robot: ", 107);
      fPrmButton[3]->Associate (this);
      fF[6]->AddFrame (fPrmButton[3], fL[4]);
      fRobot = new TLGTextEntry (fF[6], "", 108);
      fRobot->Associate (this);
      fF[6]->AddFrame (fRobot, fL[7]);
      // button group
      fF[7] = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fF[7], fL[0]);
      fOkButton = new TGTextButton (fF[7], 
                           new TGHotString ("&Ok"), 1);
      fOkButton->Associate (this);
      fF[7]->AddFrame (fOkButton, fL[3]);
      fCancelButton = new TGTextButton (fF[7], 
                           new TGHotString ("&Cancel"), 0);
      fCancelButton->Associate (this);
      fF[7]->AddFrame (fCancelButton, fL[3]);
      // set values
      string part = (const char*) udn;
      string name;
      string conf;
      string::size_type pos = part.find ('-');
      if (pos == string::npos) {
         name = part;
         conf = "";
      }
      else {
         name = part.substr (0, pos);
         part.erase (0, pos);
         conf = part;
      }
      pos = name.find ("tape://");
      if (pos != string::npos) {
         name.erase (0, pos + 7);
      }
      tape_support tape (name.c_str(), conf.c_str());
      fName->SetText (name.empty() ? "/dev/rmt/0n" : name.c_str());
      if (sourcesel) {
         fPrmButton[0]->SetState (tape.getFileStart() > 0 ? 
                              kButtonDown : kButtonUp);
         fPosArch->SetIntNumber (tape.getFileStart() > 0 ? 
                              tape.getFileStart() : 0);
      }
      else {
         fPrmButton[0]->SetState (tape.getArchiveNum() >= 0 ? 
                              kButtonDown : kButtonUp);
         fPosArch->SetIntNumber (tape.getArchiveNum() >= 0 ? 
                              tape.getArchiveNum() : 10);
      }
      fPrmButton[1]->SetState (tape.getFileNum() > 0 ? 
                           kButtonDown : kButtonUp);
      fFNum->SetIntNumber (tape.getFileNum() > 0 ? 
                          tape.getFileNum() : 0);
      if (sourcesel) {
         fPrmButton[2]->SetState ((tape.getFileSpec() &&
                                  *tape.getFileSpec()) ? 
                              kButtonDown : kButtonUp);
         fFD->SetText (tape.getFileSpec());
      }
      else {
         fPrmButton[2]->SetState ((tape.getDirSpec() &&
                                  *tape.getDirSpec()) ? 
                              kButtonDown : kButtonUp);
         if (tape.getDirSpec()) {
            fFD->SetText (tape.getDirSpec());
         }
      }
      fPrmButton[3]->SetState ((tape.getRobotSpec() &&
                               *tape.getRobotSpec()) ? 
                           kButtonDown : kButtonUp);
      fRobot->SetText (tape.getRobotSpec());
   
      // set dialog box title
      SetWindowName ("Select UDN: Tape");
      SetIconName ("Select UDN");
      SetClassHints ("SelectUDNTapeDlg", "SelectUDNTapeDlg");
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmUDNTapeDlg::~TLGDfmUDNTapeDlg () 
   {
      delete fCancelButton;
      delete fOkButton;
      delete fRobot;
      delete fFD;
      delete fFNum;
      delete fPosArch;
      for (int i = 0; i < 4; i++) {
         delete fPrmButton[i];
      }
      delete fName;
      for (int i = 0; i < 1; i++) {
         delete fLabel[i];
      }
      for (int i = 0; i < 8; i++) {
         delete fF[i];
      }
      for (int i = 0; i < 9; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmUDNTapeDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNTapeDlg::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // get data
                  string p = "tape://";
                  p += trim (fName->GetText());
                  bool set = fPrmButton[0]->GetState() == kButtonDown;
                  int pa = fPosArch->GetIntNumber();
                  if (set) {
                     char buf[256];
                     sprintf (buf, " -%c %i", fSourceSel ? 'p' : 'a', pa);
                     p += buf;
                  
                  }
                  set = fPrmButton[1]->GetState() == kButtonDown;
                  int num = fFNum->GetIntNumber();
                  if (set) {
                     char buf[256];
                     sprintf (buf, " -n %i", num);
                     p += buf;
                  }
                  set = fPrmButton[2]->GetState() == kButtonDown;
                  string fd = trim (fFD->GetText());
                  if (set && !fd.empty()) {
                     char buf[1024];
                     sprintf (buf, " -%c %s", fSourceSel ? 'f' : 'd',
                             fd.c_str());
                     p += buf;
                  }
                  set = fPrmButton[3]->GetState() == kButtonDown;
                  string robot = trim (fRobot->GetText());
                  if (set && !robot.empty()) {
                     char buf[1024];
                     sprintf (buf, " -r %s", robot.c_str());
                     p += buf;
                  }
                  *fUDN = UDN (p.c_str());
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmUDNSmDlg						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmUDNSmDlg::TLGDfmUDNSmDlg (const TGWindow *p, const TGWindow *main,
                     UDN& udn, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fUDN (&udn), fOk (&ret)
   {
      fL[0] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 4, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[3] = new TGLayoutHints (kLHintsTop | kLHintsLeft | 
                           kLHintsExpandX, 65, 65, 10, 2);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 2, 1, 1);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 10, 2, 0, 0);
      // partition group
      fF[0] = new TGGroupFrame (this, "Partition");
      AddFrame (fF[0], fL[0]);
      fF[1] = new TGHorizontalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[1], fL[1]);
      fLabel[0] = new TGLabel (fF[1], "Name: ");
      fF[1]->AddFrame (fLabel[0], fL[2]);
      fName = new TLGTextEntry (fF[1], "", 100);
      fName->Associate (this);
      fName->Resize (450, 23);
      fF[1]->AddFrame (fName, fL[4]);
      // buffer group
      fF[2] = new TGGroupFrame (this, "Buffers");
      AddFrame (fF[2], fL[0]);
         // off-line
      fF[3] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[3], fL[1]);
      fOfflineButton = new TGCheckButton (fF[3], "Off-line", 101);
      fOfflineButton->Associate (this);
      fF[3]->AddFrame (fOfflineButton, fL[4]);
         // Size & number
      fF[4] = new TGHorizontalFrame (fF[2], 10, 10);
      fF[2]->AddFrame (fF[4], fL[1]);
      fLabel[1] = new TGLabel (fF[4], "Size: ");
      fF[4]->AddFrame (fLabel[1], fL[4]);
      fSize = new TLGNumericControlBox (fF[4], 0., 12, 
                           102, kNESInteger, kNEAPositive);;
      fSize->Associate (this);
      fF[4]->AddFrame (fSize, fL[4]);
      fLabel[2] = new TGLabel (fF[4], "    Number: ");
      fF[4]->AddFrame (fLabel[2], fL[4]);
      fNum = new TLGNumericControlBox (fF[4], 0., 4, 
                           103, kNESInteger, kNEAPositive);;
      fNum->Associate (this);
      fF[4]->AddFrame (fNum, fL[4]);
      // button group
      fF[5] = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fF[5], fL[0]);
      fOkButton = new TGTextButton (fF[5], 
                           new TGHotString ("&Ok"), 1);
      fOkButton->Associate (this);
      fF[5]->AddFrame (fOkButton, fL[3]);
      fCancelButton = new TGTextButton (fF[5], 
                           new TGHotString ("&Cancel"), 0);
      fCancelButton->Associate (this);
      fF[5]->AddFrame (fCancelButton, fL[3]);
      // set values
      string part = (const char*) udn;
      string name;
      string conf;
      string::size_type pos = part.find ('-');
      if (pos == string::npos) {
         name = part;
         conf = "";
      }
      else {
         name = part.substr (0, pos);
         part.erase (0, pos);
         conf = part;
      }
      devicetype dtype = dev_from_name (name.c_str());
      if (dtype != dev_dmt) {
         name = "dmt:///LHO_Online";
      }
      pos = name.find ("dmt://");
      if (pos != string::npos) {
         name.erase (0, pos + 6);
      }
      dmt_support dmt (false, name.c_str(), conf.c_str(), false);
      fName->SetText (dmt.getPname() ? dmt.getPname() : 0);
      fSize->SetIntNumber (dmt.getBufLen());
      fNum->SetIntNumber (dmt.getBufNum());
      fOfflineButton->SetState (dmt.offline() ? kButtonDown : kButtonUp);
   
      // set dialog box title
      SetWindowName ("Select UDN: Shared memory partition");
      SetIconName ("Select UDN");
      SetClassHints ("SelectUDNPartDlg", "SelectUDNPartDlg");
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmUDNSmDlg::~TLGDfmUDNSmDlg () 
   {
      delete fCancelButton;
      delete fOkButton;
      delete fNum;
      delete fSize;
      delete fOfflineButton;
      delete fName;
      for (int i = 0; i < 3; i++) {
         delete fLabel[i];
      }
      for (int i = 0; i < 6; i++) {
         delete fF[i];
      }
      for (int i = 0; i < 7; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmUDNSmDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNSmDlg::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // get data
                  string p = "dmt://";
                  string pname = trim (fName->GetText());
                  if (!pname.empty() && (pname[0] != '/')) {
                     p += '/';
                  }
                  p += pname;
                  bool offline = fOfflineButton->GetState() == kButtonDown;
                  int bufsize = fSize->GetIntNumber();
                  int bufnum = fNum->GetIntNumber();
                  if (offline) {
                     p += " -o";
                  }
                  if (bufsize != kSMDEFSIZE) {
                     char buf[256];
                     sprintf (buf, " -l %i", bufsize);
                     p += buf;
                  }
                  if (bufnum != kSMDEFNUM) {
                     char buf[256];
                     sprintf (buf, " -n %i", bufnum);
                     p += buf;
                  }
                  //cerr << "new UDN = " << p << endl;
                  *fUDN = UDN (p.c_str());
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmUDNDlg							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmUDNDlg::TLGDfmUDNDlg (const TGWindow *p, const TGWindow *main,
                     dataserver& ds, Bool_t sourcesel, UDNList& udnsel, 
                     Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fSourceSel (sourcesel), 
   fDS (&ds), fUDNcur (udnsel), fUDNret (&udnsel), fOk (&ret)
   {
      fServerType = ds.getType();
   
      fL[0] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 4, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[4] = new TGLayoutHints (kLHintsBottom | kLHintsExpandX, 2, 2, 5, 5);
      fL[5] = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           55, 55, 2, 2);
      fL[6] = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           55, 55, 2, 2);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 22, 2, 0, 0);
      // new group
      fSelGroup = new TGGroupFrame (this, "Selection");
      AddFrame (fSelGroup, fL[0]);
      for (int i = 0; i < (kMaxUDN + 1) / 2; i++) {
         fLine[i] = new TGHorizontalFrame (fSelGroup, 10, 10);
         fSelGroup->AddFrame (fLine[i], fL[1]);
         for (int j = 2 * i; (j < 2 * i + 2) && (j < kMaxUDN); j++) {
            fUDNActive[j] = new TGCheckButton (fLine[i], "", 
                                 kDfmUDNActive + j);
            fUDNActive[j]->Associate (this);
            fLine[i]->AddFrame (fUDNActive[j], j % 2 == 0 ? fL[2] : fL[7]);
            fUDN[j] = new TGComboBox (fLine[i], kDfmUDNSel + j);
            fUDN[j]->Associate (this);
            fUDN[j]->Resize (330, 23);
            fLine[i]->AddFrame (fUDN[j], fL[3]);
         }
      }
      // button group
      fBtnFrame = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fBtnFrame, fL[4]);
      fOkButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Ok"), 1);
      fOkButton->Associate (this);
      fBtnFrame->AddFrame (fOkButton, fL[5]);
      fAddButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Add..."), 2);
      fAddButton->Associate (this);
      fBtnFrame->AddFrame (fAddButton, fL[5]);
      fCancelButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("&Cancel"), 0);
      fCancelButton->Associate (this);
      fBtnFrame->AddFrame (fCancelButton, fL[6]);
   
      // fill data
      Build();
      if ((fServerType == st_LARS) ||
         (fServerType == st_NDS) ||
         (fServerType == st_Invalid)) {
         fAddButton->SetState (kButtonDisabled);
      }
   
      // set dialog box title
      SetWindowName ("Muliple UDN Selection");
      SetIconName ("UDN Selection");
      SetClassHints ("UDNSelectionDlg", "UDNSelectionDlg");
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmUDNDlg::~TLGDfmUDNDlg ()
   {
      delete fOkButton;
      delete fCancelButton;
      delete fAddButton;
      delete fBtnFrame;
      for (int i = 0; i < kMaxUDN; i++) {
         delete fUDNActive[i];
         delete fUDN[i];
      }
      for (int i = 0; i < (kMaxUDN + 1)/2; i++) {
         delete fLine[i];
      }
      delete fSelGroup;
      for (int i = 0; i < 8; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmUDNDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   void TLGDfmUDNDlg::Build()
   {
      // clear comboboxes
      for (int j = 0; j < kMaxUDN; j++) {
         fUDN[j]->RemoveEntries (0, 10000);
      }
      // fill comboboxes
      int id = 0;
      UDNList listid; // missuse type field of UDN info to store sel id
      for (UDNiter i = fDS->begin(); 
          i != fDS->end(); ++i, ++id) {
         for (int j = 0; j < kMaxUDN; j++) {
            fUDN[j]->AddEntry ((const char*)i->first, id);
         }
         UDNInfo info;
         info.setType ((UDNInfo::UDNType)id);
         listid.insert (UDNList::value_type (i->first, info));
      }
      // set combobox selection
      id = 0;
      for (UDNiter i = fUDNcur.begin(); 
          (i != fUDNcur.end()) && (id < kMaxUDN); ++i, ++id) {
         UDNiter f = listid.find (i->first);
         if (f != listid.end()) {
            fUDN[id]->Select ((int)f->second.getType());
         }
      }
      // set active checkboxes
      for (int j = 0; j < kMaxUDN; j++) {
         fUDNActive[j]->SetState (j < id ? kButtonDown : kButtonUp);
         fUDN[j]->MapSubwindows();
         fUDN[j]->Layout();
      }
   }

//______________________________________________________________________________
   void TLGDfmUDNDlg::GetData()
   {
      fUDNcur.clear();
      for (int i = 0; i < kMaxUDN; i++) {
         if (fUDNActive[i]->GetState() == kButtonUp) {
            continue;
         }
         TString sel = 
            ((TGTextLBEntry*) (fUDN[i]->GetSelectedEntry()))->
            GetText()->GetString();
         UDN u ((const char*)sel);
         UDNInfo* info = fDS->get (u);
         if (info == 0) {
            continue;
         }
         fUDNcur.insert (UDNList::value_type (u, *info));
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDlg::addUDN (const TGWindow* pwin, dataserver& ds,
                     Bool_t sourcesel, UDN* newudn)
   {
      switch (ds.getType()) {
         case st_File:
            {
               return addFileUDN (pwin, ds, sourcesel, newudn);
            }
         case st_Tape:
            {
               return addTapeUDN (pwin, ds, sourcesel, newudn);
            }
         case st_SM:
            {
               return addSmUDN (pwin, ds, sourcesel, newudn);
            }
         case st_LARS:
            {
               Int_t retval;
               new TGMsgBox(gClient->GetRoot(), pwin, "Error", 
                           "Add UDN not supported for LARS.", 
                           kMBIconStop, kMBOk, &retval);
               return kFALSE;
            }
         case st_NDS:
            {
               Int_t retval;
               new TGMsgBox(gClient->GetRoot(), pwin, "Error", 
                           "Add UDN not supported for NDS.", 
                           kMBIconStop, kMBOk, &retval);
               return kFALSE;
            }
         default:
            {
               Int_t retval;
               new TGMsgBox(gClient->GetRoot(), pwin, "Error", 
                           "Unknown server type.", 
                           kMBIconStop, kMBOk, &retval);
               return kFALSE;
            }
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDlg::addFileUDN (const TGWindow* pwin, dataserver& ds,
                     Bool_t sourcesel, UDN* newudn)
   {
      Bool_t ret;
      UDN udn;
      for (;;) {
         new TLGDfmUDNDirDlg (gClient->GetRoot(), pwin, udn, 
                             sourcesel, ret);
         if (!ret) {
            return kFALSE;
         }
         // :TRICKY: Don't change insert/erase order
         ds.insert (udn);
         if (sourcesel && !ds.lookupUDN (udn, true)) {
            ds.erase (udn);
         }
         else {
            if (newudn) *newudn = udn;
            return kTRUE;
         }
         char msg[1024];
         Int_t retval;
         sprintf (msg, "Illegal UDN %s.", (const char*)udn);
         new TGMsgBox(gClient->GetRoot(), pwin, "Error", msg, 
                     kMBIconStop, kMBOk, &retval);
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDlg::addTapeUDN (const TGWindow *pwin, dataserver& ds,
                     Bool_t sourcesel, UDN* newudn)
   {
      Bool_t ret;
      UDN udn;
      for (;;) {
         new TLGDfmUDNTapeDlg (gClient->GetRoot(), pwin, udn, 
                              sourcesel, ret);
         if (!ret) {
            return kFALSE;
         }
         // :TRICKY: Don't change insert/erase order
         ds.insert (udn);
         if (sourcesel && !ds.lookupUDN (udn, true)) {
            ds.erase (udn);
         }
         else {
            if (newudn) *newudn = udn;
            return kTRUE;
         }
         char msg[1024];
         Int_t retval;
         sprintf (msg, "Illegal tape specification %s.",
                 (const char*)udn);
         new TGMsgBox(gClient->GetRoot(), pwin, "Error", msg, 
                     kMBIconStop, kMBOk, &retval);
      }
      return kFALSE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDlg::addSmUDN (const TGWindow *pwin, dataserver& ds,
                     Bool_t sourcesel, UDN* newudn)
   {
      Bool_t ret;
      UDN udn;
      for (;;) {
         new TLGDfmUDNSmDlg (gClient->GetRoot(), pwin, udn, ret);
         if (!ret) {
            return kFALSE;
         }
         // :TRICKY: Don't change insert/erase order
         ds.insert (udn);
         if (sourcesel && !ds.lookupUDN (udn, true)) {
            ds.erase (udn);
         }
         else {
            if (newudn) *newudn = udn;
            return kTRUE;
         }
         char msg[1024];
         Int_t retval;
         sprintf (msg, "Illegal shared memory partition %s.",
                 (const char*)udn);
         new TGMsgBox(gClient->GetRoot(), pwin, "Error", msg, 
                     kMBIconStop, kMBOk, &retval);
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmUDNDlg::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // get values
                  GetData();
                  *fUDNret = fUDNcur;
                  // set ret value
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
            // Add
            case 2:
               {
                  // rebuild selection comboboxes if a UDN was added
                  if (addUDN (this, *fDS, fSourceSel)) {
                     GetData();
                     Build();
                  }
                  break;
               }
         }
      }
      return kTRUE;
   }


}
