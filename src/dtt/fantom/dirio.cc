#include "dirio.hh"
#include "framedir.hh"
#include "gmutex.hh"
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <deque>
#include <memory>
#include <algorithm>
#include <iostream>

namespace fantom {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// fdir cache                                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class fdir_cacheline {
   public:
      Time		fTime;
      string		fDirname;
      mutable FrameDir*	fDir;
   
      fdir_cacheline() : fDir (0) {
      }
      ~fdir_cacheline() {
         if (fDir) delete fDir; }
      fdir_cacheline (const fdir_cacheline& cline) {
         *this = cline; }
      fdir_cacheline& operator= (const fdir_cacheline& cline);
   };

//______________________________________________________________________________
   fdir_cacheline& fdir_cacheline::operator= (const fdir_cacheline& cline)
   {
      if (this != &cline) {
         fTime = cline.fTime;
         fDirname = cline.fDirname;
         fDir = cline.fDir;
         cline.fDir = 0;
      }
      return *this;
   }

//______________________________________________________________________________
   class fdir_cache : protected deque<fdir_cacheline> {
   public:
      fdir_cache (int max = 10) : fMax (max < 1 ? 1 : max) {
      }
      void Add (const string& dirname, FrameDir* fdir);
      bool Get (const string& dirname, FrameDir& fdir);
      void Clear();
   
   protected:
      mutable thread::mutex	fMux;
      int		fMax;
   };

//______________________________________________________________________________
   void fdir_cache::Add (const string& dirname, FrameDir* fdir)
   {
      // Add most recent one at front
      thread::semlock lockit (fMux);
      fdir_cacheline cline;
      cline.fTime = Now();
      cline.fDirname = dirname;
      cline.fDir = fdir;
      push_front (cline);
      if ((int)size() > fMax) pop_back();
   }

//______________________________________________________________________________
   bool fdir_cache::Get (const string& dirname, FrameDir& fdir)
   {
      thread::semlock lockit (fMux);
      for (iterator i = begin(); i != end(); ++i) {
         if (dirname == i->fDirname) {
            // make sure checkData was called at least once
            i->fDir->begin();
            // copy fdir and set new time stamp
            fdir = *(i->fDir);
            i->fTime = Now();
            // move to the front
            if (i != begin()) {
               for (iterator j = i - 1; j != begin(); --j) {
                  std::swap (*i, *j); 
               }
            }
            return true;
         }
      }
      return false;
   }

//______________________________________________________________________________
   void fdir_cache::Clear()
   {
      thread::semlock lockit (fMux);
      clear();
   }

//______________________________________________________________________________
   static fdir_cache		gDirCache;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dir_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void dir_support::setDirname (const char* dirname, 
                     const char* conf) 
   {
      fAutoInc = false;
      fFileNum = 3600;
      fCurFile = 0;
      fCurDir = 0;
      fLastFile = -1;
      fLastDir = -1;
      fDirPos = fDir.end();
      fDirLast = fDir.end();
      if ((dirname == 0) || trim(dirname).empty()) {
         fDirname = "";
         fDirStem = "";
         fDirInit = true;
      }
      else {
         fDirname = trim (dirname);
         while (!fDirname.empty() && 
               (fDirname[fDirname.size() - 1] == '/')) {
            fDirname.erase (fDirname.size() - 1);
         }
         fDirStem = fDirname;
         fDirInit = false;
         string::size_type pos = fDirname.find ('#');
         if (pos != string::npos) {
            fAutoInc = true;
            fFileNum = atoi (fDirname.c_str() + pos + 1);
            if (fFileNum <= 0) fFileNum = 3600;
            fDirStem = fDirname.substr (0, pos);
         }
         pos = fDirStem.find ('@');
         if (pos != string::npos) {
            fAutoInc = true;
            char* p = (char*) fDirStem.c_str() + pos + 1;
            fCurDir = strtol (p, &p, 10);
            if (p && (*p == '.')) {
               fCurFile = strtol (p + 1, &p, 10);
            }
            if (p && (*p == ':')) {
               fLastDir = strtol (p + 1, &p, 10);
            }
            if (p && (*p == '.')) {
               fLastFile = strtol (p + 1, &p, 10);
            }
            if (fCurDir < 0) fCurDir= 0;
            if (fCurFile < 0) fCurFile = 0;
            if (fLastDir < 0) fLastDir = -1;
            if (fLastFile < 0) fLastFile = -1;
            fDirStem = fDirStem.substr (0, pos);
         }
      }
   }

//______________________________________________________________________________
   const char* dir_support::getCurDir ()
   {
      if (!fAutoInc) {
         if (fCreateIfNeeded) {
            fCreateIfNeeded = false;
            if (::mkdir (fDirname.c_str(), 0777)) {
               if (errno != EEXIST) {
                  cerr << "Directory creation failed for " << 
                     fDirname << " (errno " << errno << ")" << endl;
               }
            }
         }
         return fDirname.c_str();
      }
      else if ((fLastDir >= 0) && 
	       ((fCurDir > fLastDir) || 
		(fCurDir == fLastDir && fLastFile >= 0 && fCurFile > fLastFile)
	       )) {
         return 0;
      }
      else {
         sprintf (fTempdir, "%s%d", fDirStem.c_str(), fCurDir);
         if (fCreateIfNeeded && (fCurFile == 0)) {
            if (::mkdir (fTempdir, 0777)) {
               if (errno != EEXIST) {
                  cerr << "Directory creation failed for " << 
                     fDirname << " (errno " << errno << ")" << endl;
               }
            }
         }
         fCurFile++;
         if (fCurFile >= fFileNum) {
            fCurFile = 0;
            fCurDir++;
         }
         return fTempdir;
      }
   }

//______________________________________________________________________________
   bool dir_support::getNextFilename (std::string& fname)
   {
      // initialization
      if (!fDirInit) {
         if (!gDirCache.Get (fDirname, fDir)) {
            FrameDir* fdir = new FrameDir;
            if (!fdir) {
               return false;
            }
            init (*fdir);
            fdir->begin(); // make sure checkData is called
            gDirCache.Add (fDirname, fdir);
            fDir = *fdir;
         }
      
         // set position to first and last
         fDirPos = (fT0 > Time(0)) ? fDir.getStart (fT0.getS()) : fDir.begin();
         if (fDt > Interval (0)) {
            Time stop = fT0 + fDt;
            if (stop.getN() > 0) stop += Interval (1);
            fDirLast = fDir.getLast (stop.getS());
         }
         else {
            fDirLast = fDir.end();
         }
         fDirInit = true;
      }
   
      // get next name
      while (fDirPos != fDirLast) {
      //    // check limits
         // if (fDirPos->getEndTime() < fT0 + 1E-8) {
            // ++fDirPos;
            // continue;
         // }
         // if ((fDt > Interval (0.)) && 
            // (fDirPos->getStartTime() >= fT0 + fDt + 1E-8)) {
            // ++fDirPos;
            // continue;
         // }
         // found one
         fname = fDirPos->getFile();
         ++fDirPos;
         return true;
      }
      return false;
   }

//______________________________________________________________________________
   bool dir_support::setNextFilename (std::string& fname, 
                     const std::string& guess)
   {
      // assemble name
      const char* dir = getCurDir();
      if (!dir) {
         return false;
      }
      fname = dir;
      if (!fname.empty() && (fname[fname.size()-1] != '/')) {
         fname += "/";
      }
      fname += guess;
      return true;
   }

//______________________________________________________________________________
   void dir_support::init (FrameDir& fdir) const
   {
      // not auto-increment: just use specified dir
      if (!fAutoInc) {
         string dir = fDirname + "/*";
         fdir.add (dir.c_str(), true);
      }
      // add 'em all
      // else if (fLastDir == -1) {
         // string match = "[0-9]/*";
         // int size;
         // int newsize = 0;
         // int level = 5;
         // do {
            // size = newsize;
            // string dir = fDirStem + match;
            // fdir.add (dir.c_str(), true);
            // newsize = fdir.size();
            // match = "[0-9]" + match;
            // --level;
         // } while (level && (size != newsize));
      // }
      // // add start to stop
      // else {
         // for (int i = fCurDir; i <= fLastDir; i++) {
            // char buf[1024];
            // sprintf (buf, "%s%i/*", fDirStem.c_str(), i);
            // fdir.add (buf, true);
         // }
      // }
      else {
         string::size_type pos = fDirStem.rfind ('/');
         string dir;
         string rest;
         if (pos == string::npos) {
            rest = fDirStem;
         }
         else {
            dir = fDirStem.substr (0, pos);
            rest = fDirStem.substr (pos + 1, string::npos);
         }
         //cout << "dir = " << dir << " rest = " << rest << endl;
         DIR* dd = opendir(dir.c_str());
         if (dd) {
            for (dirent* dirt=readdir(dd) ; dirt ; dirt = readdir(dd)) {
               if ((strncmp (rest.c_str(), dirt->d_name, rest.size()) == 0) &&
                  isdigit (dirt->d_name[rest.size()])) {
                  char* end = 0;
                  long num = strtol (dirt->d_name + rest.size(), &end, 10);
                  //cout << "found num = " << num << endl;
                  if (!*end && ((num >= fCurDir) &&
                               ((fLastDir < 0) || (num <= fLastDir)))) {
                     string s;
                     if (pos != string::npos) s += dir + "/";
                     s += dirt->d_name + string ("/*");
                     fdir.add (s.c_str(), true);
                     //cout << "add files = " << s << endl;
                  }
               }
            }
         }
         closedir(dd);
      }
      // we may have added too many, remove the unwanted ones
      if (fAutoInc) {
         if (fCurFile > 0) {
            char buf[1024];
            sprintf (buf, "%s%i/*", fDirStem.c_str(), fCurDir);
            FrameDir temp; 
            temp.add (buf, true);
            int rm = min (fCurFile, temp.size());
            for (int i = 0; (i < rm) && !fdir.empty(); ++i) {
               fdir.erase (fdir.begin()->getStartGPS());
            }
         }
         if ((fLastDir >= 0) && (fLastFile >= 0)) {
            char buf[1024];
            sprintf (buf, "%s%i/*", fDirStem.c_str(), fLastDir);
            FrameDir temp; 
            temp.add (buf, true);
            int rm = temp.size() - fCurFile + 1;
            for (int i = 0; (i < rm) && !fdir.empty(); ++i) {
               fdir.erase ((--fdir.end())->getStartGPS());
            }
         }
      }
   }

//______________________________________________________________________________
   void dir_support::ClearCache()
   {
      gDirCache.Clear();
   }

}
