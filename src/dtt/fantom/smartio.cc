/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <fnmatch.h>
#include <regex.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "goption.hh"
#include "smartio.hh"
#include "framefast/framefast.hh"
#include "framefast/frameio.hh"
#include "framedir.hh"
#include "tapeio.hh"
#include "dirio.hh"
#include "dmtio.hh"
#include "ndsio.hh"
#include "sendsio.hh"
#include "larsio.hh"
#include "inetio.hh"
#include "funcio.hh"
#include "fchannel.hh"


namespace fantom {
   using namespace std;
   using namespace framefast;
   using namespace thread;

   // Time to wait if nothing to do (in usec)
   //static const int _SIO_TICK = 100000;
   static const int _SIO_TICK = 1000;
   // Maximum time to wait before terminating IO threads (in usec)
   static const int _SIO_MAXWAIT = 100000000;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// work around warnings in pthread.h					//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C" {
   typedef void (*cleanup_type)(void*);
}


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }

//============================== power-of-two functions not used!
#if 0
//______________________________________________________________________________
   static bool ispoweroftwo (int val)
   { 
      double twomant;
      int twoexp;
   
      twomant = frexp((double)val, &twoexp);
      if(twomant != 0.5)
         return false;
      else
         return true;
   }

//______________________________________________________________________________
   static double roundtopoweroftwo (double val) 
   {
      if (val == 0) {
         return 0;
      }
      double twomant;
      int twoexp;
      twomant = frexp(val, &twoexp);
      if (twomant == 0.5) {
         return val;
      }
      else {
         return ldexp (1.0, twoexp);
      }
   }
#endif

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xxx_namerecord                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class tape_namerecord : public smartio_basic::namerecord, 
   public tape_support {
   public:
      tape_namerecord (const char* name, const char* conf) 
      : namerecord (name, conf), tape_support (name + 7, conf) {
      }
   };

//______________________________________________________________________________
   class dir_namerecord : public smartio_basic::namerecord, 
   public dir_support {
   public:
      dir_namerecord (const char* name, const char* conf) 
      : namerecord (name, conf), dir_support (name + 6, conf) {
      }
      virtual bool getNextName (std::string& name) {
         return getNextFilename (name); }
   };

//______________________________________________________________________________
   class dmt_namerecord : public smartio_basic::namerecord, 
   public dmt_support {
   public:
      dmt_namerecord (bool prod, const char* name, const char* conf) 
      : namerecord (name, conf), dmt_support (prod, name + 6, conf) {
      }
   };

//______________________________________________________________________________
   class nds_namerecord : public smartio_basic::namerecord, 
   public nds_support {
   public:
      nds_namerecord (const char* name, const char* conf,
                     const channelquerylist* chns) 
      : namerecord (name, conf), nds_support (name + 6, conf) {
         selectChannels (chns); }
   };

//______________________________________________________________________________
   class sends_namerecord : public smartio_basic::namerecord, 
   public sends_support {
   public:
      sends_namerecord (const char* name, const char* conf,
                     const channelquerylist* chns) 
      : namerecord (name, conf), sends_support (name + 6, conf) {
         selectChannels (chns); }
   };

//______________________________________________________________________________
   class lars_namerecord : public smartio_basic::namerecord, 
   public lars_support {
   public:
      lars_namerecord (const char* name, const char* conf,
                      const channelquerylist* chns) 
      : namerecord (name, conf), lars_support (name + 7, conf) {
         selectChannels (chns); }
   };

//______________________________________________________________________________
   class http_namerecord : public smartio_basic::namerecord, 
   public http_support {
   public:
      http_namerecord (const char* name, const char* conf) 
      : namerecord (name, conf), http_support (name + 7, conf) {
      }
   };

//______________________________________________________________________________
   class func_namerecord : public smartio_basic::namerecord, 
   public func_support {
   public:
      func_namerecord (bool isout, const char* name, const char* conf) 
      : namerecord (name, conf), func_support (isout, name + 7, conf) {
      }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// smartio_basic                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   smartio_basic::smartio_basic (bool out) 
   : fError (false), fOutDev (out), fState (io_inactive), fUser (0), 
   fQueueLen (5), fQueueLimit (1024), fQuery (0), fTID (0) 
   {
   }

//______________________________________________________________________________
   smartio_basic::~smartio_basic () 
   {
      terminate(); 
      fNames.clear();
      if (fQuery) delete fQuery;
   }

//______________________________________________________________________________
   inline void* iothread_start (void* p) 
   {
      ((smartio_basic*)p)->iothread();
      return 0;
   }

//______________________________________________________________________________
extern "C" 
   void* iothread_c (void* p) 
   {
      return iothread_start (p);
   }

//______________________________________________________________________________
   std::string smartio_basic::extension (frametype ftype)
   {
      // extension
      string ext;
      switch  (ftype) {
         case FF:
            ext = ".gwf";
            break;
         case STF:
            ext = ".gwf";
            break;
         case MTF:
            ext = ".gwf";
            break;
         // case NDS:
            // ext = ".nds";
            // break;
         // case BIN:
            // ext = ".dat";
            // break;
         // case TXT:
            // ext = ".txt";
            // break;
         default:
            ext = "";
            break;
      }
      return ext;
   }

//______________________________________________________________________________
   bool smartio_basic::setup ()
   {
      pthread_attr_t		tattr;
      int			status;
   
      // set thread parameters: detached & process scope
      if (pthread_attr_init (&tattr) != 0) {
         return false;
      }
      pthread_attr_setdetachstate (&tattr, PTHREAD_CREATE_DETACHED);
      pthread_attr_setscope (&tattr, PTHREAD_SCOPE_PROCESS);
      // create thread
      status = pthread_create (&fTID, &tattr, iothread_c, (void*) this);
      pthread_attr_destroy (&tattr);
      // cerr << "Create LWP " << fTID <<"  for " <<
         // (isOut() ? "out" : "inp") << endl;
      return  (status == 0);
   }

//______________________________________________________________________________
   void smartio_basic::terminate ()
   {
      if (fTID != 0) {
         // do not terminate while busy
         int max = _SIO_MAXWAIT / _SIO_TICK;
         for (int i = 0; (i < max) && busy(); ++i) {
            timespec w = {0, _SIO_TICK * 1000};
            // send a signal to unblock waits
            if (i % 100 == 9) {
               pthread_kill (fTID, SIGCONT);
            }
            nanosleep (&w, 0);
         }
         pthread_cancel (fTID);
         fTID = 0;
      }
   }

//______________________________________________________________________________
   bool smartio_basic::setState (iostate state)
   {
      fState = state;
      return true;
   }

//______________________________________________________________________________
   bool smartio_basic::setChannelList (const channelquerylist* q)
   {
      if (fQuery) delete fQuery;
      fQuery = 0;
      if (q == 0) {
         return true;
      }
      fQuery = new (nothrow) channelquerylist (*q);
      return fQuery != 0;
   }

//______________________________________________________________________________
   bool smartio_basic::setChannelList (const char* config)
   {
      if (fQuery) delete fQuery;
      fQuery = 0;
      if (config == 0) {
         return true;
      }
      // check for curly brackets
      string s = trim (config);
      if (s.empty()) {
         return true;
      }
      // assume it is a list of names
      if (s[0] == '{') {
         s.erase (0, 1);
         if (s[s.size()-1] == '}') {
            s.erase (s.size()-1, 1);
         }
         // create new query list
         fQuery = newChannelList (s.c_str());
         return true;
      }
      // assume list is a file
      else {
         // create new query list
         fQuery = newChannelListFromFile (s.c_str());
         return true;
      }
   }

//______________________________________________________________________________
   bool smartio_basic::parseName (devicetype dtype, const char* arg)
   {
      semlock lockit (fMux);
      namerecord* eofmark = 0;
      if (!fNames.empty() && (fNames.back().getDevType() == dev_eof)) {
         eofmark = fNames.removeName (true);
      }
      if (!fNames.parseName (dtype, arg)) {
         fError = true;
         fMsg = fNames.Message();
         if (eofmark) fNames.addName (eofmark);
         return false;
      }
      else {
         if (eofmark) fNames.addName (eofmark);
         return true;
      }
   }

//______________________________________________________________________________
   bool smartio_basic::parseName (const char* conf) 
   {
      semlock lockit (fMux);
      namerecord* eofmark = 0;
      if (!fNames.empty() && (fNames.back().getDevType() == dev_eof)) {
         eofmark = fNames.removeName (true);
      }
      if (!fNames.parseName (conf)) {
         fError = true;
         fMsg = fNames.Message();
         if (eofmark) fNames.addName (eofmark);
         return false;
      }
      else {
         if (eofmark) fNames.addName (eofmark);
         return true;
      }
   }

//______________________________________________________________________________
   bool smartio_basic::addName (namerecord* name, bool back) 
   {
      semlock lockit (fMux);
      namerecord* eofmark = 0;
      if (!fNames.empty() && (fNames.back().getDevType() == dev_eof)) {
         eofmark = fNames.removeName (true);
      }
      if (!fNames.addName (name, back)) {
         fMsg = "Unable to add name";
         if (eofmark) fNames.addName (eofmark);
         return false;
      }
      else {
         if (eofmark) fNames.addName (eofmark);
         return true;
      }
   }

//______________________________________________________________________________
   bool smartio_basic::addName (const char* name, bool back) 
   {
      return addName (name, 0, back);
   }

//______________________________________________________________________________
   bool smartio_basic::addName (const char* name, const char* conf,
                     bool back) 
   {
      semlock lockit (fMux);
      namerecord* eofmark = 0;
      if (!fNames.empty() && (fNames.back().getDevType() == dev_eof)) {
         eofmark = fNames.removeName (true);
      }
      if (!fNames.addName (name, conf, back)) {
         fMsg = "Unable to add name";
         if (eofmark) fNames.addName (eofmark);
         return false;
      }
      else {
         if (eofmark) fNames.addName (eofmark);
         return true;
      }
   }

//______________________________________________________________________________
   bool smartio_basic::addFileName (const char* name, unsigned int cont, 
                     bool back) 
   {
      semlock lockit (fMux);
      namerecord* eofmark = 0;
      if (!fNames.empty() && (fNames.back().getDevType() == dev_eof)) {
         eofmark = fNames.removeName (true);
      }
      if (!fNames.addFileName (name, cont, back)) {
         fMsg = "Unable to add name";
         if (eofmark) fNames.addName (eofmark);
         return false;
      }
      else {
         if (eofmark) fNames.addName (eofmark);
         return true;
      }
   }

//______________________________________________________________________________
   int smartio_basic::addFiles (const char* name, bool back) 
   {
      if (!name) {
         return 0;
      }
      semlock lockit (fMux);
      namerecord* eofmark = 0;
      if (!fNames.empty() && (fNames.back().getDevType() == dev_eof)) {
         eofmark = fNames.removeName (true);
      }
      bool ret = fNames.addFiles (name, back);
      if (eofmark) fNames.addName (eofmark);
      return ret;
   }

//______________________________________________________________________________
   bool smartio_basic::addNameList (const char* mem, int len, bool back)
   {
      semlock lockit (fMux);
      namerecord* eofmark = 0;
      if (!fNames.empty() && (fNames.back().getDevType() == dev_eof)) {
         eofmark = fNames.removeName (true);
      }
      if (!fNames.addNameList (mem, len, back)) {
         fMsg = "Invalid list of names";
         if (eofmark) fNames.addName (eofmark);
         return false;
      }
      else {
         if (eofmark) fNames.addName (eofmark);
         return true;
      }
   }

//______________________________________________________________________________
   smartio_basic::namerecord* smartio_basic::removeName () 
   {
      semlock lockit (fMux);
      return fNames.removeName();
   }

//______________________________________________________________________________
   bool smartio_basic::loadName (namelist::iterator loc) 
   {
      namerecord* rec = 0;
   
      switch ((*loc)->getDevType()) {
         // file
         case dev_file:
            {
               // check time limit if applicable
               filenamerecord* frec = 
                  dynamic_cast<filenamerecord*>(*loc);
               if ((frec != 0) && frec->isOutside (fT0, fDt)) {
                  return false;
               }
               else {
                  return true;
               }
            }
         // directory
         case dev_dir:
            {
               // check if namerecord supports dir
               if (dynamic_cast<dir_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) dir_namerecord 
                  ((*loc)->getFullname(), (*loc)->getConf());
               break;
            }
         // tape
         case dev_tape:
            {
               // check if namerecord supports tape
               if (dynamic_cast<tape_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) tape_namerecord 
                  ((*loc)->getFullname(), (*loc)->getConf());
               break;
            }
         // dmt
         case dev_dmt:
            {
               // check if namerecord supports shared memory
               if (dynamic_cast<dmt_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) dmt_namerecord 
                  (isOut(), (*loc)->getFullname(), (*loc)->getConf());
               break;
            }
         // nds
         case dev_nds:
            {
               // check if namerecord supports nds
               if (dynamic_cast<nds_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) nds_namerecord 
                  ((*loc)->getFullname(), (*loc)->getConf(),
                  fQuery);
               cerr << "load name " << (*loc)->getFullname() << endl;
               break;
            }
         // nds2
         case dev_sends:
            {
               // check if namerecord supports nds
               if (dynamic_cast<sends_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) sends_namerecord 
                  ((*loc)->getFullname(), (*loc)->getConf(),
                  fQuery);
               cerr << "load name " << (*loc)->getFullname() << endl;
               break;
            }
         // lars
         case dev_lars:
            {
               // check if namerecord supports lars
               if (dynamic_cast<lars_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) lars_namerecord 
                  ((*loc)->getFullname(), (*loc)->getConf(),
                  fQuery);
               cerr << "load lars " << (*loc)->getFullname() << endl;
               break;
            }
         // http
         case dev_http:
            {
               // check if namerecord supports http
               if (dynamic_cast<http_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) http_namerecord 
                  ((*loc)->getFullname(), (*loc)->getConf());
               break;
            }
         // function callback
         case dev_func:
            {
               // check if namerecord supports func
               if (dynamic_cast<func_namerecord*>(*loc)) {
                  return true;
               }
               rec = new (nothrow) func_namerecord 
                  (isOut(), (*loc)->getFullname(), (*loc)->getConf());
               break;
            }
         // eof
         case dev_eof:
            {
               return true;
            }
         // others not supported (yet)
         case dev_invalid:
         default:
            {
               return false;
            }
      }
   
      // check if new record was created
      if (!rec) {
         return false;
      }
      // if yes, replace previous one
      iosupport* io = dynamic_cast<iosupport*> (rec);
      if (io) io->setTimeLimits (fT0, fDt);
      delete *loc; 
      *loc = rec;
      return true;
   }

//______________________________________________________________________________
   void smartio_basic::ClearCache()
   {
      dir_support::ClearCache();
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// smart_input                                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   smart_input::smart_input (const char* conf)
   : smartio_basic (false), fEOF (false), fIn (0) 
   {
      if (conf && *conf && parseName (conf)) {
         addName ("eof://");   
      }
   }

//______________________________________________________________________________
   smart_input::~smart_input ()
   {
      terminate();
   }

//______________________________________________________________________________
   // bool smart_input::next (const bool* ctrlC) 
   // {
      // if (!fIn) {
         // if (!wait (ctrlC)) {
            // return false;
         // }
      // }
      // if (fIn) {
         // fNext = fIn->nexttime();
         // delete fIn; fIn = 0;
      // }
      // return true;
   // }

//______________________________________________________________________________
   bool smart_input::next (int n) 
   {
      if (n < 0) {
         return false;
      }
      if (fChildren.empty()) {
         if (n != 0) {
            return false;
         }
         else {
            // if (!fIn) {
               // if (!wait (ctrlC)) {
                  // return false;
               // }
            // }
            if (fIn) {
               fNext = fIn->nexttime();
               delete fIn; fIn = 0;
            }
            return true;
         }
      }
      else {
         int j = 0;
         for (childiterator ch = fChildren.begin(); 
             ch != fChildren.end(); ++ch) {
            int m = (*ch)->getFrameNum();
            if (n < j + m) {
               return (*ch)->next (n - j);
            }
         }
         return false;
      }
   }

//______________________________________________________________________________
   bool smart_input::eof()
   {
      if (!fChildren.empty()) {
         for (childiterator ch = fChildren.begin(); 
             ch != fChildren.end(); ) {
            if ((*ch)->eof()) {
               delete *ch;
               fChildren.erase (ch);
            }
            else {
               ch++;
            }
         }
         if (!fChildren.empty()) {
            return false;
         }
      }
      fMux.lock();
      bool ret = fEOF && (fIn == 0) && fInQueue.empty();
      fMux.unlock();
      return ret;
   }

//______________________________________________________________________________
   bool smart_input::prefetch (int count)
   {
      if (fState == io_inactive) {
         setState (io_active);
      }
      for (childiterator ch = fChildren.begin(); 
          ch != fChildren.end(); ++ch) {
         (*ch)->prefetch (count);
      }
      return true;
   }

//______________________________________________________________________________
   bool smart_input::wait (const bool* ctrlC)
   {
   start:
   
      // if no children, just wait for current
      if (fChildren.empty()) {
         if (fIn) {
            return true;
         }
         if (fState == io_inactive) {
            setState (io_active);
         }
         if  (fState == io_failed) {
            return false;
         }
         do {
            // cerr << "wait " << (fEOF ? "eof" : "no") << " " <<
               // (fInQueue.empty() ? "empty" : "full") << " " << 
               // ((fIn == 0) ? "0" : "1") << endl;
            // check eof
            fMux.lock();
            if (eof()) {
               fMux.unlock();
               return false;
            }
            // check Ctrl-C
            if (ctrlC && *ctrlC) {
               fMux.unlock();
               return false;
            }
            // check if queue is empty
            if (fInQueue.empty()) {
               fMux.unlock();
               timespec w = {0, _SIO_TICK * 1000};
               nanosleep (&w, 0);
            }
            // get next frame
            else {
               fIn = fInQueue.front().fFrame;
               fChildren = fInQueue.front().fChildren;
               fInQueue.pop_front();
               fMux.unlock();
               // check if childeren were added
               if (!fChildren.empty()) {
                  goto start;
               }
            }
         } while (!fIn);
         return true;
      }
      // otherwise loop over childern
      else {
         for (childiterator ch = fChildren.begin(); 
             ch != fChildren.end(); ) {
            if ((*ch)->wait (ctrlC)) {
               ch++;
            }
            // check if failed because of eof
            else if ((*ch)->eof()) {
               delete *ch;
               fChildren.erase (ch);
               // check if there are any childern left
               if (fChildren.empty()) {
                  goto start;
               }
            }
            else {
               return false;
            }
         }
         return true;
      }
   }

//______________________________________________________________________________
   bool smart_input::wait (const Time& t0, const bool* ctrlC)
   {
   start:
   
      // if no children, just wait for current
      if (fChildren.empty()) {
         if (fNext <= t0) {
            return wait (ctrlC);
         }
         else {
            return true;
         }
      }
      // otherwise loop over children
      else {
         for (childiterator ch = fChildren.begin(); 
             ch != fChildren.end(); ) {
            if ((*ch)->wait (t0, ctrlC)) {
               ch++;
            }
            else if ((*ch)->eof()) {
               delete *ch;
               fChildren.erase (ch);
               // check if there are any childern left
               if (fChildren.empty()) {
                  goto start;
               }
            }
            else {
               return false;
            }
         }
         return true;
      }
   }

//______________________________________________________________________________
   Time smart_input::nexttime (int n) const
   {
      if (n < 0) {
         return Time (0);
      }
      if (fChildren.empty()) {
         if (n != 0) {
            return Time (0);
         }
         else {
            return fNext;
         }
      }
      else {
         int j = 0;
         for (const_childiterator ch = fChildren.begin(); 
             ch != fChildren.end(); ++ch) {
            int m = (*ch)->getFrameNum();
            if (n < j + m) {
               return (*ch)->nexttime (n - j);
            }
         }
         return Time (0);
      }
   }

//______________________________________________________________________________
   int smart_input::getFrameNum () const
   {
      if (fChildren.empty()) {
         return 1;
      }
      else {
         int n = 0;
         for (const_childiterator ch = fChildren.begin(); 
             ch != fChildren.end(); ++ch) {
            n += (*ch)->getFrameNum();
         }
         return n;
      }
   }

//______________________________________________________________________________
   framefast::framereader* smart_input::getFrameIn (int n) const 
   {
      if (n < 0) {
         return 0;
      }
      if (fChildren.empty()) {
         return fIn;
      }
      else {
         int j = 0;
         for (const_childiterator ch = fChildren.begin(); 
             ch != fChildren.end(); ++ch) {
            int m = (*ch)->getFrameNum();
            if (n < j + m) {
               return (*ch)->getFrameIn (n - j);
            }
         }
         return 0;
      }
   }

//______________________________________________________________________________
   bool smart_input::loadFrameFrom (inputqueue_el& el, 
                     namelist::iterator loc, bool& keepname)
   {
      keepname = false;
      if (el.fFrame) {
         delete el.fFrame; el.fFrame = 0;
      }
      if (!el.fChildren.empty()) {
         for (childiterator ch = el.fChildren.begin();
             ch != el.fChildren.end(); ++ch) {
            if (*ch) delete *ch;
         }
         el.fChildren.clear();
      }
      if (!loadName (loc)) {
         return false;
      }
      std::string logmsg;
   
      // deal with directory first
      if ((*loc)->getDevType() == dev_dir) {
         // get name of next file
         string fname;
         if ((*loc)->getNextName (fname)) {
            addFileName (fname.c_str(), 0, false);
            keepname = true;
         }
         // do not read a frame here
         return false;
      }
      // deal with file series second
      filenamerecord* frec = dynamic_cast<filenamerecord*>(*loc);
      if (frec && (frec->getCont() > 0)) {
         // get name of next file
         string fname;
         if (frec->getNextName (fname, fT0, fDt)) {
            addFileName (fname.c_str(), 0, false);
            keepname = true;
         }
         // do not read a frame here
         return false;
      }
   
      // deal with archive (lars) next
      if ((*loc)->getDevType() == dev_lars) {
         // check frame streams
         lars_namerecord* lars = dynamic_cast<lars_namerecord*>(*loc);
         if (!lars) {
            // if not true lars, check if next name returns a name
            string url;
            if ((*loc)->getNextName (url)) {
               addName (url.c_str(), false);
               keepname = true;
            }            
            // do not read a frame here
            return false;
         }
         int n = lars->getFrameStreamNum();
         // Invalid stream count
         if (n < 1) {
            fMsg = "Error: Unable to load frames from ";
            fMsg += (*loc)->getFullname();
            cerr << fMsg << endl;
            return false;
         }
         // One stream - load next url
         if (n == 1) {
            string url = lars->getFrameUrl (0);
            if (url.empty()) {
               return false; // done
            }
            addName (url.c_str(), false);
            keepname = true;
            // do not read a frame here
            return false;
         }
         // Multiple streams - setup children
         if (n < 1) {
            *loc = 0; // lars will be delete after last child!
            for (int j = 0; j < n; j++) {
               namerecord* nr = lars->getNameRecord (j);
               if (!nr) {
                  el.clear();
                  return false;
               }
               smart_input* si = new (nothrow) smart_input ();
               if (!si) {
                  delete nr;
                  el.clear();
                  return false;
               }
               si->addName (nr);
               si->addName ("eof://");
               el.fChildren.push_back (si);
            }
            // do not read a frame here
            return false;
         }
      }
   
      // create a new empty frame
      el.fFrame = new (nothrow) framereader;
      if (!el.fFrame) {
         fMsg = "Error: Unable to create framereader.";
         return false;
      }
      switch ((*loc)->getDevType()) {
         // read from file
         case dev_file:
            {
               // string fname;
               // filenamerecord* frec = dynamic_cast<filenamerecord*>(*loc);
            //    // Continuation?
               // if (frec && (frec->getCont() > 0)) {
                  // if (frec->getNextName (fname)) {
                     // keepname = true;
                  // }
                  // else { // last
                     // return false;
                  // }
               // }
               // // no cont.
               // else {
                  // fname = (*loc)->getName();
               // }
               if (!el.fFrame->loadFile ((*loc)->getName(), true)) {
                  fMsg = "Error: Unable to open file.";
                  delete el.fFrame; el.fFrame = 0;
                  return false;
               }
               logmsg = (*loc)->getFullname();
               break;
            }
         
         // read from tape
         // read from DMT
         // read from NDS
         // read from NDS2
         // read from http
         case dev_tape:
         case dev_dmt:
         case dev_nds:
         case dev_sends:
         case dev_http:
         case dev_func:
            {
               // get next frame from buffer
               iosupport* io = dynamic_cast<iosupport*>(*loc);
               if (!io) {
                  return false;
               }
               if (!el.fFrame->loadFrame (io->readFrame())) {
                  fMsg = "Error: Unable to load frame from ";
                  fMsg += logmsg;
                  cerr << fMsg << endl;
                  delete el.fFrame; el.fFrame = 0;
                  return false;
               }
               // cerr << "FRAME READ -      -   --------------------------" << endl;
               // ofstream os ("test.dump");
               // os << *el.fFrame << endl;
               // os.close();
               // cerr << "FRAME READ -      -   --------------------------" << endl;
               // keep the name record if not eof
               logmsg = (*loc)->getFullname();
               logmsg += "/" + el.fFrame->guessFilename();
               if (!io->eof()) {
                  keepname = true;
               }
               break;
            }
         
         // read from ftp address
         case dev_ftp:
            {
               fMsg = "Error: FTP not implemented yet.";
               delete el.fFrame; el.fFrame = 0;
               return false;
               break;
            }
         
         // eof file encountered
         case dev_eof:
            {
               fMux.lock();
               fEOF = true;
               fMux.unlock();
               delete el.fFrame; el.fFrame = 0;
               return false;
               break;
            }
         
         // unrecognized medium
         default:
            {
               fMsg = "Error: Unrecognized input name.";
               delete el.fFrame; el.fFrame = 0;
               return false;
            }
      }
      el.fFrame->getTOC(); // make sure we read TOC here
      fantom::fmsgqueue::fmsg msg (logmsg.c_str(), el.fFrame->length());
      log (msg);
      return true;
   }

//______________________________________________________________________________
   int smart_input::queueTotal() const
   {
      int totalkb = 0;
      for (inputqueue::const_iterator i = fInQueue.begin();
          i != fInQueue.end(); i++) {
         //if ((*i)->isInMemory()) {
         totalkb += (*i).fFrame->length() / 1024;
         //}
      }
      return totalkb;
   }

//______________________________________________________________________________
   void smart_input::inputqueue_el::clear()
   {
      if (fFrame) {
         delete fFrame; 
         fFrame = 0;
      }
      if (!fChildren.empty()) {
         for (childiterator ch = fChildren.begin();
             ch != fChildren.end(); ++ch) {
            if (*ch) delete *ch;
         }
         fChildren.clear();
      }
   }

//______________________________________________________________________________
extern "C"
   void smart_i_cleanup2 (smart_input::inputqueue_el* el)
   {
      el->clear();
   }

//______________________________________________________________________________
   void smart_input::iothread()
   {
      fInputBusy = false;
      int qsize = 0;
      int oldqlen = -1;
      while (true) {
         pthread_testcancel();
         // check if something to read
         fMux.lock();
         if (oldqlen != (int)fInQueue.size()) {
            qsize = queueTotal();
            oldqlen = fInQueue.size();
            // cerr << "Input queue length = " << oldqlen <<
               // " size = " << qsize << "kB" << endl;
         }
         if ((fState != io_active) || fNames.empty() || 
            ((fQueueLen > 0) && ((int)fInQueue.size() >= fQueueLen)) ||
            ((fQueueLen <= 0) && (!fInQueue.empty() || fIn)) ||
            (qsize >= fQueueLimit)) {
            fMux.unlock();
            timespec w = {0, _SIO_TICK * 1000};
            nanosleep (&w, 0);
            continue;
         }
         // cerr <<  "add to input queue qlen = " << fInQueue.size() << 
            // " qsize = " << queueTotal() << endl;
         fInputBusy = true;
         namelist::iterator nextframe = fNames.begin();
         if (*nextframe == 0) {
            fNames.erase (nextframe);
            fMux.unlock();
            fInputBusy = false;
            continue;
         }
         fMux.unlock();
         // read next frame
      
         inputqueue_el el;
         pthread_cleanup_push ((cleanup_type)smart_i_cleanup2, &el);
         bool keepname = false;
         if (loadFrameFrom (el, nextframe, keepname)) {
            // add frame to queue
            if (el.fFrame->isFrame()) {
               fMux.lock();
               fInQueue.push_back (el);
               el.fFrame = 0;
               el.fChildren.clear();
               fMux.unlock();
               oldqlen = -1;
            }
            // read as list of names otherwise
            else {
               int len = fNames.size();
               addNameList (el.fFrame->frame(), el.fFrame->length(), 
                           false);
               int newlen = fNames.size();
               nextframe = fNames.begin() + (newlen - len);
               el.clear();
            }
         }
         if (!keepname) {
            fMux.lock();
            fNames.erase (nextframe);
            fMux.unlock();
         }
         pthread_cleanup_pop (0); 
         fInputBusy = false;
      }
   }

//______________________________________________________________________________
   bool smart_input::busy() const
   {
      semlock lockit (fMux);
      return fInputBusy;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// smart_output                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   smart_output::smart_output (const char* conf)
   : smartio_basic (true), fFrameType (FF), fFrameLen (1), 
   fFrameN (1), fFrameCompress (true), fFrameVersion (kDefaultFrameVersion), 
   fRunNum (0), fFrameNum (0), fOut (0) 
   {
      if (conf && *conf) parseName (conf);
   }

//______________________________________________________________________________
   smart_output::~smart_output ()
   {
      terminate();
   }

//______________________________________________________________________________
   bool smart_output::next (const bool* ctrlC)
   {
      //cerr << "next output frame" << endl;
      if (fOut == 0) {
         return true;
      }
      if (fState == io_inactive) {
         setState (io_active);
      }
      if  (fState == io_failed) {
         return false;
      }
      // check queue length and size
      int qsize = 0;
      int oldqlen = -1;
      while (true) {
         // check Ctrl-C
         if (ctrlC && *ctrlC) {
            return false;
         }
         fMux.lock();
         if (oldqlen != (int)fOutQueue.size()) {
            qsize = queueTotal();
            oldqlen = fOutQueue.size();
            // cerr << "Output queue length = " << oldqlen <<
               // " size = " << qsize << "kB" << endl;
         }
         if (((int)fOutQueue.size() < max (fQueueLen, 1)) &&
            (qsize < fQueueLimit)) {
            // cerr <<  "add to output queue qlen = " << fOutQueue.size() << 
               // " qsize = " << queueTotal() << endl;
            fMux.unlock();
            break;
         }
         fMux.unlock();
         timespec w = {0, _SIO_TICK * 1000};
         nanosleep (&w, 0);
      }
   
      // move to output queue
      fMux.lock();
      fOutQueue.push_back (fOut);
      fOut = 0;
      fMux.unlock();
      // wait for queue to empty, if max. queue length is zero
      if (fQueueLen <= 0) {
         while (true) {
            // check Ctrl-C
            if (ctrlC && *ctrlC) {
               return false;
            }
            fMux.lock();
            if (fOutQueue.empty()) {
               fMux.unlock();
               break;
            }
            fMux.unlock();
            timespec w = {0, _SIO_TICK * 1000};
            nanosleep (&w, 0);
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool smart_output::setType (const char* type)
   {
      return string_to_fformat (type, fFrameType, fFrameLen, 
                           fFrameN, fFrameCompress, fFrameVersion);
   }

//______________________________________________________________________________
   framefast::framewriter* smart_output::createFrame()
   {
      if (fOut) {
         return fOut;
      }
      fOut = new (nothrow) framewriter (fFrameLen, fFrameN, 
                           fFrameCompress, fFrameVersion);
      if (fOut) {
         fOut->setDetectorInfo (fDetector);
         fOut->setRunNum (fRunNum);
         fOut->setFrameNum (fFrameNum);
         fFrameNum += fFrameN;
      }
      return fOut;
   }

//______________________________________________________________________________
extern "C"
   void smart_o_cleanup1 (framefast::basic_frameout** c)
   {
      if (*c) delete *c;
   }

//______________________________________________________________________________
   bool smart_output::saveFrameTo (framefast::framewriter* fr, 
                     namelist::iterator loc, bool& keepname)
   {
      keepname = false;
      if (!loadName (loc)) {
         cerr << "load failed for " << (*loc)->getFullname() << endl;
         return false;
      }
      bool error = false;
      string fname = fr->guessFilename() + extension (fFrameType);
      string::size_type pos = fname.find ("-R-");
      if (pos != string::npos) {
         fname.erase (pos, 3);
         fname.insert (pos, "-ldx-");
      }
      framefast::basic_frameout* out = 0;
      pthread_cleanup_push ((cleanup_type)smart_o_cleanup1, &out); 
      std::string logmsg = name_from_dev ((*loc)->getDevType());
   
      switch ((*loc)->getDevType()) {
         // write to file
         case dev_file:
            {
               //cerr << "get file writer for " << fname <<endl;
               fname = (*loc)->getName();
               out = new (nothrow) file_out (fname.c_str());
               logmsg += fname;
               break;
            }
         
         // write to directory
         case dev_dir:
            {
               // get name of next file
               //cerr << "get dir writer for " << fname <<endl;
               dir_namerecord* drec = dynamic_cast<dir_namerecord*>(*loc);
               string outname;
               if (drec->setNextFilename (outname, fname)) {
                  // cerr << "write " << fname << " to dir " << outname << endl;
                  out = new (nothrow) file_out (outname.c_str());
                  logmsg += outname;
                  if (logmsg.find ("dir://") != string::npos) {
                     logmsg.erase (0, 6);
                     logmsg.insert (0, "file://");
                  }
                  keepname = true;
               }
               break;
            }
         
         // write to tape, dmt, port, inet, ftp, http, nds, func
         case dev_tape:
         case dev_dmt:
         case dev_lars:
         case dev_http:
         case dev_ftp:
         case dev_nds:
         case dev_func:
            {
               // get writer
               //cerr << "get " << name_from_dev ((*loc)->getDevType()) << " writer for " << fname <<endl;
               iosupport* io = dynamic_cast<iosupport*>(*loc);
               if (io) {
                  out = io->getWriter (fname.c_str());
               }
               if (out) {
                  keepname = true;
               }
               logmsg += fname;
               break;
            }
         
         // unrecognized medium
         default:
            {
               //fMsg = "Error: Unrecognized medium.";
            }
      }
   
      error = !out;
      if (out) {
         fr->setFilename (fname.c_str());
         error = !fr->write (out);
         if (!error) {
            fantom::fmsgqueue::fmsg msg (logmsg.c_str(), out->length());
            log (msg);
         }
      }
      pthread_cleanup_pop (1); 
      return !error;
   }

//______________________________________________________________________________
   int smart_output::queueTotal() const
   {
      int totalkb = 0;
      for (outputqueue::const_iterator i = fOutQueue.begin();
          i != fOutQueue.end(); i++) {
         totalkb += (*i)->total() / 1024;
      }
      return totalkb;
   }

//______________________________________________________________________________
extern "C"
   void smart_o_cleanup (framefast::framewriter** fr)
   {
      if (*fr) delete *fr;
   }

//______________________________________________________________________________
   void smart_output::iothread()
   {
      fOutputBusy = false;
      while (true) {
         pthread_testcancel();
         // check if something to write
         fMux.lock();
         if (fNames.empty() || fOutQueue.empty() || (fState != io_active)) {
            fMux.unlock();
            timespec w = {0, _SIO_TICK * 1000};
            nanosleep (&w, 0);
            continue;
         }
         // get name record
         // cerr << "remove from output queue qlen = " << fOutQueue.size() << 
            // " qsize = " << queueTotal() << endl;
         namelist::iterator nextframe = fNames.begin();
         if (*nextframe == 0) {
            fNames.erase (nextframe);
            fMux.unlock();
            continue;
         }
      
         // setup cleanup
         framefast::framewriter* fr = 0;
         pthread_cleanup_push ((cleanup_type)smart_o_cleanup, &fr); 
         // get output frame
         fr = fOutQueue.front();
         fOutQueue.pop_front();
         fOutputBusy = true;
         fMux.unlock();
         // write frame to file
         bool keepname = false;
         if (!saveFrameTo (fr, nextframe, keepname)) {
            cerr << "Unable to write frame " << fr->getFilename() << endl;
         }
         if (!keepname) {
            fMux.lock();
            fNames.erase (nextframe);
            fMux.unlock();
         }
         fOutputBusy = false;
         pthread_cleanup_pop (1); 
      }
   }

//______________________________________________________________________________
   bool smart_output::busy() const
   {
      semlock lockit (fMux);
      return !fOutQueue.empty() || fOutputBusy;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// smart_io (specialization)						//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
template <>
   bool smart_io<smart_input>::Add (int num, const char* conf)
   {
      iterator i = fIO.find (num);
      if (i != fIO.end()) {
         fMsg = "Error: Cannot add channel";
         return false;
      }
      else {
         smartio_channel* inp = 0;
         inp = new (std::nothrow) smart_input (conf);
         if (!inp) {
            fMsg = "Error: insufficient memory";
            return false;
         }
         if (!(*inp)) {
            fMsg = inp->Message();
            delete inp;
            return false;
         }
         inp->setup();
         smartio_list::value_type val (num, inp);
         fIO.insert (val);
         return true;
      }
   }

//______________________________________________________________________________
template <>
   bool smart_io<smart_output>::Add (int num, const char* conf)
   {
      iterator i = fIO.find (num);
      if (i != fIO.end()) {
         fMsg = "Error: Cannot add channel";
         return false;
      }
      else {
         smartio_channel* out = 0;
         out = new (std::nothrow) smart_output (conf);
         if (!out) {
            fMsg = "Error: insufficient memory";
            return false;
         }
         if (!(*out)) {
            fMsg = out->Message();
            delete out;
            return false;
         }
         out->setup();
         smartio_list::value_type val (num, out);
         fIO.insert (val);
         return true;
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// smart_io (instantiation)						//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
template class smart_io<smart_input>;
template class smart_io<smart_output>;

#ifdef __GNUG__
   void bogus_smartio_fix (smart_io<smart_input>& a, 
                               smart_io<smart_output>& b) {
      a.Add(1, "");
      b.Add(1, "");
   }
#endif

}
