/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: fname							*/
/*                                                         		*/
/* Module Description: supoort for name handling			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 4Oct00   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: framefast.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FNAME_H
#define _LIGO_FNAME_H


#include "Time.hh"
#include "Interval.hh"
#include "fantomtype.hh"
#include "framedir.hh"
#include <string>
#include <deque>


namespace fantom {



/** @name Name handling support
    This header defines utility functions to deal with names.
   
    @memo Name handling support
    @author Written October 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Get the device type from a names.
    
    @param dname Device name
    @return Device type
    @memo Name to device type.
 ************************************************************************/
   devicetype dev_from_name (const char* dname);

/** Get the device name from the type.
    
    @param dtype Device type
    @return Device name
    @memo Device type to name.
 ************************************************************************/
   std::string name_from_dev (devicetype dtype);


/** Get the frame format string from frame type, length, number of
    files and compression.
    
    @param ftype Frame type
    @param len Length of frames (in sec)
    @param num Number of frames per file
    @param compr Compression ID
    @param vers Frame version
    @return Frame format string
    @memo Frame format from parameters.
 ************************************************************************/
   std::string fformat_to_string (frametype ftype = framefast::FF,
                     int len = 0, int num = 1, int compr = 0, 
                     int vers = 0);


/** Get the frame format parameters from frame string.
    
    @param format Frame format string
    @param ftype Frame type (return)
    @param len Length of frames (in sec) (return)
    @param num Number of frames per file (return)
    @param compr Compression ID (return)
    @param vers Frame version
    @return True if successful
    @memo Frame format from parameters.
 ************************************************************************/
   bool string_to_fformat (const char* format, frametype& ftype, 
                     int& len, int& num, int& compr, int& vers);


/** Name record.
    
    @memo Name record.
 ************************************************************************/
   class namerecord {
   public:
      /// Constructor 
      explicit namerecord (const char* name, const char* conf = 0) 
      : fConf (conf ? conf : ""), fDevType (dev_invalid) {
         setName (name);
      }
      /// Destructor
      virtual ~namerecord() {
      }
      /// Set name
      void setName (const std::string& name);
      /// Get name 
      const char* getName() const {
      
         return fName.c_str(); }
      /// Get fullname 
      const char* getFullname() const {
         return fFullname.c_str(); }
      /// Set configuration string
      void setConf (const char* conf) {
         fConf = conf ? conf : ""; }
      /// Get name 
      const char* getConf() const {
         return fConf.empty() ? 0 : fConf.c_str(); }
      /// Get device type
      devicetype getDevType() const {
         return fDevType; }
   
      /// Get next name
      virtual bool getNextName (std::string& name) {
         return false; }
   
   protected:
      /// Name
      std::string		fFullname;
      /// Name
      std::string		fName;
      /// Configuration string
      std::string		fConf;
      /// Device type
      devicetype		fDevType;
   };


/** Frame file name record. Includes start time and duration of 
    frame(s).
    
    @memo Frame file name record.
 ************************************************************************/
   class filenamerecord : public namerecord {
   public:
      /// Constructor 
      explicit filenamerecord (const char* name, const char* conf,
                        const Time& t0, const Interval& dt) 
      : namerecord (name, 0), fT0 (t0), fDt (dt), fNCont (0), 
      fN (0), fNMax (1) {
         if (conf) setConf (conf); }
      /// Destructor
      virtual ~filenamerecord() {
      }
      /// Return number of additional files in series
      unsigned int getCont() const {
         return fNCont; }
      /// Check if frame is outside specified time range
      bool isOutside (const Time& t0, const Interval& dt) const;
      /// Set configuration string
      bool setConf (const char* conf);
      /// Get next file name
      bool getNextName (std::string& fname);
      /// Get next file name (with time restriction)
      bool getNextName (std::string& fname, 
                       const Time& t0, const Interval& dt);
   protected:
      /// GPS start time
      Time		fT0;
      /// Duration
      Interval		fDt;
      /// Continuation
      unsigned int	fNCont;
      /// Current count
      unsigned int      fN;
      /// Max count
      unsigned int      fNMax;
      /// frame file series
      ffData		fData;
   };


/** Name record list.
    
    @memo Name record list.
 ************************************************************************/
   class namelist : protected std::deque<namerecord*> {
   public:
      typedef std::deque<namerecord*> namerecordlist;
      typedef namerecordlist::iterator iterator;
      typedef namerecordlist::const_iterator const_iterator;
   
      /// Constructor
      namelist() {
      }
      /// Destructor
      virtual ~namelist();
   
      /// add a new name by parsing the command line arguments
      virtual bool parseName (devicetype dtype, const char* arg);
      /// add a new name by parsing the configuration line
      virtual bool parseName (const char* conf);
      /// add a new name (record gets adopted)
      virtual bool addName (namerecord* name, bool back = true);
      /// add a new name
      virtual bool addName (const char* name, bool back = true);
      /// add a new name
      virtual bool addName (const char* name, const char* conf,
                        bool back = true);
      /// add a new name representing a file
      virtual bool addFileName (const char* name, unsigned int cont = 0,
                        bool back = true);
      /// add a new name representing a file or a file list (wildcard)
      virtual int addFiles (const char* name, bool back = true);
      /// add a list of names to the beginnin of the list
      virtual bool addNameList (const char* mem, int len, bool back = true);
      /// Remove a name from the list and return it (caller owns!)
      virtual namerecord* removeName (bool back = false);
      /// Begin
      iterator begin() {
         return namerecordlist::begin(); }
      /// End
      iterator end() {
         return namerecordlist::end(); }
      /// Front
      namerecord& front() {
         return *namerecordlist::front(); }
      /// back
      namerecord& back() {
         return *namerecordlist::back(); }
      /// Erase
      void erase (iterator pos);
      /// Clear list
      void clear();
      /// Empty list?
      bool empty() {
         return namerecordlist::empty(); }
      /// Size
      int size() {
         return namerecordlist::size(); }
   
      /// Last error message
      const char* Message () const {
         return fMsg.c_str(); }
   
   protected:
      /// Error message
      std::string	fMsg;
   };

}

#endif // _LIGO_FNAME_H



