/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: udnls							*/
/*                                                         		*/
/* Module Description: lists frame file UDNs 				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 7Sep02   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: udnls.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_UDNLS_H
#define _LIGO_UDNLS_H


namespace fantom {


/** @name udnls
    \begin{verbatim}
    usage: udnls [-n] 'frame file(s)'
           -n : suppresses head
    \end{verbatim}
    Prints a sorted list of frame file UDNs.
   
    @memo Lists frame file UDNs
    @author Written September 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/


}


#endif // _LIGO_UDNLS_H


