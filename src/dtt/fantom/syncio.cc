#include "syncio.hh"
#include "gmutex.hh"
#include <stdio.h>
#include <map>

namespace fantom {
   using namespace std;
   using namespace framefast;
   using namespace thread;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Global named barrier collection 				        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class named_barrier {
   public:
      named_barrier (const char* syncname, int tasknum);
   
   protected:
      class barrier_imp : public barrier {
      public: 
         barrier_imp (int tnum) : barrier (tnum), fNum (tnum) {
         }
         bool last() {
            semlock lockit (fMux);
            return --fNum <= 0; }
      protected:
         mutex		fMux;
         int 		fNum;
      };
   
   protected:
      typedef std::map<std::string, barrier_imp*> barrierlist;
      typedef barrierlist::iterator b_iterator;
   
      /// barrier list
      static barrierlist 	fList;
      /// barrier list mutex
      static mutex		fMux;
   };

//______________________________________________________________________________
   named_barrier::barrierlist named_barrier::fList;

//______________________________________________________________________________
   mutex named_barrier::fMux;

//______________________________________________________________________________
   named_barrier::named_barrier (const char* syncname, int tasknum)
   {
      // create new barrier if not yet in list
      fMux.lock();
      b_iterator bi = fList.find (syncname);
      barrier_imp* b = 0;
      if (bi == fList.end()) {
         b = fList[syncname] = new (nothrow) barrier_imp (tasknum);
      }
      else {
         b = bi->second;
      }
      fMux.unlock();
      // wait at barrier
      b->wait();
      // check if last
      fMux.lock();
      if (b->last()) {
         fList.erase (syncname);
      }
      fMux.unlock();
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// sync_support		 	 				        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   sync_support::sync_support (const char* syncname, int tasknum)
   : fEof (false), fSyncname (syncname), fTasknum (tasknum) 
   {
   }

//______________________________________________________________________________
   sync_support::~sync_support() 
   {
   }

//______________________________________________________________________________
   framefast::basic_frame_storage* sync_support::readFrame ()
   {
      fEof = true;
   
      if (fTasknum <= 1) {
         return 0;
      }
      named_barrier (fSyncname.c_str(), fTasknum);
      return 0;
   }

//______________________________________________________________________________
   int sync_support::fUniqueId = 0;

//______________________________________________________________________________
   std::string sync_support::unique()
   {
      char buf[256];
      sprintf (buf, "Barrier###^^^&&&___%i", fUniqueId++);
      return buf;
   }


}
