#include <time.h>
#include "goption.hh"
#include "Time.hh"
#include "Interval.hh"
#include "dmtio.hh"
#include "framefast/frametype.hh"
#include "framefast/frameio.hh"
#include "lsmp_prod.hh"
#include "lsmp_con.hh"
#include <iostream>
#include <cstdlib>
#include <cstring>

namespace fantom {
   using namespace std;
   using namespace gdsbase;
   using namespace thread;

   const double kDmtTimeout = 3.0;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dmt frame storage                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class dmt_frame_storage : public framefast::memory_frame_storage {
   public:
      /// Default constructor
      dmt_frame_storage (dmt_support& dsup, bool nowait) 
      : memory_frame_storage (0, 0), fDMT (&dsup), fNoWait (nowait) {
         load (); }
      /// Destructor
      virtual ~dmt_frame_storage() {
         reset(); }
      /// Release the frame (protect against multiple calls!)
      virtual void reset();
      /// Load a frame
      bool load ();
   
   protected:
      /// Pointer to DMT support
      dmt_support*	fDMT;
      /// No wait?
      bool		fNoWait;
   };


//______________________________________________________________________________
   void dmt_frame_storage::reset()
   {
      if (fData && fDMT->fConsumer) {
         fDMT->fConsumer->free_buffer();
         fDMT->fMux.unlock();
      }
      memory_frame_storage::reset();
   }

//______________________________________________________________________________
   bool dmt_frame_storage::load ()
   {
      reset();
   
      //cerr << "read dmt buffer" << endl;
      if (fDMT->fOut || !fDMT->fConsumer) {
         cerr << "read dmt buffer failed 1" << endl;
         return false;
      }
      // get next buffer
      fDMT->fMux.lock();
      if (!fNoWait) {
         fData = fDMT->fConsumer->get_buffer (0);
      }
      else {
         Time stop = Now() + Interval (kDmtTimeout);
         do {
            fData = fDMT->fConsumer->get_buffer (NOWAIT);
            if (fData) 
               break;
            timespec tick = {0, 100000000};
            nanosleep (&tick, 0);
         } while (Now() < stop);
      }
      fOwn = false;
      if (!fData) {
         cerr << "read dmt buffer failed 2" << endl;
         fDMT->fMux.unlock();
         return false;
      }
      fLength = fDMT->fConsumer->getLength();
      if (fLength <= 0) {
         reset();
         cerr << "read dmt buffer failed 3" << endl;
         return false;
      }
      //cerr << "got dmt buffer of size " << fLength << " B" << endl;
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dmt frame out                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class dmt_frameout : public framefast::basic_frameout {
   public:
      /// Constructor
      dmt_frameout (dmt_support& dsup);
      /// Destructor
      virtual ~dmt_frameout ();
      /// Open
      virtual bool open (int flen = 0);
      /// Close
      virtual void close();
      /// Write
      virtual bool write (const char* p, int len);
      /// Scatter-gather write
      virtual bool write (const src_dest_t* s, int slen);
   
   protected:
      /// Pointer to DMT support
      dmt_support*	fDMT;
      /// Length of data
      int		fLen;
      /// buffer pointer
      char*		fBuf;
   };

//______________________________________________________________________________
   dmt_frameout::dmt_frameout (dmt_support& dsup)
   : fDMT (&dsup), fLen (0), fBuf (0)
   {
   }

//______________________________________________________________________________
   bool dmt_frameout::open (int flen)
   {
      // check length of buffer
      if (fDMT->getBufLen() && (flen > fDMT->getBufLen())) {
         return  false;
      }
      // allocate buffer
      fDMT->fMux.lock();
      fBuf = fDMT->fProducer->get_buffer();
      if (!fBuf) fDMT->fMux.unlock();
      fLen = flen;
      return (fBuf != 0);
   }

//______________________________________________________________________________
   void dmt_frameout::close()
   {
      // free buffer
      if (fBuf) {
         fDMT->fProducer->release (fSofar);
         fDMT->fMux.unlock();
      }
      fBuf = 0;
   }

//______________________________________________________________________________
   bool dmt_frameout::write (const char* p, int size)
   {
      //cerr << "Write " << size << "B" << endl;
   
      // check buffer
      if (!fBuf || !p) {
         return false;
      }
      if (size <= 0) {
         return true;
      }
       // check length
      if (fSofar + size > fLen) {
         size = fLen - fSofar;
      }
      // copy data
      memcpy (fBuf + fSofar, p, size);
      fSofar += size;
      return true;
   }

//______________________________________________________________________________
   bool dmt_frameout::write (const src_dest_t* s, int slen)
   {
      return basic_frameout::write (s, slen); 
   }

//______________________________________________________________________________

   dmt_frameout::~dmt_frameout ()
   {
      close();
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dmt_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dmt_support::~dmt_support() 
   {
      if (fProducer) delete fProducer;
      if (fConsumer) delete fConsumer;
   }

//______________________________________________________________________________
   bool dmt_support::setPname (const char* pname, 
                     const char* conf, bool open) 
   {
      // clean up first
      if (fProducer) delete fProducer;
      fProducer = 0;
      if (fConsumer) delete fConsumer;
      fConsumer = 0;
      // check partition name; remove leading /
      if (!pname || !*pname) {
         fPname = "";
         return true;
      }
      fPname = (*pname == '/') ? pname + 1 : pname;
      if (fPname.empty()) {
         return true;
      }
      option_string opts (pname, conf, "l:n:o");
      //cerr << "DMT CONF IS " << (conf ? conf : "") << endl;
      string s;
      if (opts.getOpt ('l', s)) {
         fBufLen = atoi (s.c_str());
      }
      if (opts.getOpt ('n', s)) {
         fBufNum = atoi (s.c_str());
      }
      if (fBufNum == 1) fBufNum = 2;
      if (fBufNum < 0) fBufNum = 0;
      if (fBufLen < 0) fBufLen = 0;
      fOffline = opts.opt ('o');
      if (!open) {
         return true;
      }
      if (fOut) {
         //cerr << "create new producer @ " << fPname << endl;
         fProducer = fBufLen && fBufNum ? 
            new (nothrow) LSMP_PROD (fPname.c_str(), fBufNum, fBufLen) :
            new (nothrow) LSMP_PROD (fPname.c_str());
         if (fProducer && fOffline) {
            //cerr << "set buffer more to offline" << endl;
            fProducer->bufmode (3);
         }
      }
      else {
         //cerr << "create new consumer @ " << fPname << endl;
         fConsumer = new (nothrow) LSMP_CON (fPname.c_str(), 0);
         if (fConsumer && fOffline) {
            fConsumer->bufmode (3);
         }
      }
      return (fConsumer || fProducer);
   }

//______________________________________________________________________________
   int dmt_support::readBuffer (char*& data, int max)
   {
      //cerr << "read dmt buffer" << endl;
      if (fOut || !fConsumer) {
         cerr << "read dmt buffer failed 1" << endl;
         return -1;
      }
      // get next buffer
      const char* buf = fConsumer->get_buffer();
      if (!buf) {
         cerr << "read dmt buffer failed 2" << endl;
         return -1;
      }
      int size = fConsumer->getLength();
      if (size <= 0) {
         fConsumer->free_buffer();
         cerr << "read dmt buffer failed 3" << endl;
         return -1;
      }
      // allocate data array for frame buffer if necessary
      if (data == 0) {
         data = new (nothrow) char [size+1];
         if (!data) {
            fConsumer->free_buffer();
            cerr << "read dmt buffer failed 4" << endl;
            return -1;
         }
      }
      else {
         if (max < size) max = size;
      }
      // copy data
      memcpy (data, buf, size);
      fConsumer->free_buffer();
      //cerr << "read dmt buffer of size " << size << " B" << endl;
      return size;
   }

//______________________________________________________________________________
   framefast::basic_frame_storage* dmt_support::readFrame (bool nowait)
   {
      framefast::basic_frame_storage* f =
         new (nothrow) dmt_frame_storage (*this, nowait);
      if (nowait && (f->data() == 0)) {
         delete f;
         return 0;
      }
      else {
         return f;
      }
   }

//______________________________________________________________________________
   framefast::basic_frameout* dmt_support::getWriter (const char* fname)
   {
      if (!fOut || !fProducer) {
         return 0;
      }
      return new (nothrow) dmt_frameout (*this);
   }


}
