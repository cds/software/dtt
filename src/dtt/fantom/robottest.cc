#include <time.h>
#include <unistd.h>
#include <iostream>
#include "robotctrl.hh"

   using namespace std;
   using namespace fantom;


           
   int main (int argn, char* argv[])
   {
      if (argn <= 1) {
         cout << "usage: " << argv[0] << " 'robot conf.'" << endl;
         return 1;
      }
      robot_ctrl robot (argv[1], "/dev/rmt/0");
      if (robot.eof()) {
         cerr << "Invalid robot configuration " << argv[1] << endl;
         return 1;
      }
      cout << "Start robot" << endl;
      int tape = 1;
      while (robot.next()) {
         cout << "working on tape " << tape << " ..." ;
         sleep (30);
         cout << " done" << endl;
         tape++;
      }
      if (robot.eof()) {
         cout << "Robot sucessfully stopped" << endl;
      }
      else {
         cerr << "Error while loading tape " << tape << endl;
      }
      return 0;
   }
