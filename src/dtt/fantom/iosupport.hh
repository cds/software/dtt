/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dirio							*/
/*                                                         		*/
/* Module Description: directory support for smartio			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 22Nov00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dirio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_IOSUPPORT_H
#define _LIGO_IOSUPPORT_H

#include "Time.hh"
#include "Interval.hh"

namespace framefast {
   class basic_frame_storage;
   class basic_frameout;
}

namespace fantom {


/** @name Abstract IO support class
   
    @memo IO support
    @author Written November 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class iosupport {
   public:
      /// Default constructor
      iosupport() {
      }
      /// Desctructor
      virtual ~iosupport() {
      }
      /// Read next frame into buffer (caller owns return object!)
      virtual framefast::basic_frame_storage* readFrame () = 0;
      /// Get frame writer (caller owns return object!)
      virtual framefast::basic_frameout* getWriter (const char* fname) = 0;
      /// End of file?
      virtual bool eof() const {
         return false; }
   
      /// Set time limits
      virtual void setTimeLimits (const Time& t0, const Interval& dt) {
         fT0 = t0; fDt = dt; }
      /// Get time limits
      virtual void getTimeLimits (Time& t0, Interval& dt) const {
         t0 = fT0; dt = fDt; }
   
   protected:
      /// time interval limit (0 = start at beginning)
      Time		fT0;
      /// time duration limit (<=0 = infinite)
      Interval		fDt;
   };


}

#endif // _LIGO_IOSUPPORT_H
