/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: syncio							*/
/*                                                         		*/
/* Module Description: syncronization support for smartio		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 22Nov00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dirio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_SYNCIO_H
#define _LIGO_SYNCIO_H

#include "iosupport.hh"
#include <string>

namespace fantom {


/** @name This is not an IO class by itself but rather a barrier used
    to synchronize multiple frame streams. When initializeing the
    class one has to specify how many tasks have to be synchronized. 
    A call to the readFrame method will return 0 after all tasks 
    have called thier respective readFrame method. The specified 
    barrier name has to be unique. Use the static method unique
    to obtain a new unique name for temporary purposes.
   
    @memo Barrier synchronization class
    @author Written April 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class sync_support : public iosupport {
   public:
      /// Default constructor
      sync_support (const char* syncname, int tasknum);
      /// Desctructor
      virtual ~sync_support();
      /// Read next frame into buffer (caller owns return object!)
      virtual framefast::basic_frame_storage* readFrame ();
      /// Get frame writer (caller owns return object!)
      virtual framefast::basic_frameout* getWriter (const char* fname) {
         return 0; }
      /// End of file?
      virtual bool eof() const {
         return fEof; }
      /// Get barrier name 
      const char* getName() const {
         return fSyncname.c_str(); }
      /// Get the number of tasks to sync. at the barrier
      int getTasknum() const {
         return fTasknum; }
   
      /// Return unique barrier name
      static std::string unique();
   
   protected:
      /// EOF
      bool		fEof;
      /// Name of syncronization/barrier object
      std::string	fSyncname;
      /// Number of tasks to synchronize by the barrier
      int		fTasknum;
      /// Unique ID count
      static int	fUniqueId;
   };


}

#endif // _LIGO_SYNCIO_H
