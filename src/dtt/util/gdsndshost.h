/* Version $Id: gdsndshost.h 6413 2011-04-01 18:05:35Z james.batch@LIGO.ORG $ */

/**
 *  Get the ip address as a string from a hostname.
 * @param hostname The hostname to look up.
 * @param host_ip a pre-allocated buffer for holding the plain text ip address.
 * @param host_len the length of the host_ip buffer.  The function returns an error code if the buffer is too small.
 * @return 0 if successful.  An error code otherwise.
 * -1 No hostname provided
 * -2 invalid buffer
 * -3 not enough space in buffer
 * -4 Can't resolve hostname to an IP.
 * -5 Hostname resolved to an unsupported address type
 * -6 Unknown inet_ntop() error, check errno
 */
int getHostAddress(char *hostname, char *host_ip, unsigned int host_len) ;

/* Parse through the NDSSERVER environment variable if it exists.
 * Ideally, the environment variable contains a comma-separated list
 * of hostname:port values on which nds services might be found. As
 * an example:
 * NDSSERVER=h2nds0:8088,h2nds1:8088,ldas-pcdev1.ligo.caltech.edu:31200
 * 
 * By default, this function will copy "nds" to hostname0, "nds1" to 
 * hostname1, and 8088 to port0, port1.  If the NDSSERVER environment
 * variable is present, it can specifiy alternate values for the nds
 * hostnames and ports.  Using the above example, "h2nds0" will be copied
 * to hostname0, "h2nds1" will be copied to hostname1, and the value 8088
 * will be written to port0 and port1.
 * 
 * For the NDSSERVER value, all parts (hostname0, port0, hostname1, port1)
 * are optional, if not present the default values will be used.
 * A value of "h2nds0,h2nds1" will overwrite the default hostname values
 * while leaving the port numbers unchanged.
 * A value of ":8087" would overwrite port0 with 8087 while leaving the
 * other values unchanged.
 * A value of "h2nds0,:8087" would overwrite hostname0 with h2nds0 and
 * port1 with 8087, leaving hostname1 and port0 as defaults.
 * A value of ",,ldas-pcdev1.caltech.edu:31200" would leave hostname0, port0
 * hostname1, and port1 as their default values.
 * 
 * A limit of 255 characters may be specified for hostnames, and 5 characters 
 * for port numbers (since a port number has a max value of 65535).
 * The return value in the range 0 to 4 is successful.  Anything else,
 * some error occurred.
 *
 * Parameters: hostname0, hostname1 - character pointers to arrays of characters at
 *                                    least 256 characters long.  These arrays will
 *                                    be overwritten.
 *             port0, port1 - Pointers to integers which will have port numbers 
 *                            overwritten.
 */
int
getNDSHostPort(char *hostname0, int *port0, char *hostname1, int *port1) ;

/* The function parseNDSSERVERval performs the same function as getNDSHostPort
 * but does not obtain the NDSSERVER environment variable value first. The calling
 * function must obtain the value and pass a pointer to the value's address to 
 * the function as the first argument.  The function will extract the first two
 * host:port values from the comma-separated list, and modify the address to point
 * to the remainder of the string.  This would be useful if either the first two
 * values are of no interest, or a complete list of values are desired in which case
 * the hostnames and port numbers returned may be combined with the remainder.
 * 
 * As an example, if *str is ",:8085,host3:port3", on return *str will be "host3:port3"
 * and hostname0, port0, hostname1, and port1 can be used to construct a complete
 * list of "nds:8088,nds1:8085,host3:port3".  This could be used to create a 
 * menu of nds hosts.
 * 
 * The str argument must point to memory that can be modified. Note that the comma
 * separating the first two values from the remainder is not included when *str is 
 * modified.
 */
int
parseNDSSERVERval(char **str, char *hostname0, int *port0, char *hostname1, int *port1) ;

