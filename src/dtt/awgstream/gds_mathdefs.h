//
// Created by erik.vonreis on 12/9/19.
// Use this file in place of math.h in order that certain bits of newer code
// will compile with older versions of GCC.
//

#ifndef DTT_CDS_GDS_MATHDEFS_H
#define DTT_CDS_GDS_MATHDEFS_H

#include <math.h>
#ifndef M_PI
   #define M_PI 	3.14159265358979323846
#endif

#endif //DTT_CDS_GDS_MATHDEFS_H
