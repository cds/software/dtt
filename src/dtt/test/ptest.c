/* version $Id: ptest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "gdsutil.h"

   int
   #ifdef OS_VXWORKS
   ptest(int argc, char *argv[] )
   #else
   main(int argc, char *argv[] )
   #endif     
   {
      FILE *fp;
      char cbuf[PARAM_ENTRY_LEN];
      char fi[PARAM_ENTRY_LEN];
      char sec_name[PARAM_ENTRY_LEN];
      char par_name[PARAM_ENTRY_LEN];
      char ptype[PARAM_ENTRY_LEN];
      char *sp, *entry;
      int rwd;
      int i = 0;
      int nentry;
      int cursor = -1;
      int I;
      double D;
   
      if( argc < 2 ) {
         while(1) {
            printf("Enter filename (or q) > "); scanf("%s", fi);
            if(tolower(*fi) == 'q')
               return 0;
            printf("Enter section name > "); scanf("%s", sec_name);
            printf("Enter parameter name > "); scanf("%s", par_name);
            printf("Enter parameter type (b/i/f/s) > "); scanf("%s", ptype);
            switch(tolower(*ptype)) {
               case 'b' :
                  if(loadBoolParam(fi, sec_name, par_name, &I))
                     printf("\t\tError\n");
                  else
                     printf("\t%s\n", I ? "TRUE" : "FALSE");
                  break;
            
               case 'i' :
                  if(loadIntParam(fi, sec_name, par_name, &I))
                     printf("\t\tError\n");
                  else
                     printf("\tint %d\n", I);
                  break;
               case 'f' :
                  if(loadFloatParam(fi, sec_name, par_name, &D))
                     printf("\t\tError\n");
                  else
                     printf("\tfloat %9g\n", D);
                  break;
               default :
                  if(loadStringParam(fi, sec_name, par_name, cbuf))
                     printf("\t\tError\n");
                  else
                     printf("\tstring %s\n", cbuf);
                  break;
            }
         }
      }
   
      if((fp = fopen( argv[1], "r")) == 0) {
         printf("Cannot open %s\n", argv[1]); 
         return -1;
      }
   
      printf("rewind option? (y/n)> "); scanf("%s", cbuf);
      rwd = toupper(*cbuf) == 'Y';
   
   /* table of contents */
      printf("TOC? (y/n)> "); scanf("%s", cbuf);
      if(toupper(*cbuf) == 'Y')
         while(nextParamFileSection(fp, cbuf))
            printf("%-5d: [%s]\n", i++, cbuf);
   
   /* search for sections */
      *cbuf = 'a';
      while(1) {
         printf("Find section (name/q) > "); scanf("%s", cbuf);
         if(toupper(*cbuf) == 'Q') 
            break;
         if(findParamFileSection(fp, cbuf, rwd)) /* check for newline on cbuf */
            printf("\t[%s] found\n", cbuf);
         else
            printf("\t[%s] NOT found\n", cbuf);
      }
   
   /* get and display sections */
      while(1) {
         printf("Get section (name/q) > "); scanf("%s", cbuf);
         if(toupper(*cbuf) == 'Q') 
            break;
         if(!(sp = getParamFileSection(fp, cbuf, &nentry, rwd))) {
            printf("Could not get section [%s]\n", cbuf);
            continue;
         }
         cursor = -1;
         printf("Section [%s] has %d entries\n", cbuf, nentry );
         printf("Print param block? (y/n) >"); scanf("%s", cbuf);
      
         if(toupper(*cbuf) == 'Y') 
            for(i = 0; i < nentry; ++i) {
               if((entry = nextParamSectionEntry(sp, nentry, &cursor)) != NULL)
                  printf("%s\n", entry);
               else
                  printf("error, missing entries\n");
            }
      
      /* parse section */
         while(1) {
            printf("Find parameter (name/q) > "); scanf("%s", cbuf);
            if(toupper(*cbuf) == 'Q') 
               break;
            if((entry = getParamSectionEntry(cbuf, sp, nentry, &cursor)) != NULL)
               printf("\tvalue:%s\n", entry);
            else
               printf("\tcould not find parameter\n");
         }
         free(sp);
      }
      fclose(fp);
      return 0;
   }

