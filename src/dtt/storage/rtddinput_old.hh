/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: rtddinput.h						*/
/*                                                         		*/
/* Module Description: gets data through the rtdd and stores it		*/
/*		       a storage object					*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 21Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gdsdatum.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_RTDDINPUT_H
#define _GDS_RTDDINPUT_H

/* Header File List: */
#include <string>
#include <vector>
#include "gdsdatum.h"
#include "testchn.h"
#include "gdsmutex.h"
#include "tconv.h"
#include "testpointmgr.h"
#include "daqsocket.h"

namespace diag {


/** @name Real-time data distribution input API
    Data is read through the real-time data distribution system,
    down-converted and decimated if necessary, partitioned and
    stored in the diagnostics storage object.

   
    @memo Reads data through the real-time data distribution.
    @author Written November 1998 by Daniel Sigg
    @version 0.1
************************************************************************/

//@{

/** @name Data types and constants
    Data types of the real-time data distribution input API

    @memo Data types of the real-time data distribution input API
************************************************************************/

#if 0
/** Compiler flag for a enabling dynamic configuration. When enabled
    the host address and interface information of network data server 
    are queried from the network rather than read in through a file.

    @author DS, June 98
    @see Test point API
************************************************************************/
#define _CONFIG_DYNAMIC
#endif

//@{
//@}


/** @name Functions
    Functions of the real-time data distribution input API

    @memo Functions of the real-time data distribution input API
************************************************************************/

//@{
//@}



/** Class implementing a callback method for the real-time data 
    distribution system.
    @memo Class for rtdd callback.
    @author DS, November 98
    @see Real-time data distribution input API
************************************************************************/
   class rtddCallback {
   public:
      /** Constructs an channel callback object.
          @memo Constructor.
          @param chnname channel name
          @return void
       ******************************************************************/
      explicit rtddCallback (const string& Chnname);
   
      /** Destructs the channel callback object.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~rtddCallback ();
   
      /** Constructs an channel callback object.
          @memo Copy constructor.
          @param rtddcb channel callback object
          @return void
       ******************************************************************/
      rtddCallback (const rtddCallback& rtddcb);
   
      /** Constructs an channel callback object.
          @memo Copy constructor.
          @param rtddcb channel callback object
          @return void
       ******************************************************************/
      rtddCallback& operator= (const rtddCallback& rtddcb);
   
      /** Compares two channel object by their name.
          @memo Equality operator.
          @return true if equal
       ******************************************************************/
      bool operator== (const rtddCallback& rtddchn) const {
         return gds_strcasecmp (chnname.c_str(), 
                              rtddchn.chnname.c_str()) == 0;
      }
   
      /** Compares two channel object by their name.
          @memo Equality operator.
          @return true if equal
       ******************************************************************/
      bool operator== (const string& name) const {
         return gds_strcasecmp (chnname.c_str(), 
                              name.c_str()) == 0;
      }
   
      /** Compares two channel object by their name.
          @memo Inequality operator.
          @return true if not equal
       ******************************************************************/
      bool operator!= (const rtddCallback& rtddchn) const {
         return gds_strcasecmp (chnname.c_str(), 
                              rtddchn.chnname.c_str()) != 0;
      }
   
      /** Compares two channel object by their name.
          @memo Inequality operator.
          @return true if not equal
       ******************************************************************/
      bool operator!= (const string& name) const {
         return gds_strcasecmp (chnname.c_str(), 
                              name.c_str()) != 0;
      }
   
      /** Compares two channel object by their name.
          @memo Equality operator.
          @return true if equal
       ******************************************************************/
      bool operator< (const rtddCallback& rtddchn) const {
         return gds_strcasecmp (chnname.c_str(), 
                              rtddchn.chnname.c_str()) < 0;
      }
   
      /** Subsrcibe to real-time data distribution.
          @memo Subscribe method.
          @param start time when channels are needed
          @param active time when channels become available
          @return true if successful
       ******************************************************************/
      bool subscribe (tainsec_t start = 0, tainsec_t* active = 0);
   
      /** Subsrcibe to real-time data distribution.
          @memo Subscribe method.
          @param start time when channels are needed
          @param active time when channels become available
          @return true if successful
       ******************************************************************/
      bool unsubscribe ();
   
      /** Checks if channel callback was successfully setup.
          @memo isSet operator.
          @return true if channel subscription failed
       ******************************************************************/
      bool isSet () const {
         return (idnum >= 0);
      }
   
      /** Callback method for channel data. This method is called 
          by the RTDD every time new channel data is avaialble. This
          is a pure virtual function and has to be overwritten by a
          descendent class. This function should return 0, in order 
          to continue to receive data.
          @memo Callback method.
          @param time time of first data point (sec)
          @param epoch epoch of first data point
          @param data array of data points
          @param ndata number of data points
          @param err error code indicating invalid or missing data
          @return 0 to continue
       ******************************************************************/
      virtual int callback (taisec_t time, int epoch,
                        float data[], int ndata, int err) = 0;
   
      /// channel name
      string		chnname;
   
   protected:
      /// mutex to protect object
      mutable recursivemutex	mux;
   
   private:
      // rtdd identifier
      int		idnum;
   
      static int callbackC (rtddCallback* usrdata, 
                        taisec_t time, int epoch,
                        float data[], int ndata, int err)
      {
         return usrdata->callback (time, epoch, data, ndata, err);
      }
   };


/** Class for reading channel data from the real-time data 
    distribution system. This class will receive data from the RTDD
    system through the callback mechanism. It implements the following
    prepropressing options: a first decimation stage, a down-conversion,
    a second decimation stage and automatic partition of data. Incoming
    data is buffered in a partition if of the right time frame. If a
    partition is complete it is automatically handed over to the
    diagnostics storage object. Partitions can have unique names,
    meaning a new data object is added for each partition, or they
    can have a name which was previously used, meaning the data is 
    added to the data object of same name.
    @memo Class for channel input from the rtdd.
    @author DS, November 98
    @see Real-time data distribution input API
************************************************************************/
   class rtddChannel : public rtddCallback {
      /// rtdd manager is a friend
      friend class rtddManager;
   
   public:
      class partition;
      class preprocessing;
   
      /// list of partitions
      typedef std::vector <partition> partitionlist;
      /// list of preprocessing objects
      typedef std::vector <preprocessing> preprocessinglist;
   
   
      /** This class describes a channel preprocessing stage. A
          preprocessing stage contains an optional first decimation,
   	  an optional down-conversion and and optional second decimation.
   	  @memo Data preprocessing.
       ******************************************************************/
      class preprocessing {
         /// rtddChannel is a friend
         friend class rtddChannel;
      public:
      
      /** Constructs a preprocessing object.
          @memo Constructor.
      	  @param dataRate channel data rate
          @param Decimate1 first decimation rate (power of 2)
          @param Decimate2 second decimation rate (power of 2)
          @param Zoomstart time zero for down-conversion
          @param Zoomfreq down-conversion frequency
          @param rmvDelay remove decimation delay
          @return void
       ******************************************************************/
         explicit preprocessing (int dataRate, 
                           int Decimate1 = 1, int Decimate2 = 1, 
                           tainsec_t Zoomstart = 0, double Zoomfreq = 0,
                           bool rmvDelay = true);
      
      /** Constructs a preprocessing object.
          @memo Copy constructor.
      	  @param p preprocessing object
          @return void
       ******************************************************************/
         preprocessing (const preprocessing& p);
      
      /** Copies a preprocessing object.
          @memo Copy operator.
      	  @param p preprocessing object
          @return void
       ******************************************************************/
         preprocessing& operator= (const preprocessing& p);
      
      /** Destructs a preprocessing object.
          @memo Destructor.
          @return void
       ******************************************************************/
         ~preprocessing ();
      
      /** Compares two preprocessing object.
          @memo Equality operator.
      	  @param pre preprocessing object
          @return true if equal decimation and zoom parameters
       ******************************************************************/
         bool operator== (const preprocessing& pre) const;
      
      /** Sets the active time of a preprocessing object. When set,
          data is only sent through the decimation filters for the
          specified interval. To account for filter settling times,
          data pass through actually start earlier by 10 times the
          filter delay and stops later by twice the filter delay.
          A value of -1 indicates that the corresponding limit
          should be ignored, i.e. data will be processed irregardly.
          If this function is called multiple times, the active time
          will be the minimum/maximum of all supplied values. Setting
          newValues to true will override this behaviour and take them
          as is. Only when obey is set true, the active time will 
          actually be used to determine if preprocessing should take
          place.
      
          @memo set active time method.
      	  @param Start time when preprocessing must deliver first data
      	  @param Stop time when preprocessing can stop delivering data
      	  @param obey if true active time will actually be used 
      	  @param newValues when false the min/max is calculated
          @return void
       ******************************************************************/
         void setActiveTime (tainsec_t Start, tainsec_t Stop,
                           bool obey = false, bool newValues = false);
      
      /** Data feeding function. If the number of data points fed into
          this routine is smaller than the decimation rate, data will
          be held back until enough data points have been received. Only
          when enough data points are ready will the process function be
          called. The number of data points fed into the routine has 
          to be either a multiple of the total decimation rate or an
          integer fraction thereof. If an integer fraction, subsequent 
          calls must be of equal length.
          @memo Data feeding method.
          @param time time of first data point (sec)
          @param epoch epoch of first data point
          @param data array of data points
          @param ndata number of data points
          @param err error code indicating invalid or missing data
          @param partitions list of partitions
          @param mux channel mutex
          @param update indicates if partition list needs to be updated
          @return true if successful
       ******************************************************************/
         bool operator() (taisec_t time, int epoch,
                         float data[], int ndata, int err,
                         partitionlist& partitions, 
                         mutex& mux, bool& update);
      
      /** Channel preprocessing function.
          @memo Preprocessing method.
          @param time time of first data point (sec)
          @param epoch epoch of first data point
          @param data array of data points
          @param ndata number of data points
          @param err error code indicating invalid or missing data
          @param partitions list of partitions
          @param mux channel mutex
          @param update indicates if partition list needs to be updated
          @return true if successful
       ******************************************************************/
         bool process (taisec_t time, int epoch,
                      float data[], int ndata, int err,
                      partitionlist& partitions, 
                      mutex& mux, bool& update);
      
      protected:
         /// channel data rate
         int		datarate;
         /// first decimation rate (must be a power of 2)
         int		decimate1;
         /// second decimation rate (must be a power of 2)
         int		decimate2;
         /// time zero for down-conversion (nsec)
         tainsec_t	zoomstart;
         /// frequency of down-conversion (Hz)
         double		zoomfreq;
         /** spacing of data points (Hz); original time series, and after 
             1st and 2nd decimation */
         double		dt[3];
      	 /// remove decimation delay
         bool		removeDelay;
      	 /// decimation delay (in sec)
         double		decdelay;
      	 /// number of taps in the delay filter
         int		delaytaps;
      	 /// delay shift (in nsec)
         tainsec_t	delayshift;
      	 /// accumulated delay after first decimation stage (in nsec)
         tainsec_t	delay1;
      
         /// if true do preprocessing only during active time
         bool		useActiveTime;
      	 /// preprocessing start time; -1 ignore, 0 uninitialized
         tainsec_t	start;
      	 /// preprocessing stop time; -1 ignore, 0 uninitialized
         tainsec_t	stop;
      
      private:
         /// buffer time
         tainsec_t	bufTime;
         /// size of data in buffer
         int		bufSize;
         /// t = 0 of grid
         tainsec_t 	t0Grid;
         /// buffer for holding data segments which are too small
         float*		buf;
      
         /// primitive high pass for zomm analysis
         bool		hpinit;
         float		hpval;
         /// temporary storage for delay filter
         float*		tmpdelay1;
         /// temporary storage for 1st decimation filter
         float*		tmpdec1;
         /// temporary storage for 2nd decimation filter, I phase
         float*		tmpdec2I;
         /// temporary storage for 2nd decimation filter, Q phase
         float*		tmpdec2Q;
      };
   
   
      /** This class describes a channel data partition. A partition
          describes a finite time series. It contains a start time, a 
          duration and the spacing of the data points. A partition
          can describe a single time series or the both in-phase and
          the quad-phase of a down-converted time series.
   	  @memo Data partition.
       ******************************************************************/
      class partition {
         /// rtddChannel is a friend
         friend class rtddChannel;
         /// rttChannel::partition is a friend, too
         friend class rtddChannel::preprocessing;
      public:
      
      /** Constructs a partition.
          @memo Constructor.
      	  @param Name name of partition
      	  @param Start beginning of partition (nsec)
      	  @param Duration length of partition (nsec)
      	  @param Dt spacing of data points
      	  @param tp precursor time
          @return void
       ******************************************************************/
         partition (string Name, tainsec_t Start, tainsec_t Duration,
                   double Dt = 1.0, tainsec_t tp = 0);
      
      /** Constructs a partition.
          @memo Copy constructor.
      	  @param p partition
          @return void
       ******************************************************************/
         partition (const partition& p);
      
      /** Sets a new data point spacing.
          @memo data point spacing function.
          @param Dt spacing of data points
          @param Decimate1 first decimation rate (power of 2)
          @param Decimate2 second decimation rate (power of 2)
          @param Zoomstart time zero for down-conversion
          @param Zoomfreq down-conversion frequency
      	  @param rmvDelay remove decimation filter delay
          @return void
       ******************************************************************/
         void setup (double Dt, 
                    int Decimate1 = 1, int Decimate2 = 1, 
                    tainsec_t Zoomstart = 0, double Zoomfreq = 0,
                    bool rmvDelay = true);
      
      /** Copies new data points into the partition.
          @memo Fill function.
          @param data data array
          @param len number of data points
          @param bufnum buffer ID (0 = in-phase; 1 = quad-phase)
          @return void
       ******************************************************************/
         void fill (const float data[], int len, int bufnum = 0);
      
      /** Copies data points from the partition into a data array.
          @memo Copy function.
          @param data data array
          @param len maximum number of data points
          @param cmplx true if down-converted time series
          @return void
       ******************************************************************/
         void copy (float data[], int max, bool cmplx = false);
      
      /** Returns the index of data array to be copied given the
          start time of the time series and its length. Returns 
          -1 if the time series is ahead of the partion interval, and
          -2 if the time series is past the partition interval.
          @memo Index function.
          @param Start beginning of time series (nsec)
          @param  Length number of data points in time series
          @return time series index to fill partition
       ******************************************************************/
         int index (tainsec_t Start, int Length) const;
      
      /** Returns the number of points to be copied from the time 
          series into the partition.
          @memo Range function.
          @param Start beginning of time series (nsec)
          @param Length number of data points in time series
          @return void
       ******************************************************************/
         int range (tainsec_t Start, int Length) const;
      
      /** Indicates to the partition that it can not expect any more
          data. Sets the error flag if partition is not full.
          @memo Stop waiting.
          @return void
       ******************************************************************/
         void nomore ();
      
      /** Checks if partition is full, meaning all data points have 
          been filled into the internal buffer.
          @memo Is full function.
          @return true if partition is full
       ******************************************************************/
         bool isFull () const;
      
      /** Checks if partition is done, meaning it is either full or
          no more data can be expected.
          @memo Is done function.
          @return true if partition is done
       ******************************************************************/
         bool isDone () const;
      
      /** Compares two partitions using their start time.
          @memo Less than operator.
          @param p another partition
          @return true if smaller
       ******************************************************************/
         bool operator< (const partition& p) const;
      
         /// name of time series; will be used to store data
         string		name;
      
      protected:
      	 /// beginning of time series (GPS nsec)
         tainsec_t	start;
      	 /// length of time series (GPS nsec)
         tainsec_t	duration;
         /// precursor time
         tainsec_t	precursor;
      	 /// spacing of data points (sec)
         double		dt;
      	 /// number of data points
         int		length;
      	 /// data buffers
         std::vector<float>	buf[2];
         /// first decimation rate (power fo 2)
         int		decimate1;
         /// second decimation rate (power of 2)
         int		decimate2;
         /// time zero for down-conversion (nsec)
         tainsec_t	zoomstart;
         /// frequency of down-conversion (Hz)
         double		zoomfreq;
      	 /// remove decimation filter delay
         bool 		removeDelay;
      	 /// decimation delay (in sec)
         double		decdelay;
      	 /// taps in time delay filter (in number of original samples)
         int		delaytaps;
         /// remaining time delay of data
         double 	timedelay;
      
      private:
         bool		done;
      };
   
   
      /** Constructs a channel object.
          @memo Constructor.
          @param Chnname channel name
          @param dat storage object for storing partition data
          @param dataRate data rate in Hz
          @param dataType data type as defined by the nds
          @return void
       ******************************************************************/
      rtddChannel (string Chnname, gdsStorage& dat, int dataRate, 
                  int dataType);
   
      /** Constructs a channel object.
          @memo Copy constructor.
      	  @param chn channel object
          @return void
       ******************************************************************/
      rtddChannel (const rtddChannel& chn);
   
      /** Destructs the channel object.
          @memo Destructor.
          @return void
       ******************************************************************/				
      virtual ~rtddChannel ();
   
      /** Copies a channel object.
          @memo Copy operator.
      	  @param chn channel object
          @return channel object
       ******************************************************************/
      rtddChannel& operator= (const rtddChannel& chn);
   
      /** Adds a new preprocessing stage. Usually, this is done 
          automatically by addPartitions.
          @memo Add preprocessing stage.
          @param Decimate1 first decimation rate (power of 2)
          @param Decimate2 second decimation rate (power of 2)
          @param Zoomstart time zero for down-conversion
          @param Zoomfreq down-conversion frequency
      	  @param rmvDelay remove decimation filter delay
      	  @param useActiveTime if true active time will be used 
      	  @param Start time when preprocessing must deliver first data
      	  @param Stop time when preprocessing can stop delivering data
          @return true if successful
       ******************************************************************/
      bool addPreprocessing (int Decimate1 = 1, int Decimate2 = 1, 
                        tainsec_t Zoomstart = 0, double Zoomfreq = 0,
                        bool rmvDelay = true, bool useActiveTime = false,
                        tainsec_t Start = -1, tainsec_t Stop = -1);
   
      /** Adds another list of partitions to the channel object. This
          function also adds all necessary preprocessing objects.
          @memo Add partitions.
          @param newPartitions list of new partitions
      	  @param useActiveTime if true active time will be used 
          @return true if successful
       ******************************************************************/
      bool addPartitions (const partitionlist& newPartitions, 
                        bool useActiveTime = false);
   
      /** Resets the partition list.
          @memo Reset function.
       ******************************************************************/
      void reset ();
   
      /** Skips partitions which require data from the time before
          stop.
          @memo Skip function.
          @param stop skip partitions before this time
       ******************************************************************/
      void skip (tainsec_t stop);
   
      /** Returns the time of the last received data block (or zero
          if none was received yet). The returned time represents the
          time of the (last + 1) data sample in GPS nsec, i.e. all data
          sample with a time earlier than returned were received.
          @memo Time stamp method.
          @return time of last received data block
       ******************************************************************/
      tainsec_t timeStamp () const {
         semlock		lockit (mux);
         return timestamp;
      }
   
      /** Returns the maximum time delay introduced by the preprocessing
          (mainly FIR filter delays).
          @memo Time delay method.
          @return maximum time delay through preprocessing
       ******************************************************************/
      tainsec_t maxDelay () const;
   
      /** Callback method for channel data (see description of parent).
          @memo Callback method.
          @param time time of first data point (sec)
          @param epoch epoch of first data point
          @param data array of data points
          @param ndata number of data points
          @param err error code indicating invalid or missing data
          @return 0 to continue
       ******************************************************************/
      virtual int callback (taisec_t time, int epoch,
                        float data[], int ndata, int err);
   
      /** Looks for finished partitions and stores them in the
          diagnostics storage object.
          @memo Update storage object with finished partitions.
          @param async runs asynchronous if true
          @return void
       ******************************************************************/
      virtual void updateStorage (bool async = false);
   
   protected:
      /// channel data rate
      int		datarate;
      /// channel data type
      int		datatype;
      /// channel data type: bytes per sample
      int		databps;
      /// pointer to storage object
      gdsStorage*	storage;
      /// list of partitions
      partitionlist	partitions;
      /// list of preprocessing stages
      preprocessinglist	preprocessors;
      /// time stamp of most recent data
      tainsec_t		timestamp;
      /// counts how many times channel is in use
      int		inUse;
      /// true if channel is a test point
      bool		isTP;
   
   private:
      /// asynchronous task for updating storage object
      static void updateStorageTask (rtddChannel* chn);
      /**  mutex for updating tasks, every tasks gets a read lock until
           it is finshed, upon destruction of a channel the destructor will
           get a write lock to make sure all update tasks have terminated.
           Never try to get this lock after mux is already locked!! */
      mutable readwritelock	updatelock;
      /// true if asynchronoud update task is running
      bool		asyncUpdate;
   };


/** Class for reading a set of channels from the real-time data 
    distribution system. This object manages a list of rtddChannel
    objects.
    Usage: In general, a diagnostics test should only use add and del
    methods at the beginning and end of the test, respectively. On the 
    other hand a diagnostics supervisory task should use set and clear
    to start and stop the data flow.

    @memo Class for channel input from the rtdd.
    @author DS, November 98
    @see Real-time data distribution input API
 ************************************************************************/
   class rtddManager : public channelHandler {
   public:
      /** Constructs a real-time data distribution management object.
   	  @memo Default constructor
          @param dat storage object
          @param TPmgr test point manager
          @param Lazytime Time to wait for cleanup after a lazy clear
       ******************************************************************/
      explicit rtddManager (gdsStorage* dat = 0, 
                        testpointMgr* TPMgr = 0, double Lazytime = 0);
   
      /** Destructs a real-time data distribution management object.
          @memo Destructor.
       ******************************************************************/
      ~rtddManager ();
   
      /** Establishes connection to the network data server. If the
          server is a 0 pointer (default), the server name is read from
          the parameter file. If the port number is zero (default),
          the port number is read from parameter file.
          @memo Connect method.
          @param server name of NDS
          @param port port number of NDS
          @return true if successful
       ******************************************************************/
      bool connect (const char* server = 0, int port = 0);
   
      /** Initializes the storage object pointer.
          @memo Init method.
          @param dat storage object
          @return true if successful
       ******************************************************************/
      bool init (gdsStorage* dat = 0);
   
      /** Initializes the test point manager pointer.
          @memo Init method.
          @param TPMgr test point manager
          @return true if successful
       ******************************************************************/
      bool init (testpointMgr* TPMgr);
   
      /** Returns true if all channels are set active.
          @memo Are channels set method.
          @return true if set
       ******************************************************************/
      bool areSet () const;
   
      /** Returns true if all channels are in use.
          @memo Are channels used method.
          @return true if set
       ******************************************************************/
      bool areUsed () const;
   
      /** Requests channel data by sending a request to the RTDD API.
          (This will not set test points!) This method will request
          on-line data from the NDS.
          @memo Set method.
          @param start time when channels are needed
          @param active time when channels become available
          @return true if successful
       ******************************************************************/
      bool set (tainsec_t start = 0, tainsec_t* active = 0);
   
      /** Requests channel data by sending a request to the RTDD API.
          (This will not set test points!) This method works with 
          a start time and duration and therefore will lookup data
          from the NDS archive. Start time and duration are given
          in multiples of GPS seconds.
          @memo Set method.
          @param start start time of request
          @param duration requested time interval
          @return true if successful
       ******************************************************************/
      bool set (taisec_t start, taisec_t duration);
   
      /** Returns true if the RTDD is currently busy reading data
          from the NDS.
          @memo Busy method.
          @return true if busy
       ******************************************************************/
      bool busy () const;
   
      /** Clears channels by sending a request to the RTDD API.
          (This will not clear test points!)
          @memo Clear method.
          @param lazy lazy clear if true
          @return true if successful
       ******************************************************************/
      bool clear (bool lazy = false);
   
      /** Adds a channel to the internal list. (Channel is added to
          the test point manager if it is a test point.) Optionally,
   	  the inUse count (after adding it) of the channel is returned.
          @memo Add method.
          @param name channel name
          @param inUseCount number of uses (return)
          @return true if successful
       ******************************************************************/
      bool add (const string& name, int* inUseCount = 0);
   
      /** Adds a channel to the internal list. (Channel is added to
          the test point manager if it is a test point.) Optionally,
   	  the inUse count (after adding it) of the channel is returned.
          @memo Add method.
          @param chn channel information
          @param inUseCount number of uses (return)
          @return true if successful
       ******************************************************************/
      bool add (const rtddChannel& chn, int* inUseCount = 0);
   
      /** Adds a partitions to a channel.
          @memo Add method.
          @param chnname name of channel
          @param partitions partition list
      	  @param useActiveTime if true active time will be used 
          @return true if successful
       ******************************************************************/
      bool add (const string& chnname, 
               const rtddChannel::partitionlist& partitions, 
               bool useActiveTime = false);
   
      /** Adds a preprocessing stage to a channel.
          @memo Add method.
          @param chnname name of channel
          @param Decimate1 first decimation rate (power of 2)
          @param Decimate2 second decimation rate (power of 2)
          @param Zoomstart time zero for down-conversion
          @param Zoomfreq down-conversion frequency
      	  @param rmvDelay remove decimation filter delay
          @return true if successful
       ******************************************************************/
      bool add (const string& name, 
               int Decimate1, int Decimate2 = 1, 
               tainsec_t Zoomstart = 0, double Zoomfreq = 0,
               bool rmvDelay = true);
   
      /** Deletes a channel from the internal list. If a lazy clear
          was done, the the list entry is only marked for deletion.
          (Channel is deleted from the test point manager if it is a 
          test point.)
          @memo Delete method.
          @param chnname name of channel
          @return true if successful
       ******************************************************************/
      bool del (const string& chnname);
   
      /** Deletes all channels from the internal list. Ignores lazy
          clear and always deletes all channels. (Channel are
          deleted from the test point manager if they are test points.)
          @memo Delete method.
          @return true if successful
       ******************************************************************/
      bool del ();
   
      /** Resets a channel from the internal list. 
          @memo Reset method.
          @param chnname name of channel
          @return true if successful
       ******************************************************************/
      bool reset (const string& chnname);
   
      /** Resets all channels from the internal list.
          @memo Reset method.
          @return true if successful
       ******************************************************************/
      bool reset ();
   
      /** Returns the time of the received data blocks. This function
          returns the minimum of all channel time stamps. The function
          guarantees that all data samples from earlier time were 
          received.
          @memo Time stamp method.
          @return time of received data blocks
       ******************************************************************/
      tainsec_t timeStamp () const;
   
      /** Returns the maximum time delay introduced by the preprocessing
          (mainly FIR filter delays).
          @memo Time delay method.
          @return maximum time delay through preprocessing
       ******************************************************************/
      tainsec_t maxDelay () const;
   
      /** Returns the time when the last data receiving was completed.
          This function returns the actual time the transfer was 
          completed rather than the time stamp of the transfer.
          @memo Receive time method.
          @return last time data was received
       ******************************************************************/
      tainsec_t receivedTime () const {
         return lasttime; }
   
   protected:
      /// channel list
      typedef std::vector <rtddChannel> channellist;
   
      /// mutex to protect object
      mutable recursivemutex	mux;
      /// Pointer to storage object
      gdsStorage*	storage;
      /// Pointer to test point object
      testpointMgr*	tpMgr;
      /// list of RTDD channels
      channellist	channels;
      /// time to keep test points around after clear/delete
      double		lazytime;
      /// time when a lazy clear occured (0 indicates no clear)
      double		cleartime;
      /// Real-time mode?
      bool		RTmode;
      /// fast/slow NDS writer?
      bool		fastUpdate;
      /// timestamp of next data transfer
      tainsec_t		nexttimestamp;
      /// stop time of transfer (when reading old data)
      tainsec_t		stoptime;
      /// actual time when last data transfer occured
      tainsec_t		lasttime;
      /// NDS interface object
      DAQSocket		nds;
   
      /** Finds a channel. Returns channels.end() if not found.
          @memo Find method.
          @param name channel name
          @return iterator to channel object
       ******************************************************************/
      channellist::iterator find (const string& name) const;
   
      /** Cleanup task. Clears channels if lazy delay has expired.
          @memo Cleanup method.
       ******************************************************************/
      void cleanup ();
   
   private:
      /// prevent copy
      rtddManager (const rtddManager&);
      rtddManager& operator= (const rtddManager&);
   
      /// ID of nds task
      taskID_t		ndsTID;
      /// mutex to protect task from being canceled
      mutex		ndsmux;
      /// nds server name
      char		daqServer[256];
      /// nds server port
      int		daqPort;
      /// nds task for receiving data
      static int ndstask (rtddManager& RTDDMgr);
      /// nds callback method
      bool ndsdata (const char* buf, int err = 0);
      /// nds start
      bool ndsStart ();
      /// nds start with old data
      bool ndsStart (taisec_t start, taisec_t duration);
      /// nds stop
      bool ndsStop ();
      /// nds check stop time
      bool ndsCheckEnd ();
   
      /// ID of cleanup task
      taskID_t		cleanTID;
      /// cleanup task
      static int cleanuptask (rtddManager& RTDDMgr);
   };

//@}
}

#endif /* RTDDINPUT */
