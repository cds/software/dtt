/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  diaggui						     */
/*                                                                           */
/* Module Description:  ROOT gui for diagnostics tests			     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/** @name diaggui
    This is a main diagnostics program. It implements a graphical user
    interface to communicate with the diagnostics kernel and to perform
    stimulus response tests. The following test are currently supported:
    Fourier tools, swept sine measurement, sine response measurement and
    triggered time response measurement.
   
    @memo Diagnostics program
    @author Written December 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

/* Header File List */
#include <unistd.h>
#include <sys/file.h>
#include <fcntl.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TGMsgBox.h>
#include <string>
#include "diagmain.hh"
#include "gdsmain.h"
#include "cmdapi.hh"
#include <iostream>
#include "signal.h"

   using namespace diag;
   using namespace std;

   // ::COMPILER:: This is a sad story
   // SUNPRO doesn't automatically link shared libraries against Cstd
   // If they are truely dynamically loaded, the caller must have the 
   // missing symbols defined!
   // So just defined them here (even so they are never used!)
   static std::string s1________ ("abcd");
   static std::string s2________ (s1________, 0, 2);

   // ROOT object
   TROOT root ("GUI", "Diagnostics GUI");


   // Command line arguments for diagnostics kernel
   const string 	_argHelp ("-h");
   const string 	_argHelpAlt ("-?");
   const string		_argHelpAlt2 ("--help") ;
   const string 	_argGUI ("-g");
   const string		_argGUIAlt ("--test") ;
   const string 	_argLocal ("-l");
   const string		_argLocalAlt ("--local");
   const string 	_argServer ("-s");
   const string		_argServerAlt ("--server");
   const string 	_argScript ("-f");
   const string		_argScriptAlt ("--script") ;
   const string 	_argVerbose ("-v");
   const string		_argVerboseAlt ("--verbose");
   const string 	_argNDS ("-n");
   const string		_argNDSAlt ("--nds");
   const string 	_argNDSport ("-m");
   const string		_argNDSportAlt ("--port");
   const string 	_argPrintFile ("-p");
   const string		_argPrintFileAlt ("--print");
   const string 	_argPrintFormat ("-P");
   const string		_argPrintFormatAlt ("--format");
   const string 	_argDisplay ("-d");
   const string		_argDisplayAlt2 ("--display") ;
   const string 	_argDisplayAlt ("-display");
   const string     _argAutostart ("-a");
   const string     _argAutostartAlt ("--autostart");
   const string     _argLive ("--live");
   const string     _argLiveAlt ("--fom");
   const string     _argStallMsg("--stallmsg");
   const char* const 	_sweptsine = "sweptsine";
   const char* const 	_FFT = "fft";
   const char* const 	_sineresp = "sineresponse";
   const char* const 	_timeseries = "timeseries";

   const char* const _helptext = 
   "Usage: diaggui -flags filename\n"
   "       --test 'test'\n"
   "       -g 'test'     diagnostics test:\n"
   "                     fft - Fourier tools\n"
   "                     sweptsine - swept sine response test\n"
   "                     sineresponse - (multiple) sine response test\n"
   "                     timeseries - triggered time series measurement\n\n"
   "       --autostart\n"
   "       -a            start test automatically.  Requires a filename to be specified.\n\n"
   "       --stallmsg   Enables display of a warning message when test is not running\n\n"
   "       --live\n"
   "       --fom         Enables the following options: -a, --stallmsg\n"
   "                     Use for continuous live, or figure of merit displays.\n"
   "                     The behavior of --fom is subject ot change as\n"
   "                     more features are developed.\n\n"
   "       --local\n"
   "       -l            local diagnostics kernel\n\n"
   "       --server 'server'\n"
   "       -s 'server'   remote diagnostics server\n\n"
   "       --nds 'NDS'\n"
   "       -n 'NDS'      network data server address\n\n"
   "       --port 'port'\n"
   "       -m 'port'     port of network data server\n\n"
   "       --print 'file'\n"
   "       -p 'file'     print file\n"
   "                     File - extension determines plot format\n"
   "                            no extension assumes printer name\n\n"
   "       --format 'format'\n"
   "       -P 'format'   print format\n"
   "                     format: 'orientation''layout''selection'\n"
   "                     Orientation - P (portrait), L (landscape, default)\n"
   "                     Layout - 1 (default), 2 or 4\n"
   "                     Selection - F (1st, default), S (2nd), A (all)\n\n"
   "       --display 'display'\n"
   "       -d 'display'  X windows display\n\n"
   "       --help\n"
   "       -?\n"
   "       -h            this help\n";


namespace diag {
   class diagcommandline : public basic_commandline {
   public:
   
      diagcommandline (int argc, char* argv[], bool Silent = false)
      : basic_commandline (argc, argv, Silent), fCB (0) {
      }
      void SetCB (DiagMainWindow* cb) {
         fCB = cb; }
   
   protected:
      DiagMainWindow* fCB;
      virtual bool notify (const string& msg, 
                        const char* prm, int pLen, char** res, int* rLen);
   };

   bool diagcommandline::notify (const string& msg, 
                     const char* prm, int pLen, char** res, int* rLen)
   {
      res = 0;
      rLen = 0;
      if (fCB) {
         return (bool) fCB->Notification (msg.c_str());
      }
      else {
         return true;
      } 
   }
}


   char* strnew (const char* p)
   {
      char*	t = new (nothrow) char [strlen (p) + 1];
      if (t != 0) {
         strcpy (t, p);
      }
      return t;
   }


   int main (int argc, char **argv)
   {      
      // parse arguments
      Int_t	meastype = 0;
      Int_t	rootArgc = 1;
      char*	rootArgv[100] = {0};
      Int_t	diagArgc = 1;
      char*	diagArgv[100] = {0};
      bool	diagkernel = false;
      bool	usernds = false;
      bool 	verbose = false;
      bool  autostart = false;
      bool  stallmsg = false;
      string	filename;
      bool	printit = false;
      string    printfile;
      string    printformat;
   
      rootArgv[0] = strnew (argv[0]);
      diagArgv[0] = strnew (argv[0]);
      for (int i = 1; i < argc; i++) {
         // look for test argument
         if (_argGUI == argv[i] || _argGUIAlt == argv[i]) 
	 {
            if ((i + 1 < argc) && (argv[i+1][0] != '-')) 
	    {
               string testname = string (argv[i+1]);
               for (unsigned int j = 0; j < testname.size(); j++) {
                  testname[j] = tolower (testname[j]);
               }
               if (testname == _FFT) {
                  meastype = 0;
               }
               else if (testname == _sweptsine) {
                  meastype = 1;
               }
               else if (testname == _sineresp) {
                  meastype = 2;
               }
               else if (testname == _timeseries) {
                  meastype = 3;
               }
               i++;
            }
         }
         // look for local kernel
         else if (_argLocal == argv[i] || _argLocalAlt == argv[i]) 
	 {
            if (!diagkernel && (diagArgc < 100)) {
               diagArgv[diagArgc] = strnew ("-l");
               diagkernel = true;
               diagArgc++;
            }
         }
         // look for remote kernel
         else if (((_argServer == argv[i]) || _argServerAlt == argv[i]) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) 
	 {
            if (!diagkernel && (diagArgc < 99)) {
               diagArgv[diagArgc] = strnew ("-s");
               diagArgv[diagArgc+1] = strnew (argv[i + 1]);
               diagkernel = true;
               diagArgc += 2;
               i++;
            }
         }
         // look for startup script
         // else if (((_argScript == argv[i] || _argScriptAlt == argv[i]) && 
                 // (i + 1 < argc) && (argv[i+1][0] != '-')) {
            // if (diagArgc < 99) {
               // diagArgv[diagArgc] = strnew ("-f");
               // diagArgv[diagArgc+1] = strnew (argv[i + 1]);
               // diagArgc += 2;
               // i++;
            // }
         // }
         // look for NDS server name
         else if (((_argNDS == argv[i]) || _argNDSAlt == argv[i]) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) 
	 {
            if (diagArgc < 99) {
               diagArgv[diagArgc] = strnew ("-n");
               diagArgv[diagArgc+1] = strnew (argv[i + 1]);
               diagArgc += 2;
               i++;
               usernds = true;
            }
         }
         // look for NDS port number
         else if (((_argNDSport == argv[i]) || _argNDSportAlt == argv[i]) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) 
	 {
            if (diagArgc < 99) {
               diagArgv[diagArgc] = strnew ("-m");
               diagArgv[diagArgc+1] = strnew (argv[i + 1]);
               diagArgc += 2;
               i++;
            }
         }
         // look for print file argument
         else if (((_argPrintFile == argv[i]) || _argPrintFileAlt == argv[i]) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) 
	 {
            printit = true;
            printfile = argv[i+1];
            // if (rootArgc < 99) {
               // rootArgv[rootArgc] = strnew ("-b");
               // rootArgc += 1;
            // }
            i++;
         }
         // look for print format argument
         else if (((_argPrintFormat == argv[i]) || _argPrintFormatAlt == argv[i]) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) 
	 {
            printit = true;
            printformat = argv[i+1];
            // if (rootArgc < 99) {
               // rootArgv[rootArgc] = strnew ("-b");
               // rootArgc += 1;
            // }
            i++;
         }
         // look for verbose
         else if (_argVerbose == argv[i] || _argVerboseAlt == argv[i]) 
	 {
            verbose = true;
         }
         // look for help
         else if ((_argHelp == argv[i]) ||  (_argHelpAlt == argv[i]) || (_argHelpAlt2 == argv[i])) 
	 {
            cout << _helptext;
            return 0;
         }
         // look for x display
         else if (((_argDisplay == argv[i]) || (_argDisplayAlt == argv[i]) || (_argDisplayAlt2 == argv[i])) && 
                 (i + 1 < argc) && (argv[i+1][0] != '-')) 
	 {
            if (rootArgc < 99) {
               rootArgv[rootArgc] = strnew ("-display");
               rootArgv[rootArgc+1] = strnew (argv[i + 1]);
               rootArgc += 2;
               i++;
            }
         }
         else if((_argAutostart == argv[i]) || (_argAutostartAlt == argv[i]))
         {
             autostart = true;
         }
         else if(_argStallMsg == argv[i])
         {
             stallmsg = true;
         }
         else if((_argLive == argv[i]) || (_argLiveAlt == argv[i]))
         {
             autostart = true;
             stallmsg = true;
         }
         // look for file name (must be last argument)
         else if ((i + 1 == argc) && (argv[i][0] != '-')) 
	 {
            // if (diagArgc < 99) {
               // diagArgv[diagArgc] = strnew ("-f");
               // diagArgv[diagArgc+1] = strnew (argv[i]);
               // diagArgc += 2;
               // filename = argv[i];
            // }
            filename = argv[i];
         }
         // otherwise assume ROOT argument
         else 
	 {
            if (rootArgc < 100) 
	    {
               rootArgv[rootArgc] = strnew (argv[i]);
               rootArgc++;
            }
         }
      }
      
      // make sure we connect to a kernel
      if (!diagkernel && (diagArgc < 100)) {
         diagArgv[diagArgc] = strnew ("-l");
         diagArgc++;
      }
   
      // if not verbose disable std out and err
      if (!verbose) {
         int i = open ("/dev/null", 2);
         (void) dup2(i, 1);
         (void) dup2(i, 2);   
      }
      else
      {
         cout << "Running in verbose mode." << endl;
      }
      
      cout << "Creating command line interface" << endl;
   
      // create command line interface to diagnostics kernel
      basic_commandline*	cmdline = 
         new (nothrow) diagcommandline (diagArgc, diagArgv, false);
      if (NULL == cmdline)
      {
         cerr << "Out of memory creating command line" << endl;
         return 1;
      }
      // Read in file
      cout << "clearEcho()" << endl;
      cmdline->clearEcho();
      cout << "parsing line for " << filename << endl;
      cmdline->parse (string ("restore -all ") + filename);
      cout << "check if deque is empty" << endl;
      // Have the decency to check if the deque is empty before accessing elements...
      if (!cmdline->getEcho().empty() && cmdline->getEcho().back().find ("error") != string::npos) {
         filename = "";
      }
   
      cout << "Initializing root" << endl;
      // initializing root environment
      // ::IMPORTANT:: initialize ROOT after loading libgds.so (through
      // command line) to avoid problems with fork in the epics library
      // (i.e., the creation of a CA repeater)
      TApplication theApp ("Diagnostics tests", &rootArgc, rootArgv);
   
      // check if connection to a kernel was established
      if ((cmdline == 0) || !cmdline || !cmdline->isConnected()) {
         new TGMsgBox (gClient->GetRoot(), gClient->GetRoot(),
                      "Error", "Unable to connect to diagnostics kernel",
                      kMBIconStop, kMBOk);
         delete cmdline;
         gApplication->Terminate (1);
         return 1;
      }

      if(autostart && filename == "")
      {
          cerr << "Error - a valid file name must be specified when using the -a or --autostart option." << endl;
          new TGMsgBox(gClient->GetRoot(), gClient->GetRoot(),
                       "Error", "A valid file name must be specified when using the -a or --autostart option.",
                       kMBIconStop, kMBOk);
          autostart = false;
      }

      
      cout << "creating main window" << endl;
      // create main window
      DiagMainWindow mainWindow (gClient ? gClient->GetRoot() : 0, cmdline, 
                           meastype, filename.c_str(), autostart, stallmsg);
      // intercept notification messages from the kernel
      ((diagcommandline*)cmdline)->SetCB (&mainWindow);
   
      cout << "starting main window" << endl;
      if (printit) {
         string format = printformat + "#" + printfile;
         mainWindow.Print (format.c_str());
      }
      // run the GUI
      else {
         signal(SIGPIPE, SIG_IGN);
         theApp.Run();
      }
   
      delete cmdline;
      return 0;
   }
