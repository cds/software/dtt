//
// Created by erik.vonreis on 8/13/20.
//

#ifndef CDS_CRTOOLS_COMPONENTHOLDER_H
#define CDS_CRTOOLS_COMPONENTHOLDER_H

#include <list>
#include <memory>

using namespace std;

/// Hold components that need lifetime as long as ComponentHolder
/// but aren't used after creation
/// The main purpose of this class is to keep alive GUI components that
/// are referenced only at window creation but can't be deleted
/// until the window closes
///
/// Create components of any subtype of T with make_shared
/// and add it with << operator or add_component method.
/// \tparam T
template<class T>
class ComponentHolder {

private:
  list<shared_ptr<T>> components;

public:
  ComponentHolder() = default;

  ComponentHolder(const ComponentHolder &) = delete;

  void operator=(const ComponentHolder &) = delete;

  template<class S>
  shared_ptr<S> add_component(shared_ptr<S> comp)
  {
    components.emplace_back(dynamic_pointer_cast<T>(comp));

    return comp;
  }

  /// Call like so: shared_ptr<S> local_ptr = comp_holder << make_shared<S>( ...
  /// \tparam S a subclass of T
  /// \param comp A shared ointer to an object of type S that you wish to
  /// live as long as ComponentHodler
  /// \return  The same shared pointer passed as an argument.
  template<class S>
  shared_ptr<S> operator<<(std::shared_ptr<S> comp)
  {
    return add_component(comp);
  }

};

#endif // CDS_CRTOOLS_COMPONENTHOLDER_H
