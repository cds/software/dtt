/* version $Id$ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef _LIGO_TLGCOLOR_H
#define _LIGO_TLGCOLOR_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGColor						*/
/*                                                         		*/
/* Module Description: Color handling	 				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 12Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGColor.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "TLGFrame.hh"
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <vector>

namespace dttgui {

   class TPlotColorLookup;
   class TLGNumericControlBox;

/** @name TLGColor
    This header exports color handling routines. 
 ************************************************************************/
//@{

   /// Global instance if color lookup table
   TPlotColorLookup& gPlotColorLookup();


/** Color lookup table. Internal use only. Negative numbers are RGB;
    positive numbers between 0 and 7 are form index.
   
    @memo Color lookup table
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TPlotColorLookup {
   public:
      /// Color descriptor
      class ColorType {
      protected:
         /// Root index
         Int_t		fIndex;
         /// RGB value
         Int_t		fRGB;
      public:
      	 /// Constructor (negative index is RGB value!)
         ColorType (Int_t col = 1);
      	 /// Constructor (RGB values)
         ColorType (Int_t r, Int_t g, Int_t b);
      	 /// Set color
         void SetColor (Int_t col);
         /// Set RGB
         void SetRGB (Int_t r, Int_t g, Int_t b);
      	 /// Set RGB (positive value)
         void SetRGB (Int_t rgb);
         /// Equality operator
         bool operator== (const ColorType& ct) const {
            return fIndex == ct.fIndex; }
         /// ROOT color index
         Int_t Index() const {
            return fIndex; }
      	/// RGB value
         Int_t RGB() const {
            return fRGB; }
      	 /// Get color as used by the GUI
         Bool_t GuiColor (ULong_t& color) const;
      	 /// Get compatibility value (as used by DTT)
         Int_t Compatibility() const;
      };
      /// Color table
      typedef std::vector<ColorType> ColorTable;
   
   private:
      /// Color table
      ColorTable		fCTable;
      /// Color table version	
      int			fVersion;
   
   public:
      /// Create
      TPlotColorLookup ();
      /// Access table
      ColorType& operator[] (Int_t index);
      /// Access table
      const ColorType& operator[] (Int_t index) const;
      /// Get version
      int GetVersion() const {
         return fVersion; }
      /// Size of list
      int Size() const {
         return fCTable.size(); }
      /** Add a color and return the root index.
          A negative argument is an RGB value, a positive
          value is a ROOT color index. Returns the ROOT color
          index or -1 on error. */
      int Add (Int_t col);
      /** Remove a color and return the root index of the removed
          entry. A negative argument is an RGB value, a positive
          value is a ROOT color index. Returns the ROOT color
          index or -1 if not found. */
      int Remove (Int_t col);
   };



/** Color selection entry. For internal use only.
   
    @memo Color selection entry
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGColorLBEntry : public TGTextLBEntry {
   protected:
      /// Color value (RGB) Hex value, 0xRRGGBB.
      ULong_t 		fColor;
      /// Color index (for LIGOGUI; fEntryId is index for ROOT)
      Int_t		fColIndex;
   
   public:
      /// Create color selection entry
      TLGColorLBEntry (TGWindow* p, Int_t id);
      /// Select a color
      virtual Bool_t SetColor (Int_t cid);
      /// Get the color
      virtual Int_t GetColor() const {
         return fColIndex; }
      /// Update
      virtual void Update (TGLBEntry* entry);              
      /// Redraw
      virtual void DoRedraw();

      /// Acivate - Controls what happens when the mouse moves over this objec.
      virtual void Activate(Bool_t a) ;
   };



/** Color selection combobox. Selects colors from a standard lookup table.
   
    @memo Color selection combobox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGColorComboBox : public TGComboBox {
   public: 
      /// Create color selection combobox
      TLGColorComboBox (TGWindow* p, Int_t id, Bool_t allownew = kTRUE);
   
      /// Select (syntax changed in version 5.12-0)
#if ROOT_VERSION_CODE < 330752       
      virtual void Select (Int_t id);
#else
      virtual void Select (Int_t id, Bool_t emit = kTRUE);
#endif
      /// Handle button
      virtual Bool_t HandleButton (Event_t* event);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   
   protected:
      /// Build color list
      virtual void Build();
      /// Allow new?
      bool		fAllowNew;
      /// Version of color list
      int		fVersion;
      /// Last selection
      int		fLastSel;
   };


/** Color allocation dialog box. Allocates and removes colors.
   
    @memo Color allocation dialog
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGColorAllocDialog : public TLGTransientFrame {
   protected:
      /// Group frame
      TGCompositeFrame*	fG[1];
      /// lines
      TGCompositeFrame* fF[4];
      /// labels
      TGLabel*		fLabel[7];
      /// Color combo box
      TGComboBox*	fColor;
      /// color values
      TLGNumericControlBox*	fNum[6];
      /// buttons
      TGButton*		fButton[4];
      /// layout hints
      TGLayoutHints*	fL[4];
      /// Pointer to global dialog box
      static TLGColorAllocDialog*	gColorDlg;
   
   public:
      /// Constructor
      TLGColorAllocDialog (const TGWindow *p, const TGWindow *main);
      /// Destructor
      virtual ~TLGColorAllocDialog ();
      /// Close window method
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
      /// Bring up dialog box (stand-alone!)
      static void DialogBox (const TGWindow* p);
   };


//@}
}

#endif
