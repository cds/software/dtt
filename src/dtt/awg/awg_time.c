//
// Created by erik.vonreis on 3/15/24.
//

#include "awg_time.h"
#include "tconv.h"



static tainsec_t (*_gps_time)() = &TAInow;

/**
 * Return the real-time system time in nano-seconds since the GPS epoch.
 * There's a default implementation, but it can be over written with awg_set_time_function();
 * Or reset to to default with awg_set_default_time_function()
 * @return
 */
tainsec_t awg_gps_time() {
    return _gps_time();
}

/**
 * Override the current time function with a new function.
 * Useful for providing custom synchronization functions as from DTT.
 * Affects calls to awg_gps_time();
 * @param f
 */
void awg_set_time_function(tainsec_t (*f)()) {
    if(NULL != f) {
        _gps_time = f;
    }
}

/**
 * Revert to the default time function.
 * Affects calls to awg_gps_time().
 */
void awg_set_default_time_function() {
    _gps_time = TAInow;
}