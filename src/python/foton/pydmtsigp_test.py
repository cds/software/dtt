#!/usr/bin/env python3

import sys
import pydmtsigp as sigp
import unittest
import math
import array
import numpy as np

def dComplex_from(x):
    if isinstance(x, int) or isinstance(x, float):
        return sigp.dComplex(x, 0)
    elif isinstance(x, sigp.fComplex):
        return sigp.dComplex(x.Real(), x.Imag())
    elif isinstance(x, sigp.dComplex):
        return sigp.dComplex(x)
    elif isinstance(x, complex):
        return sigp.dComplex(x.real, x.imag)
    else:
        raise ValueError(f"value {x} is not convertable to dComplex")

def dComplex_is_real(x):
    """Returns true if dComplex x is close to real"""
    thresh = 1e-4
    r = x.Real()
    i = x.Imag()

    if i == 0:
        return True
    if r == 0:
        return False

    if abs(i/r) < thresh:
        return True
    else:
        return False

class Polynomial(object):
    """A polynomial type that builds itself up with dComplex coefficients
    put can report real coefficients"""
    def __init__(self, coefs = []):
        self.coefs = []
        for coef in coefs:
            self.coefs.append(dComplex_from(coef))


    def __mul__(self, other):
        if isinstance(other, Polynomial):
            m_limit = len(self.coefs)
            n_limit = len(other.coefs)
            new_limit = m_limit + n_limit -1
            new_coefs = [sigp.dComplex(0,0) for x in range(new_limit)]
            for m in range(m_limit):
                for n in range(n_limit):
                    new_coefs[m+n] += self.coefs[m] * other.coefs[n]
            return Polynomial(new_coefs)
        else:
            try:
                y = dComplex_from(other)
                return Polynomial([x*y for x in self.coefs])
            except ValueError:
                raise ValueError(f"operand {other} cannot be multiplied by a Polynomial type")

    def is_real(self):
        """test coefficients to see if they are close to being real"""
        is_real = [dComplex_is_real(x) for x in self.coefs]
        if not all(is_real):
            print("*** Warning.  Not all coefficients of a polynomial are real")
        return all(is_real)

    def gds_order(self):
        """
        Return coefficients in gds_order as real numbers

        :param self:
        :return:
        """
        self.is_real()
        gds_coefs = [x.Real() for x in self.coefs]
        gds_coefs.reverse()
        return gds_coefs

    def add_zero(self, zero):
        """Add an zero to an existing polynomial through polynomial multiplication

        returns the new polynomial with the added zero
        """
        z = dComplex_from(zero)
        pz = Polynomial([-z, 1])
        return self * pz



class TestPoly(unittest.TestCase):
    def setUp(self):
        self.p1 = Polynomial([1])
        self.p2 = Polynomial([1+1j, 1])

    def test_setup(self):
        self.assertEqual(self.p1.gds_order(), [1])
        self.assertEqual(self.p2.gds_order(), [1, 1])

    def test_mult_constant(self):
        p = self.p1 * 3
        self.assertEqual(p.gds_order(), [3])
        p = self.p2 * 3
        self.assertEqual(p.gds_order(), [3, 3])

    def test_mult_p(self):
        p = self.p1 * self.p2
        self.assertEqual(p.gds_order(), [1,1])
        p = self.p2*self.p2
        self.assertEqual(p.gds_order(), [1, 2, 0])
        self.assertFalse(p.is_real())

    def test_add_complex_zero(self):
        # add in the conjugate zero to p2
        # should make a real polynomial
        p = self.p2.add_zero(-1 + 1j)
        print(self.p2.coefs)
        print(p.coefs)
        self.assertTrue(p.is_real())
        self.assertEqual(p.gds_order(), [1, 2, 2])


def inverse_bilinear(fs_hz, root, unwarp=True):
    """
    Hand written bilinear calculation to calc continuous roots independently from
    dmtsigp
    :param fs_hz: sampling frequency
    :param root:  a sigp.dComplex root in the z-plane
    :return: gain, continuous root
    """

    # denominator inverted
    c_one = sigp.dComplex(1,0)
    denom = root + c_one
    numer = root - c_one
    scaling_factor = 2*fs_hz
    new_root = scaling_factor*numer/denom

    g = 1.0
    if unwarp:
        if(new_root.Mag() > 0):
            warp_factor = new_root.Mag() / scaling_factor
            g = math.atan(warp_factor)/warp_factor
            new_root *= g

    return 2*g*scaling_factor/denom.Mag(), new_root

def complex_to_str(c):
    return f"{c.Real()}+{c.Imag()}i"

def calc_response(b_raw, a_raw, input):
    """
    Calculate the response of a filter defined as [b,a] on the given input
    Zero input and output is assumed prior to input.
    :param b_raw: standard "b" filter tap coefficients
    :param a_raw: standard "a" filter tap coefficeints, assumes a0 = 1.0.  a1 starts at index 0, a2 at index 1 etc.
    :param input: a vector of input values
    :return: A response based on input the same length as input, as a numpy array
    """
    a = np.array(a_raw)
    b = np.array(b_raw)
    past_x = np.array([0]*len(b))
    past_y = np.array([0]*len(a))
    response = []

    for x0 in input:
        past_x = np.array([x0] + list(past_x[:-1]))
        y0 = (past_x * b).sum() + (past_y * a).sum()
        past_y = np.array([y0] + list(past_y[:-1] ))
        response.append(y0)

    return np.array(response)

step_response = None
impulse_response = None

class TestFotonMethods(unittest.TestCase):
    def setUp(self):
        self.rates_hz = [2**x for x in range(0, 20)]
        self.filter_names = [f"FilterDesign{r}" for r in self.rates_hz]
        self.filter_designs = [sigp.FilterDesign(x[0], x[1]) for x in zip(self.rates_hz, self.filter_names)]
        # for fd in self.filter_designs:
        #     fd.direct([-0.1], [-0.2])
        self.calc_iir()

    def calc_iir(self):

        self.iir_fs = 65536
        fd = sigp.FilterDesign(self.iir_fs, "IIR Test Filter")
        #fd = sigp.FilterDesign(1024, "IIR Test Filter")
        #fd.ellip(sigp.Filter_Type.kBandPass, 2, 1.0, 10.0, 0.1, 0.2)
        self.iir_b = [-0.2, -0.3]
        self.iir_a = [0.1]

        self.iir_zeros = [-sigp.dComplex(self.iir_b[1] / self.iir_b[0], 0)]
        self.iir_poles = [sigp.dComplex(self.iir_a[0], 0)]

        self.iir_sos = [self.iir_b[0], self.iir_b[1] / self.iir_b[0], 0, -self.iir_a[0], 0]

        self.iir_gain = self.iir_sos[0]

        fd.direct(self.iir_b, self.iir_a)

        self.iir_filter = fd.get()

        # calculate roots and zeros in the S plane
        self.s_gain = self.iir_gain
        self.s_zeros = []
        self.s_poles = []

        for z in self.iir_zeros:
            g, nz = inverse_bilinear(self.iir_fs, z)
            self.s_zeros.append(nz)
            self.s_gain /= g

        for p in self.iir_poles:
            g, np = inverse_bilinear(self.iir_fs, p)
            self.s_poles.append(np)
            self.s_gain *= g

        # calc polynomials
        numer_p = Polynomial([1])
        for z in self.s_zeros:
            numer_p = numer_p.add_zero(z)

        self.iir_numer = numer_p.gds_order()

        denom_p = Polynomial([1])
        for p in self.s_poles:
            denom_p = denom_p.add_zero(p)

        self.iir_denom = denom_p.gds_order()

        # memoize the responses.  They take a while to calculate
        global step_response
        global impulse_response

        # calculate expected step response
        self.response_duration_sec = 1
        self.response_size = int(self.response_duration_sec * self.iir_fs)

        step_input = [1] * self.response_size
        if step_response is None:
            self.step_response = calc_response(self.iir_b, self.iir_a, step_input)
            step_response = self.step_response
        else:
            self.step_response = step_response

        # calculate expected impulse response
        impulse_input = [0] * self.response_size
        impulse_input[0] = 1.0
        if impulse_response is None:
            self.impulse_response = calc_response(self.iir_b, self.iir_a, impulse_input)
            impulse_response = self.impulse_response
        else:
            self.impulse_response = impulse_response

        # save filter design object
        self.iir_fd = fd


    def test_FilterDesign_creation(self):
        for fd in self.filter_designs:
            self.assertIsInstance(fd, sigp.FilterDesign)

    def test_get_pipes(self):
        for fd in self.filter_designs:
            self.assertIsInstance(fd.get(), sigp.Pipe)

    def test_call_pipes(self):
        for fd in self.filter_designs:
            self.assertIsInstance(fd(), sigp.Pipe)

    def test_init(self):
        """
        Also tests getFSample()
        """
        fd = self.filter_designs[0]
        self.assertEqual(fd.getFSample(), 1.0)
        fd.init(2**20)
        self.assertEqual(fd.getFSample(), 2**20)

    def test_setFSample(self):
        fd = self.filter_designs[0]
        self.assertEqual(fd.getFSample(), 1.0)
        fd.setFSample(2**20)
        self.assertEqual(fd.getFSample(), 2**20)

    def test_unitygain(self):
        """
        Also tests gain()
        :return:
        """
        fd = self.filter_designs[0]
        self.assertTrue(fd.isUnityGain())
        fd.gain(2.0)
        self.assertFalse(fd.isUnityGain())

    def test_names(self):
        for fd, name in zip(self.filter_designs, self.filter_names):
            self.assertEqual(name, fd.getName())
        fd = self.filter_designs[0]
        new_name = "NewName"
        fd.setName(new_name)
        self.assertEqual(fd.getName(), new_name)

    def test_getFOut(self):
        for fd in self.filter_designs:
            self.assertEqual(fd.getFSample(), fd.getFOut())

    def test_heterodyne(self):
        for fd in self.filter_designs:
            self.assertFalse(fd.getHeterodyne())
        fd = self.filter_designs[0]
        pipe = fd()
        fd.set(pipe, 2.0, True)
        # self.assertTrue(fd.getHeterodyne())
        # self.assertEqual(fd.getFSample()*2.0, fd.getFOut())

    def test_prewarp(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.getPrewarp())
        fd.setPrewarp(False)
        self.assertFalse(fd.getPrewarp())

    def test_copy(self):
        fd = self.filter_designs[0]
        pipe = fd.copy()
        self.assertIsInstance(pipe, sigp.Pipe)

    def test_release(self):
        fd = self.filter_designs[0]
        pipe = fd.release()
        self.assertIsInstance(pipe, sigp.Pipe)

    def test_add(self):
        fd = self.filter_designs[0]
        pipe = fd()
        self.assertTrue(fd.add(pipe))

    def test_step_response(self):
        fd = self.iir_fd
        response = fd.response("step", self.response_duration_sec);
        for i in range(len(response)):
            self.assertAlmostEqual(response[i], self.step_response[i])

    def test_impulse_response(self):
        fd = self.iir_fd
        response = fd.response("impulse", self.response_duration_sec);
        for i in range(len(response)):
            self.assertAlmostEqual(response[i], self.impulse_response[i])

    def test_ramp_response(self):
        fd = self.iir_fd
        response = fd.response("ramp", self.response_duration_sec);
        self.assertEqual(len(response), self.response_size)

    def test_bad_response(self):
        fd = self.iir_fd
        self.assertRaises(ValueError, fd.response, "not_a_reponse", self.response_duration_sec)

    def test_direct_response(self):
        fd = self.iir_fd
        step_input = [1.0]*self.response_size
        response = fd.response(step_input)
        print("resp=", response[:50])
        print("exp=", self.step_response[:50])
        for i in range(len(response)):
            self.assertAlmostEqual(response[i], self.step_response[i])

# test complex structs
    def test_dComplex(self):
        x = sigp.dComplex(1.0,0)
        y = sigp.dComplex(0, 1.0)
        self.assertIsInstance(x, sigp.dComplex)
        self.assertEqual(y, x*y)
        self.assertEqual(x+y, sigp.dComplex(1.0,1.0))
        z = sigp.dComplex(y)
        z += y
        self.assertEqual(z, 2.0*y)
        z /= 2.0
        self.assertEqual(z, y)
        self.assertAlmostEqual(z, y)
        self.assertEqual(z/x, z)
        z = y - x
        self.assertEqual(z, sigp.dComplex(-1.0, 1.0))
        self.assertEqual(z.Real(), -1)
        self.assertEqual(z.Imag(), 1)
        self.assertEqual(x.Mag(), 1)
        self.assertEqual(z.MagSq(), 2)
        self.assertEqual(x.Arg(), 0)
        self.assertEqual(-z, sigp.dComplex(1.0, -1.0))
        self.assertEqual(z + ~z, -2.0*x)

        w = x.setMArg(2.0**0.5, math.pi*3/4.0)
        self.assertAlmostEqual(z.Arg(), math.pi*3/4.0)
        w -= z
        self.assertAlmostEqual(w.Real(), 0)
        self.assertAlmostEqual(w.Imag(), 0)
        self.assertEqual(-y, ~y)
        self.assertFalse(x != x)

        z.xcc(z)
        q = sigp.dComplex(1,0)
        self.assertEqual(2.0*q , z)

    def test_fComplex(self):
        x = sigp.fComplex(1.0,0)
        y = sigp.fComplex(0, 1.0)
        self.assertIsInstance(x, sigp.fComplex)
        self.assertEqual(y, x*y)
        self.assertEqual(x+y, sigp.fComplex(1.0,1.0))
        z = sigp.fComplex(y)
        z += y
        self.assertEqual(z, 2.0*y)
        z /= 2.0
        self.assertEqual(z, y)
        self.assertAlmostEqual(z, y)
        self.assertEqual(z/x, z)
        z = y - x
        self.assertEqual(z, sigp.fComplex(-1.0, 1.0))
        self.assertEqual(z.Real(), -1)
        self.assertEqual(z.Imag(), 1)
        self.assertEqual(x.Mag(), 1)
        self.assertEqual(z.MagSq(), 2)
        self.assertEqual(x.Arg(), 0)
        self.assertEqual(-z, sigp.fComplex(1.0, -1.0))
        self.assertEqual(z + ~z, -2.0*x)

        w = x.setMArg(2.0**0.5, math.pi*3/4.0)
        self.assertAlmostEqual(z.Arg(), math.pi*3/4.0)
        w -= z
        self.assertAlmostEqual(w.Real(), 0)
        self.assertAlmostEqual(w.Imag(), 0)
        self.assertEqual(-y, ~y)
        self.assertFalse(x != x)

        z.xcc(z)
        q = sigp.fComplex(1,0)
        self.assertEqual(2.0*q , z)


# test designs
    def test_filter(self):
        pass
        # fd = self.filter_designs[0]
        # test_design = "gain(2)"
        # fd.filter(test_design)
        # self.assertEqual(fd.getFilterSpec(), test_design)
        # self.assertFalse(fd.isUnityGain())

    def test_pole(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.pole(0.1, 2.0))
        self.assertEqual(fd.getFilterSpec(), "pole(0.1,2)")
        self.assertTrue(fd.pole(0.1))
        self.assertEqual(fd.getFilterSpec(), "pole(0.1,2)pole(0.1)")

    def test_zero(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.zero(0.1, 2.0))
        self.assertEqual(fd.getFilterSpec(), "zero(0.1,2)")
        self.assertTrue(fd.zero(0.1))
        self.assertEqual(fd.getFilterSpec(), "zero(0.1,2)zero(0.1)")

    def test_pole2(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.pole2(0.1, 1.0, 2.0))
        self.assertEqual(fd.getFilterSpec(), "pole2(0.1,1,2)")
        self.assertTrue(fd.pole2(0.1, 1.0))
        self.assertEqual(fd.getFilterSpec(), "pole2(0.1,1,2)pole2(0.1,1)")

    def test_zero2(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.zero2(0.1, 1.0, 2.0))
        self.assertEqual(fd.getFilterSpec(), "zero2(0.1,1,2)")
        self.assertTrue(fd.zero2(0.1, 1.0))
        self.assertEqual(fd.getFilterSpec(), "zero2(0.1,1,2)zero2(0.1,1)")

    def test_zpk(self):
        fd = self.filter_designs[0]
        zeros = [sigp.dComplex(-0.1,0)]
        poles = [sigp.dComplex(-0.2,0)]
        self.assertTrue(fd.zpk(zeros, poles, 1.0))
        # rounding error that could change with compilers.
        # self.assertEqual(fd.getFilterSpec(), "zpk([-0.1000000000000001],[-0.2],0.9999999999999998)")

    def test_rpoly(self):
        fd = self.filter_designs[0]
        num = array.array("d",[1.0])
        den = np.array([1.0])
        self.assertTrue(fd.rpoly(num, den, 1.0))
        self.assertEqual(fd.getFilterSpec(), "rpoly([1],[1],1)")

    def test_biquad(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.biquad(1.0,1.0,1.0,1.0, 1.0))
        self.assertEqual(fd.getFilterSpec(), "biquad(1,1,1,1,1)")

    def test_sos(self):
        fd = self.filter_designs[0]
        sos = [1.0]*5
        self.assertTrue(fd.sos(sos))
        self.assertEqual(fd.getFilterSpec(), "sos(1,[1;1;1;1])")

    def test_zroots(self):
        fd = self.filter_designs[0]
        zeros = [sigp.dComplex(-0.1,0)]
        poles = [sigp.dComplex(-0.2,0)]
        self.assertTrue(fd.zroots(zeros, poles, 1.0))
        self.assertEqual(fd.getFilterSpec(), "zroots([-0.1],[-0.2],1)")

    def test_direct(self):
        fd = self.filter_designs[0]
        b = [1.0]
        a = [1.0]
        self.assertTrue(fd.direct(b, a))
        self.assertEqual(fd.getFilterSpec(), "direct([1],[1])")
        success, b,a = sigp.iir2direct(fd.get())

    def test_ellip(self):
        fd = self.filter_designs[0]
        result = fd.ellip(sigp.Filter_Type.kBandPass, 2, 1.0, 10.0, 0.1, 0.2)
        self.assertTrue(result)
        self.assertEqual(fd.getFilterSpec(), 'ellip("BandPass",2,1,10,0.1,0.2)')

    def test_cheby1(self):
        fd = self.filter_designs[0]
        result = fd.cheby1(sigp.Filter_Type.kBandPass, 2, 1.0, 0.1, 0.2)
        self.assertTrue(result)
        self.assertEqual(fd.getFilterSpec(), 'cheby1("BandPass",2,1,0.1,0.2)')

    def test_cheby2(self):
        fd = self.filter_designs[0]
        result = fd.cheby2(sigp.Filter_Type.kBandPass, 2, 1.0, 0.1, 0.2)
        self.assertTrue(result)
        self.assertEqual(fd.getFilterSpec(), 'cheby2("BandPass",2,1,0.1,0.2)')

    def test_butter(self):
        fd = self.filter_designs[0]
        result = fd.butter(sigp.Filter_Type.kBandPass, 2, 0.1, 0.2)
        self.assertTrue(result)
        self.assertEqual(fd.getFilterSpec(), 'butter("BandPass",2,0.1,0.2)')

    def test_notch(self):
        fd = self.filter_designs[0]
        result = fd.notch( 0.1, 10)
        self.assertTrue(result)
        self.assertEqual(fd.getFilterSpec(), "notch(0.1,10,0)")

    def test_resgain(self):
        fd = self.filter_designs[0]
        result = fd.resgain( 0.1, 10)
        self.assertTrue(result)
        self.assertEqual(fd.getFilterSpec(), "resgain(0.1,10,30)")

    def test_comb(self):
        fd = self.filter_designs[0]
        result = fd.comb( 0.1, 10)
        self.assertTrue(result)
        self.assertEqual(fd.getFilterSpec(), "comb(0.1,10,0)")

    # FIR filters
    def test_setFirType(self):
        fd = self.filter_designs[0]
        # just check if we crash
        fd.setFirType(0x7)

    def test_remez(self):
        fd = self.filter_designs[0]
        self.assertRaises(ValueError, fd.remez, 10, [1.0,2.0,3.0], [1.0,2.0], [])
        self.assertRaises(ValueError, fd.remez, 10, [1.0,2.0,3.0,4.0], [1.0,2.0], [1.0])
        self.assertTrue(fd.remez ( 10, [0.1,0.2,0.3,0.4], [1.0,2.0], []))
        self.assertEqual(fd.getFilterSpec(), "remez(10,[0.1;0.2;0.3;0.4],[1;2])")
        self.assertTrue(fd.remez ( 10, [0.1,0.2,0.3,0.4], [1.0,2.0], [1.0,2.0]))

    # def test_firls(self):
    #     fd = self.filter_designs[0]
    #     self.assertRaises(ValueError, fd.firls, 10, [1.0,2.0,3.0], [1.0,2.0], [])
    #     self.assertRaises(ValueError, fd.firls, 10, [1.0,2.0,3.0,4.0], [1.0,2.0], [1.0])
    #     self.assertTrue(fd.firls ( 10, [0.1,0.2,0.3,0.4], [1.0,2.0,3.0,4.0], [1.0,2.0]))
    #     self.assertEqual(fd.getFilterSpec(), "firls(10,[0.1;0.2;0.3;0.4],[1;2;3;4],[1;2])")

    def test_firw(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.firw(10, sigp.Filter_Type.kBandPass, "Hanning", 0.1, 0.4))
        self.assertTrue(fd.getFilterSpec().startswith("firw("))

    def test_fircoeffs(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.fircoefs([1.0,1.0,1.0,1.0], True))
        self.assertTrue(fd.getFilterSpec().startswith("fircoef("))


    def test_difference(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.difference())
        self.assertTrue(fd.getFilterSpec().startswith("difference("))


    def test_decimateby2(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.decimateBy2(10))
        func = "decimateBy2("
        self.assertEqual(fd.getFilterSpec()[:len(func)], func)

    def test_linefilters(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.linefilter(0.1))
        func = "linefilter("
        self.assertEqual(fd.getFilterSpec()[:len(func)], func)

    def test_limiter(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.limiter("", 0))
        func = "limiter("
        self.assertEqual(fd.getFilterSpec()[:len(func)], func)

    def test_multirate(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.multirate("abs", 65536))
        func = "multirate("
        self.assertEqual(fd.getFilterSpec()[:len(func)], func)

    def test_mixer(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.mixer(0.2))
        func = "mixer("
        self.assertEqual(fd.getFilterSpec()[:len(func)], func)

    def test_frontend(self):
        # not implemented
        fd = self.filter_designs[0]
        self.assertFalse(fd.frontend("test_filter.txt", "BOUNCE_ALL",0))
        #self.assertEqual(fd.getFilterSpec(), 'cheby1("BandPass",4,1,9,11)')

    def test_setgain(self):
        fd = self.filter_designs[0]
        self.assertTrue(fd.setgain(0.2, 0.9))
        func = "gain("
        self.assertEqual(fd.getFilterSpec()[:len(func)], func)

    def test_closeloop(self):
        fd = self.filter_designs[0]
        fd.butter(sigp.Filter_Type.kBandPass, 2, 0.1, 0.2)
        self.assertTrue(fd.closeloop())


    def test_xfer(self):
        fd = self.filter_designs[0]

        # pass a single frequency, should get complex value in return
        self.assertIsInstance( fd.Xfer(0.1), sigp.fComplex)

        # should return an FSeries object
        # self.assertIsInstance(fd.Xfer(0.1,0.8, 0.01), sigp.FSeries)

        # should return a list of fComplex
        tf = fd.Xfer([0.1,0.2])
        self.assertIsInstance(tf[0], sigp.fComplex)

        # should return two lists: first of frequencies, second of complex TF values
        freq, tf = fd.Xfer(0.1, 0.2)
        self.assertIsInstance(tf[0], sigp.fComplex)
        self.assertIsInstance(freq[0], float)


    # module iir functions
    def test_bilinear(self):
        r = sigp.dComplex(-0.4,0)
        r1 = sigp.dComplex(-0.4,0)
        g = sigp.bilinear(1, r)
        r2 = sigp.dComplex(r)
        g2 = sigp.inverse_bilinear(1, r)
        self.assertAlmostEqual(r1.Real(), r.Real())
        self.assertAlmostEqual(r1.Imag(), r.Imag())
        self.assertAlmostEqual(g*g2, 1.0)
        self.assertNotEqual(r2, r)

    def test_inverse_bilinear(self):
        r = sigp.dComplex(-0.4, 0.1)
        r3 = sigp.dComplex(r)
        print(f"r={complex_to_str(r)}")
        print(f"r3={complex_to_str(r3)}")
        g2, r2 = inverse_bilinear(1, r3, True)
        g = sigp.inverse_bilinear(1, r, True)
        print(f"r={complex_to_str(r)}")
        print(f"r2={complex_to_str(r2)}")
        self.assertAlmostEqual(g2, g)
        self.assertAlmostEqual(r2.Real(), r.Real())
        self.assertAlmostEqual(r2.Imag(), r.Imag())


    def test_sort_roots(self):
        r = sigp.dComplex(-0.4,0.1)
        roots = [r, ~r, sigp.dComplex(-0.2,0)]
        self.assertTrue(sigp.sort_roots(roots))

    def test_s2z(self):
        zeros = [sigp.dComplex(-0.2, 0)]
        poles = [sigp.dComplex(-0.1, 0)]
        fs = 65536
        gain = 1
        success, new_zeros, new_poles, gain = sigp.s2z(fs, zeros, poles, gain)
        self.assertTrue(success)
        self.assertIsInstance(gain, float)
        self.assertIsInstance(new_zeros[0], sigp.dComplex)
        self.assertIsInstance(new_poles[0], sigp.dComplex)
        self.assertNotEqual(zeros[0], new_zeros[0])

    def test_z2s(self):
        zeros = [sigp.dComplex(-0.2, 0)]
        poles = [sigp.dComplex(-0.1, 0)]
        fs = 65536
        gain = 1
        success, new_zeros, new_poles, gain = sigp.z2s(fs, zeros, poles, gain)
        self.assertTrue(success)
        self.assertIsInstance(gain, float)
        self.assertIsInstance(new_zeros[0], sigp.dComplex)
        self.assertIsInstance(new_poles[0], sigp.dComplex)
        self.assertNotEqual(zeros[0], new_zeros[0])

    def test_z2z(self):
        # roots into second order sections
        roots = [sigp.dComplex(-0.1, 0)]
        zeros = [sigp.dComplex(-0.2, 0)]
        success, sos = sigp.z2z(roots, zeros, 1)
        self.assertTrue(success)
        self.assertIsInstance(sos[0], float)

        # sos into roots
        sos = [1.0] * 5
        success, zeros, poles, gain = sigp.z2z(sos)
        self.assertTrue(success)
        self.assertIsInstance(zeros[0], sigp.dComplex)
        self.assertIsInstance(poles[0], sigp.dComplex)
        self.assertIsInstance(gain, float)

    def test_isiir(self):
        self.assertTrue(sigp.isiir(self.iir_filter))

    def test_iirsoscount(self):
        self.assertEqual(sigp.iirsoscount(self.iir_filter), 1)

    def test_iirpolecount(self):
        self.assertEqual(sigp.iirpolecount(self.iir_filter), 1)

    def test_iirzerocount(self):
        self.assertEqual(sigp.iirzerocount(self.iir_filter), 1)

    def test_iirpolezerocount(self):
        poles, zeros = sigp.iirpolezerocount(self.iir_filter)
        self.assertEqual(poles, 1)
        self.assertEqual(zeros, 1)

    def test_iirorder(self):
        print(f"iirorder={sigp.iirorder(self.iir_filter)}")
        order = sigp.iirorder(self.iir_filter)
        self.assertEqual(order, 1)


    def test_iircmp(self):
        self.assertTrue(sigp.iircmp(self.iir_filter, self.iir_filter))

    def test_iir2zpk_design(self):
        success, design = sigp.iir2zpk_design(self.iir_filter)
        self.assertTrue(success)
        func = "zpk([" + ";".join(["%.10f"%x.Real() for x in self.s_zeros]) + "],[" + ";".join(["%.11f"%x.Real() for x in self.s_poles]) + "],"+"%.16f" % self.s_gain+")"
        self.assertEqual(design, func)

    def test_iir2zpk(self):
        success, zeros, poles, gain = sigp.iir2zpk(self.iir_filter)

        print(zeros[0].Real(), zeros[0].Imag())
        print(self.iir_zeros[0].Real(), self.iir_zeros[0].Imag())
        self.assertTrue(success)
        self.assertIsInstance(zeros[0], sigp.dComplex)
        self.assertIsInstance(poles[0], sigp.dComplex)
        self.assertIsInstance(gain, float)
        self.assertEqual(len(poles), len(self.s_poles))
        self.assertEqual(len(zeros), len(self.s_zeros))



        for cz, ez in zip(zeros, self.s_zeros):
            self.assertAlmostEqual(cz, ez)

        for cp, ep in zip(poles, self.s_poles):
            self.assertAlmostEqual(cp, ep)

        self.assertAlmostEqual(self.s_gain, gain)

    def test_iir2poly(self):
        success, numer, denom, gain = sigp.iir2poly(self.iir_filter)
        self.assertTrue(success)
        self.assertGreater(len(numer), 0)
        self.assertIsInstance(numer[0], float)
        self.assertIsInstance(denom[0], float)
        self.assertIsInstance(gain, float)
        print(f"z={complex_to_str(self.s_zeros[0])}")
        self.assertEqual(numer, self.iir_numer)
        self.assertEqual(denom, self.iir_denom)
        self.assertAlmostEqual(gain, self.s_gain)


    def test_iir2z_design(self):
        success, design = sigp.iir2z_design(self.iir_filter)
        self.assertTrue(success)
        func = "zroots("
        self.assertEqual(design[:len(func)], func)

    def test_iir2z_sos(self):
        success,sos = sigp.iir2z_sos(self.iir_filter)
        self.assertTrue(success)
        self.assertIsInstance(sos[0], float)

        self.assertEqual(sos, self.iir_sos)

    def test_iir2z(self):
        success, zeros, poles, gain = sigp.iir2z(self.iir_filter)
        self.assertIsInstance(zeros[0], sigp.dComplex)
        self.assertIsInstance(poles[0], sigp.dComplex)
        self.assertIsInstance(gain, float)

        # assumes only one zero and pole

        self.assertEqual(len(zeros), len(self.iir_zeros))
        self.assertEqual(len(poles), len(self.iir_poles))

        for cz, ez in zip(zeros, self.iir_zeros):
            self.assertAlmostEqual(cz, ez)

        for cp, ep in zip(poles, self.iir_poles):
            self.assertAlmostEqual(cp, ep)

        self.assertAlmostEqual(self.iir_gain, gain)

    def test_iir2direct(self):
        success, b, a = sigp.iir2direct(self.iir_filter)
        self.assertTrue(success)
        self.assertIsInstance(b[0], float)
        self.assertIsInstance(a[0], float)
        self.assertEqual(b, self.iir_b)
        self.assertEqual(a, self.iir_a)

if __name__ == '__main__':
    unittest.main()
