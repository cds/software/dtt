SET(CMAKE_EXPORT_COMPILE_COMMANDS true)
cmake_minimum_required(VERSION 3.13 FATAL_ERROR)

SET(CMAKE_PROJECT_HOMEPAGE_URL  "http://git.ligo.org/erik.vonreis/dtt")

enable_testing()

# version numbers on prerelease should have a ~ in front of the suffix.  ~ is stored before end-of-string, so
# 0.2.0~rc2 is earlier version than 0.2.0, which is earlier than 0.2.0+rc2
# 0.2.0~rc2 is earlier than 0.2.0~rc11
# therefore, VERSION should be set to the main version number for the *next* release.
project(cds-crtools VERSION 4.1.4)

#set to "ax" for alpha release, "bx" for a beta release, "rcx" for a release candidate, and an empty string for release.
SET(VERSION_SUFFIX "")
SET(VERSION_TAG "~${VERSION_SUFFIX}")
if ("${VERSION_SUFFIX}" STREQUAL "")
    SET(VERSION_TAG "")
endif()

SET(VERSION_STRING "${cds-crtools_VERSION}${VERSION_TAG}")

SET(CDS_VERSION "${VERSION_STRING}")

MESSAGE(STATUS "version: ${VERSION_STRING}")

add_compile_definitions(
        R__ACCESS_IN_SYMBOL
        METAIO_VSN=80501
        DTT_VERSION=\"${VERSION_STRING}\"
        CDS_VERSION=\"${CDS_VERSION}\"
        #VERSION=\"${CDS_VERSION}\"
)

MESSAGE(STATUS "Install Prefix: ${CMAKE_INSTALL_PREFIX}")

SET(CMAKE_MODULE_PATH
(${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake) )

INCLUDE(CheckCXXCompilerFlag)
INCLUDE(CheckIncludeFile)
INCLUDE(CheckIncludeFileCXX)
INCLUDE(CheckFunctionExists)
INCLUDE(CheckCXXSourceCompiles)
INCLUDE(FindThreads)
include(CheckLibraryExists)
include(GNUInstallDirs)
include(FindPkgConfig)
include(CheckIncludeFile)

add_subdirectory(src)

#packaging
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "A collection of tools for gathering data from, and inducing excitations on LIGO data channels. diag, diagd, diaggui, awggui, tpcmd, chndump are included along with foton, the filter design program.")
SET(CPACK_PACKAGE_CONTACT "Erik von Reis <evonreis@caltech.edu>")

#note that a general CPACK version number seems to override package specific version  number
#so we must specify package version specifically only.

#debian packaging
SET(CPACK_DEBIAN_PACKAGE_VERSION ${VERSION_STRING})
SET(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)
SET(CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)
SET(CPACK_DEBIAN_PACKAGE_REPLACES "gds-crtools")
SET(CPACK_DEBIAN_PACKAGE_CONFLICTS "gds-crtools")
SET(CPACK_DEBIAN_PACKAGE_PROVIDES "gds-crtools")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "gds-cdsbase (>= 2.18.17+cds5)")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "python3-scipy")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "python3")
SET(CPACK_DEBIAN_PACKAGE_DEPENDS "python-scipy, python3-scipy, python-numpy, python3-numpy")
SET(CPACK_DEBIAN_PACKAGE_RELEASE $ENV{PACKAGE_RELEASE})

#redhat packaging
SET(CPACK_RPM_FILE_NAME RPM-DEFAULT)
#underscore because '+' causes bug in CPACK RPM which renders symlinks absolute instead of relative
SET(CPACK_RPM_PACKAGE_VERSION "${VERSION_STRING}")
SET(CPACK_RPM_PACKAGE_RELEASE $ENV{PACKAGE_RELEASE})
SET(CPACK_RPM_PACKAGE_LICENSE "GPL2+")
SET(CPACK_RPM_PACKAGE_AUTOREQ 1)
SET(CPACK_RPM_PACKAGE_REQUIRES root-graf-x11)

SET(CPACK_SOURCE_PACKAGE_FILE_NAME "${CMAKE_PROJECT_NAME}-${VERSION_STRING}")
SET(CPACK_SOURCE_IGNORE_FILES "/.builds/;/cmake-build-debug/;/fontconfig/;/.git;.srctrl;/buildtest/;/cmakesetup/")
SET(CPACK_SOURCE_GENERATOR TGZ)

include(CPack)
